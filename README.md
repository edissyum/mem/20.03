Installation :
A noter qu'il faut avoir préalablement installé les paquets nécessaires au bon fonctionnement de Maarch : https://docs.maarch.org/gitbook/html/MaarchCourrier/20.03/guat/guat_installation/debian.html

	git clone https://gitlab.com/edissyum/mem/20.03 /var/www/maarch_courrier/
    cd /var/www/maarch_courrier/
	git checkout tags/$(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1) -b $(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1)

Procédure pour la mise à jour chez un client (déjà installé en MEM) :

    cp -r /var/www/maarch_courrier/ /var/www/maarch_courrier.bck/
    pg_dump -Fp --column-inserts -f dump.psql maarch
    cd /var/www/maarch_courrier/
    git pull
    git stash
    git checkout tags/$(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1) -b $(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1)
    (si des fichiers gênent, ne pas hésitez à supprimer le fichier en question)

Procédure pour la mise à jour chez un client (non installé en MEM) :

    cp -r /var/www/maarch_courrier/ /var/www/maarch_courrier.bck/
    pg_dump -Fp --column-inserts -f dump.psql maarch
    cd /var/www/maarch_courrier/
    rm -rf node_modules/

    Si nécessaire, supprimer les fichiers qui bloque manuellement

    git remote set-url origin https://gitlab.com/edissyum/mem/20.03
    git pull
    git stash
    git checkout tags/$(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1) -b $(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1)
    (si des fichiers gênent, ne pas hésitez à supprimer le fichier en question)


TECHNIQUE :

Procédure de mise à jour de MEM :

	git remote set-url origin https://labs.maarch.org/maarch/MaarchCourrier
	git pull
	git stash
	git checkout tags/$(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1) -b $(git tag --sort=v:refname | grep -E '20.03.+([0-9])$' | tail -1)
	git remote set-url origin https://gitlab.com/edissyum/mem/20.03

En dehors des modules Edissyum, vérifiez les commits du dernier TAG MEM afin de reporter les correctifs
Pensez à reporter les spécifiques indiquant que nous sommes sur une installation MEM

Modules configurés :

    - Open-Capture
    - Réconciliation
    - MailCollect
    - Export PESv2
    - SVE

PARAMETRES À RAJOUTER :

    - Pour faire la recherche par typist, "Agent qualifiant" dans l'ajout de critères il vous faut :
        - Rajouter un paramètres Maarch : 
            - Identifiant : ActionQualifID
            - Description : Identifiants des actions d'envoi en qualification
            - Type : Chaine de caractères
            - Valeur : Liste des actions d'envoi en qualification, séparée par une virgule (e.g : ACTION#20,ACTION#21) 

    - Afin d'augmenter le nombre de résultats lors de la recherche d'expéditeur ou de destinataire il vous faut :
        - Rajouter un paramètres Maarch : 
            - Identifiant : CorrespondantMaxItems
            - Description : Nombre de résultats maximums pour la recherche de contacts
            - Type : Entier
            - Valeur example : 500 

    - Filtre des utilisateurs pour le module user_quota : 
        - Rajouter un paramètres Maarch : 
            - Identifiant : user_quota_filtered
            - Description : Filtre des utilisateur du module user_quota
            - Type : Chaîne de caractères
            - Valeur example : 'superadmin', 'edissyum', 'adminfnc'

Fix effectués :

    - Fix pour la liste de diff lors de la redirection (Wiki/Maarch/Maarch 20.03/Correctifs Liste de diffusion)
       - Raison : Quand on selectionnait un utilisateur (lors de la redirection) ayant une entité primaire différente de celle du document courant, cette dernière était changée

	- Fix pour éviter la création de contact si l'adresse e-mail existe déjà 
        - src/app/contact/controllers/ContactController.php ligne 144 à 153

	- Rajout de la spécificité MEM dans l'affichage des versions 
        - src/app/versionUpdate/controllers/VersionUpdateController.php
		- src/frontend/app/about-us.component.html
        - src/frontend/app/about-us.component.ts
		- src/frontend/lang/lang-fr.ts

	- Cacher le bouton de mise à jour dans l'administration : 
		- src/frontend/service/privileges.service.ts

    - Afficher les modèles d'enregistrement désactivés dans la fiche détaillé : 
        - src/frontend/app/indexation/select-indexing-model/select-indexing-model.component.ts

    - Ne pas supprimer les customs si ils n'apparaisent pas dans le modèle d'enregistrement
        - src/app/resource/controllers/StoreController.php

    - Fix Réconciliation manuelle non fonctionnelle si le document cible n'a pas de doc
        - src/app/action/controllers/ActionMethodController.php

    - Add fingerprint when file cannot be converted
        - src/app/convert/controllers/ConvertPdfController.php

    - Fix de la génération de thumbnails à la volée
        - src/app/convert/controllers/ConvertThumbnailController.php

    - Rajouter la possibilité de spécifier l'application quand on ajoute un id externe à un courrier
        - src/app/resource/controllers/ResController.php
        - Dans l'appel WS PUT /res/externalInfos, il sera possible de rajouter un argument optionnel 'app' 
            "externalInfos" => [
                [
                    "app" => "APPLICATION_X",
                    "external_id => 123,
                    "res_id" => 100,
                    "external_link" => ""
                ]
            ],
            "status" => "VAL"

    - Fix pour la génération des thumbnails depuis un fichier HTML 
        - src/app/convert/controllers/ConvertThumbnailController.php

    - Fix pour ne pas afficher une ancienne thumbnail le temps que le nouvelle charge
        - src/frontend/app/home/home.component.ts
        - src/frontend/app/list/basket-list.component.ts 

    - Patch pour fonctionner sur les VPS ou pour fonctionner avec un custom mentionné dans le vhost : setEnv CUSTOM_MAARCH cs_maarch
        - src/core/models/CoreConfigModel.php
        - core/class/class_core_tools.php

    - Fix pour l'envoi d'email en superadmin
        - src/frontend/app/sentResource/sent-resource-page/sent-resource-page.component.ts

    - Rajout de tables manquantes pour la RAZ des données
        - sql/delete_all_ressources.sql
    
    - Fix pour la migration des contacts 
        - migration/20.03/migrateContacts.php

    - Rajout d'extensions personnalisées
        - apps/maarch_entreprise/xml/extensions.xml

    - Rajout du if pour éviter une erreur si la priorité n'est pas dans le modèle de document
    - Ajout du if pour éviter une erreur en superadmin lors de l'affichage des informations du courrier
        - src/frontend/app/indexation/indexing-form/indexing-form.component.ts

    - Ajout du if pour éviter une erreur lors de l'envoi en visa depuis l'indexation
        - src/frontend/app/visa/visa-workflow.component.ts

    - Ajout du port pour la connexion BDD
        - modules/ldap/process_users_to_maarch.php

    - Fix pour ne pas afficher les entités désactivés dans l'admin
        - src/app/administration/controllers/AdministrationController.php

    - Fix pour le departement par défaut si il n'a qu'un chiffre
        - src/frontend/app/administration/contact/page/form/contacts-form.component.ts

    - Fix pour éviter l'activation d'une action après le refus d'enregistrer depuis les courriers à qualifier
        - src/frontend/app/process/process.component.ts

    - Fix pour les notifications ANC
        - modules/templates/datasources/mlb_notes_content.php
    
    - Fix pour permettre la modification d'un courrier départ si le typist est égal à l'utilisateur connecté
        - src/app/resource/controllers/ResController.php
        - src/frontend/app/viewer/document-viewer.component.ts

    - Fix pour permettre au superadmin de voir les emails
        - src/app/email/controllers/EmailController.php
        - src/app/user/controllers/UserController.php
    
    - Ajout de la date de clôture dans l'export des courriers
        - core/class/ExportControler.php

    - Ajout du typist dans la recherche :
        - apps/maarch_entreprise/js/functions.js
        - apps/maarch_entreprise/indexing_searching/search_adv.php
        - apps/maarch_entreprise/indexing_searching/search_adv_result.php
        - apps/maarch_entreprise/xml/IVS/validation_rules.xml

    - Fix pour permettre à l'utilisateur apache de lancer la commande convert depuis OnlyOffice
        - src/app/convert/controllers/ConvertThumbnailController.php

    - Ajout d'une option pour restreindre les annotations à notre entités
        - src/frontend/service/privileges.service.ts
        - src/frontend/app/notes/note-editor.component.ts

    - Add closing_date if status == 'END'
        - src/app/resource/controllers/ResController.php

    - Fix pour éviter que l'export de banette soit utilisé lors de l'export de courrier depuis la recherche
        - core/class/ExportControler.php
    
    - Fix docservers rights for VPS (Pyb01)
        - src/app/docserver/controllers/DocserverController.php

    - Ajout de la variable de langue userINACT
        - src/frontend/lang/lang-fr.ts

    - Augmentation du nombre de résultats pour les destinataires / expéditeurs
        - apps/maarch_entreprise/js/indexing.js
        - apps/maarch_entreprise/css/styles.css
        - src/core/controllers/AutoCompleteController.php

    - Ajout du lastname lors de la recherche sans contact selectionné
        - apps/maarch_entreprise/indexing_searching/search_adv_result.php
        - apps/maarch_entreprise/lang/fr.php

    - Ajout d'un filtre utilisateur sur le userquota
        - src/app/user/controllers/UserController.php

    - Envoi des notifications aux utilisateurs avec status INACT
        - modules/notifications/batch/basket_event_stack.php

    - Fix pour éviter un objet de PJ > 255 char
        - src/frontend/app/attachments/attachment-create/attachment-create.component.html

    - Fix pour éviter une erreur lors de la migration des emails
        - migration/19.04/migrateSendmail.php

    - Ajout du connecteur pastell parapheur
        - src/core/models/CurlModel.php
        - src/frontend/app/app.module.ts
        - modules/visa/xml/remoteSignatoryBooks.xml.default
        - modules/visa/batch/process_mailsFromSignatoryBook.php
        - src/app/action/controllers/PreProcessActionController.php
        - src/app/action/controllers/ExternalSignatoryBookTrait.php
        - src/app/external/externalSignatoryBook/controllers/PastellController.php
        - src/frontend/app/actions/send-external-signatory-book-action/send-external-signatory-book-action.component.ts
        - src/frontend/app/actions/send-external-signatory-book-action/send-external-signatory-book-action.component.html

    - Fix pour éviter l'activation d'une action si toutes les infos ne sont pas renseignées
        - src/frontend/app/process/process.component.ts

    - Ajout du connecteur Neocity (EME01)
        - rest/index.
        - src/app/resource/controllers/ResController.php
        - apps/maarch_entreprise/xml/config_reporting.json

    - Fix pour récupérer toutes les natures lors des migrations (PYB01)
        - migration/20.03/2003.sql
