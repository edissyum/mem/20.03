<?php

/*
*    Copyright 2008-2018 Maarch
*
*   This file is part of Maarch Framework.
*
*   Maarch Framework is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Maarch Framework is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Maarch Framework.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Export Class
*
* Contains all the specific functions to export documents
*
* @package  Maarch LetterBox 3.0
* @version 3.0
* @since 06/2007
* @license GPL
* @author  Nathan Cheval <nathan.cheval@edissyum.com>
*
*/

require_once 'core/core_tables.php';
use SrcCore\models\CoreConfigModel;

class export
{
    public function get_path(){
        if (file_exists(
            $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
            . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
            . DIRECTORY_SEPARATOR . 'maarch_entreprise'
            . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
            . 'file_export.xml'
        )
        ) {
            $path =  $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
                . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
                . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR . 'file_export.xml';
        } else {
            $path = 'apps' . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . 'file_export.xml';
        }
        $config = simplexml_load_file($path);
        if($config){
            foreach ($config -> DESTINATION as $arrayConfig){
                $path = (string) $arrayConfig -> path;
            }
            return $path;
        }else{
            echo "export pesv2::get_path error";
        }
    }

    public function get_structure(){
        $globality      = array();
        $extensionArr   = array();
        $nameArr        = array();


        if (file_exists(
            $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
            . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
            . DIRECTORY_SEPARATOR . 'maarch_entreprise'
            . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
            . 'file_export.xml'
        )
        ) {
            $path =$_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
                . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
                . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                  . DIRECTORY_SEPARATOR . 'file_export.xml';
        } else {
            $path = 'apps' . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                  . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                  . 'file_export.xml';
        }
        $nameConfig = simplexml_load_file($path);
        if ($nameConfig) {
            foreach ($nameConfig -> NAME as $nameTag) {
                foreach ($nameTag -> ELEMENT as $item) {
                    $type = (string) $item -> type;
                    $value = (string) $item -> value;
                    $filter = (string) $item -> filter;
                    $isCustom = (string) $item -> isCustom;
                    array_push(
                        $nameArr,
                        array(
                            'type' => $type,
                            'value' => $value,
                            'filter' => $filter,
                            'isCustom' => $isCustom
                        )
                    );
                }
            }
            foreach ($nameConfig->EXTENSION as $extensionTag) {
                foreach ($extensionTag -> ELEMENT as $item) {
                    $type = (string) $item -> type;
                    $value = (string) $item -> value;
                    $filter = (string) $item -> filter;
                    array_push(
                        $extensionArr,
                        array(
                            'type' => $type,
                            'value' => $value,
                            'filter' => $filter
                        )
                    );
                }
            }
            foreach($nameConfig -> SEPARATOR as $separatorTag){
                $separatorArr['value'] = (string) $separatorTag -> value;
            }
            array_push(
                $globality,
                array(
                    'ELEMENTS' => $nameArr,
                    'EXTENSION' => $extensionArr,
                    'SEPARATOR' => $separatorArr
                )
            );

            return $globality;
        } else {
            echo "export pesv2::get_structure error";
        }
    }

    public function get_authorized_attachment_type(){

        if (file_exists(
            $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
            . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
            . DIRECTORY_SEPARATOR . 'maarch_entreprise'
            . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
            . 'file_export.xml'
        )
        ) {
            $path =$_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
                . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
                . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . DIRECTORY_SEPARATOR . 'file_export.xml';
        } else {
            $path = 'apps' . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . 'file_export.xml';
        }
        $config = simplexml_load_file($path);
        if($config){
            foreach ($config -> ATTACHMENTS as $attachmentType){
                $attachmentTypes = (string) $attachmentType -> TYPES;
            }
            return $attachmentTypes;
        }else{
            echo "export pesv2::get authorized attachment error";
        }
    }

    public function get_status(){

        $statusArr = array();
        if (file_exists(
            $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
            . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
            . DIRECTORY_SEPARATOR . 'maarch_entreprise'
            . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
            . 'file_export.xml'
        )
        ) {
            $path =$_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
                . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
                . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . DIRECTORY_SEPARATOR . 'file_export.xml';
        } else {
            $path = 'apps' . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . 'file_export.xml';
        }
        $statusConfig = simplexml_load_file($path);
        if ($statusConfig) {
            foreach ($statusConfig->STATUS as $statusTag) {
                $statusArr['in'] = (string) $statusTag -> IN;
                $statusArr['out'] = (string) $statusTag -> OUT;
            }
            return $statusArr;
        }else {
            echo "export pesv2::get_status error";
        }
    }

    public function get_suffix_attachment(){

        if (file_exists(
            $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
            . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
            . DIRECTORY_SEPARATOR . 'maarch_entreprise'
            . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
            . 'file_export.xml'
        )
        ) {
            $path =$_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
                . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
                . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . DIRECTORY_SEPARATOR . 'file_export.xml';
        } else {
            $path = 'apps' . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . 'file_export.xml';
        }
        $config = simplexml_load_file($path);
        if($config){
            foreach ($config -> ATTACHMENTS as $attachmentSuffix){
                $attachmentSuffix = (string) $attachmentSuffix -> SUFFIX;
            }
            return $attachmentSuffix;
        }else{
            echo "export pesv2::get suffix attachment error";
        }
    }

    public function getSanitize(){

        if (file_exists(
            $_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
            . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
            . DIRECTORY_SEPARATOR . 'maarch_entreprise'
            . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
            . 'file_export.xml'
        )
        ) {
            $path =$_SESSION['config']['corepath'] . 'custom' . DIRECTORY_SEPARATOR
                . CoreConfigModel::getCustomId() . DIRECTORY_SEPARATOR . 'apps'
                . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . DIRECTORY_SEPARATOR . 'file_export.xml';
        } else {
            $path = 'apps' . DIRECTORY_SEPARATOR . 'maarch_entreprise'
                . DIRECTORY_SEPARATOR . 'xml' . DIRECTORY_SEPARATOR
                . 'file_export.xml';
        }
        $sanitizeSettings = simplexml_load_file($path);
        if ($sanitizeSettings) {
            $sanitizeArr = array();
            foreach ($sanitizeSettings->SANITIZER as $settings) {
                foreach ($settings -> ELEMENT as $item) {
                    array_push($sanitizeArr,
                        array(
                            'original'      => (string)$item -> ORIGINAL,
                            'replacement'   => (string) $item -> REPLACEMENT,
                        )
                    );
                }
            }
            return $sanitizeArr;
        }else {
            echo "export pesv2::get_status error";
        }

    }

    public function convert_database_field($nameArray, $resId, $view){
        for($i = 0; $i < count($nameArray); $i++){
            $isCustom = $nameArray[$i]['isCustom'];
            $type = $nameArray[$i]['type'];
            $value = $this->filter_filename($nameArray[$i]['value']);
            $filter = $nameArray[$i]['filter'];
            $dateFilter = array(
                'year'  => 'YYYY',
                'month' => 'MM',
                'day'   => 'Day',
                'full_date' => 'DDMMYYYY'
            );
            if($type == 'column'){
                if(isset($filter) && array_key_exists($filter, $dateFilter)){
                    $field = "to_char(doc_date,'" . $dateFilter[$filter] . "') as date";
                }else{
                    $field = $value;
                }

                $db = new \SrcCore\models\DatabaseModel();

                if ($isCustom == 'false') {
                    $stmt = $db::select([
                        'select' => [$field],
                        'table' => [$view],
                        'where' => ['res_id = ?'],
                        'data' => [$resId]
                    ])[0];
                    if ($value == 'date')
                        $stmt = $stmt['date'];
                    else
                        $stmt = $stmt[$field];
                }else{
                    $stmt = $db::select([
                        'select' => ["custom_fields#>>'{" . $field . "}' as custom"],
                        'table' => [$view],
                        'where' => ['res_id = ?'],
                        'data' => [$resId]
                    ])[0]['custom'];
                }
                $nameArray[$i]['value'] = $this->filter_filename(trim($stmt));
            }
        }
        return $nameArray;
    }

    public function convert_format($extensionArray, $resId, $view){
        if($extensionArray[0]['type'] == 'column'){
            $field = $extensionArray[0]['value'];
            $db = new \SrcCore\models\DatabaseModel();
            $stmt = $db::select([
                'select' => [$field],
                'table' => [$view],
                'where' => ['res_id = ?'],
                'data' => [$resId]
            ])[0][$field];

            $extensionArray[0]['value'] = $stmt;
        }
        return $extensionArray;
    }

    public function generate_name($resId, $view, $suffixAttachment = NULL, $attachmentNumber = '', $attachmentExtension = NULL){
        $tmp = $this->get_structure();
        $elements = $tmp[0]['ELEMENTS'];
        $extension = $tmp[0]['EXTENSION'];
        $separator = $tmp[0]['SEPARATOR'];

        $elements = $this -> convert_database_field($elements, $resId, $view);

        if(isset($attachmentExtension)){
            $extension[0] = array(
                'type'      => 'column',
                'value'     => $attachmentExtension,
                'filter'    => ''
            );
        }else {
            $extension = $this->convert_format($extension, $resId, $view);
        }

        $string = $this->convert_in_string($elements, $extension, $separator, $suffixAttachment, $attachmentNumber);

        return $string;
    }

    public function convert_in_string($elements, $extension, $separator, $suffixAttachment, $attachmentNumber){
        $tmpArray = array();
        foreach ($elements as $array) {
            array_push(
                $tmpArray,
                $array['value']
            );
        }
        $thisString = join($separator['value'], $tmpArray);
        if(isset($suffixAttachment)){
            $thisString .= $suffixAttachment . $attachmentNumber;
        }
        if(isset($extension)){
            $thisString .= '.' . $extension[0]['value'];
        }
        return $thisString;
    }

    private function filter_filename($name) {
        $name = str_replace(array_merge(
            array_map('chr', range(0, 31)),
            array('<', '>', ':', '"', '/', '\\', '|', '?', '*', "'")
        ), '', $name);
        $ext    = pathinfo($name, PATHINFO_EXTENSION);
        $name   = mb_strcut(pathinfo($name, PATHINFO_FILENAME), 0, 255 - ($ext ? strlen($ext) + 1 : 0), mb_detect_encoding($name)) . ($ext ? '.' . $ext : '');

        $sanitizeArr = $this->getSanitize();
        foreach($sanitizeArr as $settings){
            if(strpos($name, $settings['original'])){
                $name = str_replace($settings['original'], $settings['replacement'], $name);
            }   
        }

        return $name;
    }

    public function check_status($resId, $table){
        $db = new \SrcCore\models\DatabaseModel();
        $status = $this->get_status();
        $statusList = explode(',', $status['in']);

        $stmt = $db::select([
            'select' => ['status'],
            'table' => [$table],
            'where' => ['res_id = ?'],
            'data' => [$resId]
        ])[0]['status'];

        if(in_array($stmt, $statusList) || $status['in'] == ''){
            return true;
        }else return false;
    }

    public function retrieve_creation_date($resId, $table){
        $db = new \SrcCore\models\DatabaseModel();

        $stmt = $db::select([
            'select' => ['creation_date'],
            'table' => [$table],
            'where' => ['res_id = ?'],
            'data' => [$resId]
        ])[0]['creation_date'];

        return $stmt;
    }

    public function retrieve_attachments($resIdMaster, $table, $authorizedAttachmentType){
        $db = new \SrcCore\models\DatabaseModel();

        $where = ['res_id_master = ?', 'status not in (?)'];

        if(!empty($authorizedAttachmentType)){
            $authorizedAttachmentType = str_replace(",","','", $authorizedAttachmentType);
            array_push(
                $where,
                "attachment_type IN ('" . preg_replace('/\s+/', '', $authorizedAttachmentType) . "')"
            );
        }

        $stmt = $db::select([
            'select' => ['res_id'],
            'table' => [$table],
            'where' => $where,
            'data' => [$resIdMaster, array('DEL', 'OBS')]
        ]);

        return $stmt;
    }
}
