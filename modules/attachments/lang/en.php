<?php
/*
 *
 *   Copyright 2008-2012 Maarch
 *
 *   This file is part of Maarch Framework.
 *
 *   Maarch Framework is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Maarch Framework is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Maarch Framework.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined("_TITLE")) {
    define("_TITLE", "Title");
}
if (!defined("_ATTACHMENTS")) {
    define("_ATTACHMENTS", "Attachments");
}
if (!defined("_ATTACHMENTS_COMMENT")) {
    define("_ATTACHMENTS_COMMENT", "Attachments management");
}
if (!defined("_ATTACH_ANSWER")) {
    define("_ATTACH_ANSWER", "Attach an attachment");
}
if (!defined("_ATTACH")) {
    define("_ATTACH", "Attach an attachment");
}
if (!defined("_NEW_ATTACH_ADDED")) {
    define("_NEW_ATTACH_ADDED", "New attachment");
}
if (!defined("_CREATED")) {
    define("_CREATED", "Created on");
}
if (!defined("_BBY")) {
    define("_BBY", "By");
}
if (!defined("_EXP_DATE")) {
    define("_EXP_DATE", "Start date");
}
if (!defined("_CREATE_PJ")) {
    define("_CREATE_PJ", "Create an attachment");
}
if (!defined("_ATTACH_FILE")) {
    define("_ATTACH_FILE", "Attach a file");
}
if (!defined("_MODEL")) {
    define("_MODEL", "Model");
}
if (!defined("_CHOOSE_MODEL")) {
    define("_CHOOSE_MODEL", "Select the model");
}
if (!defined("_BACK_DATE")) {
    define("_BACK_DATE", "Return date");
}
if (!defined("_EFFECTIVE_DATE")) {
    define("_EFFECTIVE_DATE", "Effective date");
}
if (!defined("_ATTACHMENT_TYPES")) {
    define("_ATTACHMENT_TYPES", "Attachment type");
}
if (!defined("_RESPONSE_PROJECT")) {
    define("_RESPONSE_PROJECT", "Response project");
}
if (!defined("_SIGNED_RESPONSE")) {
    define("_SIGNED_RESPONSE", "Signed response");
}
if (!defined("_ROUTING")) {
    define("_ROUTING", "Flow slip");
}
if (!defined("_OUTGOING_MAIL_SIGNED")) {
    define("_OUTGOING_MAIL_SIGNED", "Signed start date");
}
if (!defined("_CONVERTED_PDF")) {
    define("_CONVERTED_PDF", "Converted PDF by the solution");
}
if (!defined("_PRINT_FOLDER")) {
    define("_PRINT_FOLDER", "Printed folder");
}
if (!defined("_OUTGOING_MAIL")) {
    define("_OUTGOING_MAIL", "Spontaneous start mail");
}
if (!defined("_A_PJ")) {
    define("_A_PJ", "Attachment");
}
if (!defined("_INCOMING_PJ")) {
    define("_INCOMING_PJ", "Incoming attachment");
}
if (!defined("_ENVELOPE")) {
    define("_ENVELOPE", "Envelope");
}
if (!defined("_CHOOSE_ATTACHMENT_TYPE")) {
    define("_CHOOSE_ATTACHMENT_TYPE", "Choose an attachment type");
}
if (!defined("_UPDATED_DATE")) {
    define("_UPDATED_DATE", "Updated on");
}
if (!defined("_FILE_MISSING")) {
    define("_FILE_MISSING", "The file is missing");
}
if (!defined("_FILE_EMPTY")) {
    define("_FILE_EMPTY", "THe file is empty");
}
if (!defined("_SHOW_PREVIOUS_VERSION")) {
    define("_SHOW_PREVIOUS_VERSION", "See previous versions");
}
if (!defined("_ATTACHEMENTS")) {
    define("_ATTACHEMENTS", "Attachments");
}
if (!defined("_WAYBILL")) {
    define("_WAYBILL", "Mailing slip");
}

if (!defined("_NO_PREVIEW_AVAILABLE")) {
    define("_NO_PREVIEW_AVAILABLE", "No preview available");
}

if (!defined("_FILE_HAS_NO_PDF")) {
    define("_FILE_HAS_NO_PDF", "PDF version does not exist for this file.");
}

/************** Error message **************/

if (!defined('_NO_RESPONSE_PROJECT')) {
    define('_NO_RESPONSE_PROJECT', 'The selected document doesn\'t have any project response. Please enter the informations manually');
}
