<?php
/*
 *
 *   Copyright 2008-2012 Maarch
 *
 *   This file is part of Maarch Framework.
 *
 *   Maarch Framework is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Maarch Framework is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Maarch Framework.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined("_TITLE")) {
    define("_TITLE", "Titre");
}
if (!defined("_ATTACHMENTS")) {
    define("_ATTACHMENTS", "Pièces jointes");
}
if (!defined("_ATTACHMENTS_COMMENT")) {
    define("_ATTACHMENTS_COMMENT", "Gestion des pièces jointes");
}
if (!defined("_ATTACH_ANSWER")) {
    define("_ATTACH_ANSWER", "Attacher une pièce jointe");
}
if (!defined("_ATTACH")) {
    define("_ATTACH", "Attacher PJ");
}
if (!defined("_NEW_ATTACH_ADDED")) {
    define("_NEW_ATTACH_ADDED", "Nouvelle pièce jointe");
}
if (!defined("_CREATED")) {
    define("_CREATED", "Créé le");
}
if (!defined("_BBY")) {
    define("_BBY", "Par");
}
if (!defined("_EXP_DATE")) {
    define("_EXP_DATE", "Date de départ");
}
if (!defined("_CREATE_PJ")) {
    define("_CREATE_PJ", "Créer une pièce jointe");
}
if (!defined("_ATTACH_FILE")) {
    define("_ATTACH_FILE", "Attacher une pièce");
}
if (!defined("_MODEL")) {
    define("_MODEL", "Modèle");
}
if (!defined("_CHOOSE_MODEL")) {
    define("_CHOOSE_MODEL", "Sélectionnez le modèle");
}
if (!defined("_BACK_DATE")) {
    define("_BACK_DATE", "Date de retour");
}
if (!defined("_EFFECTIVE_DATE")) {
    define("_EFFECTIVE_DATE", "Date de retour effective");
}
if (!defined("_ATTACHMENT_TYPES")) {
    define("_ATTACHMENT_TYPES", "Type d'attachement");
}
if (!defined("_RESPONSE_PROJECT")) {
    define("_RESPONSE_PROJECT", "Projet de réponse");
}
if (!defined("_SIGNED_RESPONSE")) {
    define("_SIGNED_RESPONSE", "Réponse signée");
}
if (!defined("_ROUTING")) {
    define("_ROUTING", "Fiche de circulation");
}
if (!defined("_OUTGOING_MAIL_SIGNED")) {
    define("_OUTGOING_MAIL_SIGNED", "Courrier départ signé");
}
if (!defined("_CONVERTED_PDF")) {
    define("_CONVERTED_PDF", "PDF converti par la solution");
}
if (!defined("_PRINT_FOLDER")) {
    define("_PRINT_FOLDER", "Dossier imprimé");
}
if (!defined("_OUTGOING_MAIL")) {
    define("_OUTGOING_MAIL", "Courrier départ spontané");
}
if (!defined("_A_PJ")) {
    define("_A_PJ", "Pièce jointe");
}
if (!defined("_INCOMING_PJ")) {
    define("_INCOMING_PJ", "Pièce jointe capturée");
}
if (!defined("_ENVELOPE")) {
    define("_ENVELOPE", "Enveloppe");
}
if (!defined("_CHOOSE_ATTACHMENT_TYPE")) {
    define("_CHOOSE_ATTACHMENT_TYPE", "Choisissez un type d'attachement");
}
if (!defined("_UPDATED_DATE")) {
    define("_UPDATED_DATE", "Mis à jour le");
}
if (!defined("_FILE_MISSING")) {
    define("_FILE_MISSING", "Le fichier est manquant");
}
if (!defined("_FILE_EMPTY")) {
    define("_FILE_EMPTY", "Le fichier est vide");
}
if (!defined("_SHOW_PREVIOUS_VERSION")) {
    define("_SHOW_PREVIOUS_VERSION", "Voir les versions précédentes");
}
if (!defined("_ATTACHEMENTS")) {
    define("_ATTACHEMENTS", "Attachements");
}
if (!defined("_WAYBILL")) {
    define("_WAYBILL", "Bordereau d'envoi");
}

if (!defined("_NO_PREVIEW_AVAILABLE")) {
    define("_NO_PREVIEW_AVAILABLE", "Aucun aperçu disponible");
}

if (!defined("_FILE_HAS_NO_PDF")) {
    define("_FILE_HAS_NO_PDF", "La version PDF du fichier n'existe pas.");
}

/************** Message d'erreur **************/

if (!defined('_NO_RESPONSE_PROJECT')) {
    define('_NO_RESPONSE_PROJECT', 'Le courrier selectionné ne contient aucun projet de réponse. Veuillez entrer les informations manuellement');
}
