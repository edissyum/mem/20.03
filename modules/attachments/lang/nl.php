<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */

if (!defined('_TITLE')) {
    define('_TITLE', 'Titel');
}
if (!defined('_ATTACHMENTS')) {
    define('_ATTACHMENTS', 'Bijlagen');
}
if (!defined('_ATTACHMENTS_COMMENT')) {
    define('_ATTACHMENTS_COMMENT', 'Beheer van de bijlagen');
}
if (!defined('_ATTACH_ANSWER')) {
    define('_ATTACH_ANSWER', 'Een bijlage bijvoegen');
}
if (!defined('_ATTACH')) {
    define('_ATTACH', 'Bijlage bijvoegen');
}
if (!defined('_NEW_ATTACH_ADDED')) {
    define('_NEW_ATTACH_ADDED', 'Nieuwe bijlage');
}

if (!defined('_CREATED')) {
    define('_CREATED', 'Aangemaakt op');
}
if (!defined('_BBY')) {
    define('_BBY', 'Door');
}
if (!defined('_EXP_DATE')) {
    define('_EXP_DATE', 'Vertrekdatum');
}
if (!defined('_CREATE_PJ')) {
    define('_CREATE_PJ', 'Een bijlage aanmaken');
}
if (!defined('_ATTACH_FILE')) {
    define('_ATTACH_FILE', 'Een stuk bijvoegen');
}
if (!defined('_MODEL')) {
    define('_MODEL', 'Model');
}
if (!defined('_CHOOSE_MODEL')) {
    define('_CHOOSE_MODEL', 'Selecteer het model');
}
if (!defined('_BACK_DATE')) {
    define('_BACK_DATE', 'Datum van terugkeer');
}
if (!defined('_EFFECTIVE_DATE')) {
    define('_EFFECTIVE_DATE', 'Effectieve datum van terugkeer');
}
if (!defined('_ATTACHMENT_TYPES')) {
    define('_ATTACHMENT_TYPES', 'Type bijlage');
}
if (!defined('_RESPONSE_PROJECT')) {
    define('_RESPONSE_PROJECT', 'Antwoordontwerp');
}
if (!defined('_SIGNED_RESPONSE')) {
    define('_SIGNED_RESPONSE', 'Ondertekend antwoord');
}
if (!defined('_ROUTING')) {
    define('_ROUTING', 'Circulatiefiche');
}
if (!defined('_OUTGOING_MAIL_SIGNED')) {
    define('_OUTGOING_MAIL_SIGNED', 'Brief vertrek ondertekend');
}
if (!defined('_CONVERTED_PDF')) {
    define('_CONVERTED_PDF', 'PDF geconverteerd door de oplossing');
}
if (!defined('_PRINT_FOLDER')) {
    define('_PRINT_FOLDER', 'Afgedrukte map');
}
if (!defined('_OUTGOING_MAIL')) {
    define('_OUTGOING_MAIL', 'Brief spontaan vertrek');
}
if (!defined('_A_PJ')) {
    define('_A_PJ', 'Bijlage');
}
if (!defined('_INCOMING_PJ')) {
    define('_INCOMING_PJ', 'Bijlage vastgelegd');
}
if (!defined('_ENVELOPE')) {
    define('_ENVELOPE', 'Envelop');
}
if (!defined('_CHOOSE_ATTACHMENT_TYPE')) {
    define('_CHOOSE_ATTACHMENT_TYPE', 'Kies een type bijlage');
}
if (!defined('_UPDATED_DATE')) {
    define('_UPDATED_DATE', 'Bijgewerkt op');
}
if (!defined('_FILE_MISSING')) {
    define('_FILE_MISSING', 'Het bestand ontbreekt');
}
if (!defined('_FILE_EMPTY')) {
    define('_FILE_EMPTY', 'Het bestand is leeg');
}
if (!defined('_SHOW_PREVIOUS_VERSION')) {
    define('_SHOW_PREVIOUS_VERSION', 'De vorige versies bekijken');
}
if (!defined('_ATTACHEMENTS')) {
    define('_ATTACHEMENTS', 'Bijlagen');
}
if (!defined('_WAYBILL')) {
    define('_WAYBILL', 'Verzendingsborderel');
}
if (!defined('_NO_PREVIEW_AVAILABLE')) {
    define('_NO_PREVIEW_AVAILABLE', 'Geen overzicht beschikbaar');
}
if (!defined('_FILE_HAS_NO_PDF')) {
    define('_FILE_HAS_NO_PDF', 'Er bestaat geen PDF-versie van het bestand.');
}
if (!defined('_NO_RESPONSE_PROJECT')) {
    define('_NO_RESPONSE_PROJECT', 'De geselecteerde brief bevat geen enkel antwoordontwerp. Geef de informatie manueel in');
}