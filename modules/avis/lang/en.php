<?php
if (!defined("_AVIS")) {
    define("_AVIS", "recommendation");
}

if (!defined("_AVIS_WORKFLOW")) {
    define("_AVIS_WORKFLOW", "recommendation workflow");
}

if (!defined("_AVIS_WORKFLOW_COMMENT")) {
    define("_AVIS_WORKFLOW_COMMENT", "recommendation workflow management");
}

if (!defined("_SEND_DOCS_TO_RECOMMENDATION")) {
    define("_SEND_DOCS_TO_RECOMMENDATION", "Sent a recommendation request (parallel)");
}

if (!defined("_SEND_DOCS_TO_RECOMMENDATION_DESC")) {
    define("_SEND_DOCS_TO_RECOMMENDATION_DESC", "Opens a modal to add or remove people in notification in the mailing list.");
}

if (!defined("_VALIDATE_RECOMMENDATION")) {
    define("_VALIDATE_RECOMMENDATION", "Validate the recommendation request(parallel)");
}

if (!defined("_VALIDATE_RECOMMENDATION_DESC")) {
    define("_VALIDATE_RECOMMENDATION_DESC", "Allows you to add the person who checked the request to the notification request note.");
}

if (!defined("_AVIS_USER")) {
    define("_AVIS_USER", "For recommendation");
}

if (!defined("_AVIS_USER_COPY")) {
    define("_AVIS_USER_COPY", "On copy (recommendation)");
}

if (!defined("_AVIS_USER_INFO")) {
    define("_AVIS_USER_INFO", "For information (recommendation)");
}

if (!defined("_OPINION_LIMIT_DATE")) {
    define("_OPINION_LIMIT_DATE", "Recommendation deadline");
}

if (!defined("_TO_AVIS")) {
    define("_TO_AVIS", "For recommendation");
}

if (!defined("_LAST_AVIS")) {
    define("_LAST_AVIS", "decision-maker counsellor");
}

if (!defined("_SEND_TO_AVIS_WF")) {
    define("_SEND_TO_AVIS_WF", "Request for a recommendation (sequential)");
}

if (!defined("_SEND_TO_AVIS_WF_DESC")) {
    define("_SEND_TO_AVIS_WF_DESC", "Opens a modal to configure a notification circuit for mail.");
}

if (!defined("_PROCEED_WORKFLOW_AVIS")) {
    define("_PROCEED_WORKFLOW_AVIS", "Issue an opinion (sequential)");
}

if (!defined("_PROCEED_WORKFLOW_AVIS_DESC")) {
    define("_PROCEED_WORKFLOW_AVIS_DESC", "Updates the notification date of the current advisor in the notification circuit ('process_date' in the listinstance table).");
}

if (!defined("_PROCEED_WORKFLOW_AVIS_SIMPLE")) {
    define("_PROCEED_WORKFLOW_AVIS_SIMPLE", "Issue an opinion (parallel)");
}

if (!defined("_PROCEED_WORKFLOW_AVIS_SIMPLE_DESC")) {
    define("_PROCEED_WORKFLOW_AVIS_SIMPLE_DESC", "Updates the notification date of the user present in the mail mailing list ('process_date' in the listinstance table).");
}

if (!defined("_AVIS_SENT")) {
    define("_AVIS_SENT", "Notice given");
}

if (!defined("_DOCUMENTS_LIST_WITH_AVIS")) {
    define("_DOCUMENTS_LIST_WITH_AVIS", "List of mails with notices");
}

if (!defined("_WRITTEN_BY")) {
    define("_WRITTEN_BY", "Written by");
}

if (!defined("_VALIDATE_BY")) {
    define("_VALIDATE_BY", "Validate by");
}
