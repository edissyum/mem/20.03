<?php
if (!defined("_AVIS")) {
    define("_AVIS", "avis");
}

if (!defined("_AVIS_WORKFLOW")) {
    define("_AVIS_WORKFLOW", "Circuit d'avis");
}

if (!defined("_AVIS_WORKFLOW_COMMENT")) {
    define("_AVIS_WORKFLOW_COMMENT", "Gestion du circuit d'avis");
}

if (!defined("_SEND_DOCS_TO_RECOMMENDATION")) {
    define("_SEND_DOCS_TO_RECOMMENDATION", "Envoyer une demande d'avis (parallèle)");
}

if (!defined("_SEND_DOCS_TO_RECOMMENDATION_DESC")) {
    define("_SEND_DOCS_TO_RECOMMENDATION_DESC", "Ouvre une modal permettant d'ajouter ou de supprimer des personnes en avis dans la liste de diffusion.");
}

if (!defined("_VALIDATE_RECOMMENDATION")) {
    define("_VALIDATE_RECOMMENDATION", "Valider la demande d'avis (parallèle)");
}

if (!defined("_VALIDATE_RECOMMENDATION_DESC")) {
    define("_VALIDATE_RECOMMENDATION_DESC", "Permet d'ajouter dans la note de demande avis la personne qui a contrôlé la demande.");
}

if (!defined("_AVIS_USER")) {
    define("_AVIS_USER", "Pour avis");
}

if (!defined("_AVIS_USER_COPY")) {
    define("_AVIS_USER_COPY", "En copie (avis)");
}

if (!defined("_AVIS_USER_INFO")) {
    define("_AVIS_USER_INFO", "Pour information (avis)");
}

if (!defined("_OPINION_LIMIT_DATE")) {
    define("_OPINION_LIMIT_DATE", "Date limite de l'avis");
}

if (!defined("_TO_AVIS")) {
    define("_TO_AVIS", "POUR AVIS");
}

if (!defined("_LAST_AVIS")) {
    define("_LAST_AVIS", "Conseiller décideur");
}

if (!defined("_SEND_TO_AVIS_WF")) {
    define("_SEND_TO_AVIS_WF", "Envoyer une demande d'avis (séquentiel)");
}

if (!defined("_SEND_TO_AVIS_WF_DESC")) {
    define("_SEND_TO_AVIS_WF_DESC", "Ouvre une modal permettant de configurer un circuit d'avis pour le courrier.");
}

if (!defined("_PROCEED_WORKFLOW_AVIS")) {
    define("_PROCEED_WORKFLOW_AVIS", "Emettre un avis (séquentiel)");
}

if (!defined("_PROCEED_WORKFLOW_AVIS_DESC")) {
    define("_PROCEED_WORKFLOW_AVIS_DESC", "Met à jour la date d'émission d'avis du conseiller actuel du circuit d'avis ('process_date' de la table listinstance).");
}

if (!defined("_PROCEED_WORKFLOW_AVIS_SIMPLE")) {
    define("_PROCEED_WORKFLOW_AVIS_SIMPLE", "Emettre un avis (parallèle)");
}

if (!defined("_PROCEED_WORKFLOW_AVIS_SIMPLE_DESC")) {
    define("_PROCEED_WORKFLOW_AVIS_SIMPLE_DESC", "Met à jour la date d'émission d'avis de l'utilisateur présent en avis dans la liste de diffusion du courrier ('process_date' de la table listinstance).");
}

if (!defined("_AVIS_SENT")) {
    define("_AVIS_SENT", "Avis donné");
}

if (!defined("_DOCUMENTS_LIST_WITH_AVIS")) {
    define("_DOCUMENTS_LIST_WITH_AVIS", "Liste des courriers avec avis");
}

if (!defined("_WRITTEN_BY")) {
    define("_WRITTEN_BY", "Rédigé par");
}

if (!defined("_VALIDATE_BY")) {
    define("_VALIDATE_BY", "Validé par");
}
