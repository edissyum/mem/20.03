<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */
if (!defined('_AVIS')) {
    define('_AVIS', 'bericht');
}
if (!defined('_AVIS_WORKFLOW')) {
    define('_AVIS_WORKFLOW', 'Berichtencircuit');
}
if (!defined('_AVIS_WORKFLOW_COMMENT')) {
    define('_AVIS_WORKFLOW_COMMENT', 'Beheer van een berichtencircuit');
}
if (!defined('_SEND_DOCS_TO_RECOMMENDATION')) {
    define('_SEND_DOCS_TO_RECOMMENDATION', 'Een (parallelle) berichtenvraag verzenden');
}
if (!defined('_SEND_DOCS_TO_RECOMMENDATION_DESC')) {
    define('_SEND_DOCS_TO_RECOMMENDATION_DESC', 'Opent een modaliteit die toelaat personen in het bericht, in de verdelingslijst toe te voegen of te verwijderen.');
}
if (!defined('_VALIDATE_RECOMMENDATION')) {
    define('_VALIDATE_RECOMMENDATION', 'Een (parallelle) berichtenvraag bevestigen');
}
if (!defined('_VALIDATE_RECOMMENDATION_DESC')) {
    define('_VALIDATE_RECOMMENDATION_DESC', 'Om in de nota van de berichtaanvraag de persoon toe te voegen die de vraag gecontroleerd heeft.');
}
if (!defined('_AVIS_USER')) {
    define('_AVIS_USER', 'Voor bericht');
}
if (!defined('_AVIS_USER_COPY')) {
    define('_AVIS_USER_COPY', 'In kopie (bericht)');
}
if (!defined('_AVIS_USER_INFO')) {
    define('_AVIS_USER_INFO', 'Ter informatie (bericht)');
}
if (!defined('_OPINION_LIMIT_DATE')) {
    define('_OPINION_LIMIT_DATE', 'Berichtendeadline');
}
if (!defined('_TO_AVIS')) {
    define('_TO_AVIS', 'VOOR BERICHT');
}
if (!defined('_LAST_AVIS')) {
    define('_LAST_AVIS', 'Beslissende consultant');
}
if (!defined('_SEND_TO_AVIS_WF')) {
    define('_SEND_TO_AVIS_WF', 'Een (sequentiële) berichtenvraag verzenden');
}
if (!defined('_SEND_TO_AVIS_WF_DESC')) {
    define('_SEND_TO_AVIS_WF_DESC', 'Opent een modaliteit die een berichtencircuit voor het brief kan configureren.');
}
if (!defined('_PROCEED_WORKFLOW_AVIS')) {
    define('_PROCEED_WORKFLOW_AVIS', 'Een bericht uitzenden (sequentieel)');
}
if (!defined('_PROCEED_WORKFLOW_AVIS_DESC')) {
    define('_PROCEED_WORKFLOW_AVIS_DESC', 'Werkt de uitgiftedatum bij van het bericht van de huidige consultant van het berichtencircuit (\'process_date\' van de tabel listinstance).');
}
if (!defined('_PROCEED_WORKFLOW_AVIS_SIMPLE')) {
    define('_PROCEED_WORKFLOW_AVIS_SIMPLE', 'Een bericht uitzenden (parallel)');
}
if (!defined('_PROCEED_WORKFLOW_AVIS_SIMPLE_DESC')) {
    define('_PROCEED_WORKFLOW_AVIS_SIMPLE_DESC', 'Werkt de uitgiftedatum bij van het bericht van de gebruiker die aanwezig is in het bericht in de verdelingslijst van het brief (\'process_date\' van de tabel listinstance).');
}
if (!defined('_AVIS_SENT')) {
    define('_AVIS_SENT', 'Gegeven bericht');
}
if (!defined('_DOCUMENTS_LIST_WITH_AVIS')) {
    define('_DOCUMENTS_LIST_WITH_AVIS', 'Lijst van de brieven met berichten');
}
if (!defined('_WRITTEN_BY')) {
    define('_WRITTEN_BY', 'Opgesteld door');
}
if (!defined('_VALIDATE_BY')) {
    define('_VALIDATE_BY', 'Gevalideerd door');
}
