<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */

/*********************** SERVICES ***********************************/
if (!defined('_DIFFUSION_LIST')) {
    define('_DIFFUSION_LIST', 'Verdelingslijst');
}
if (!defined('_BASKET')) {
    define('_BASKET', 'Bakje');
}
if (!defined('_BASKETS_COMMENT')) {
    define('_BASKETS_COMMENT', 'Bakjes');
}
if (!defined('_THE_BASKET')) {
    define('_THE_BASKET', 'Het bakje');
}
if (!defined('_THE_ID')) {
    define('_THE_ID', 'De gebruikersnaam');
}
if (!defined('_THE_DESC')) {
    define('_THE_DESC', 'De beschrijving');
}
if (!defined('_DELETED_BASKET')) {
    define('_DELETED_BASKET', 'Bakje verwijderd');
}
if (!defined('_BASKET_DELETION')) {
    define('_BASKET_DELETION', 'Verwijdering van het bakje');
}
if (!defined('_BASKET_AUTORIZATION')) {
    define('_BASKET_AUTORIZATION', 'Toelating van het bakje');
}
if (!defined('_BASKET_SUSPENSION')) {
    define('_BASKET_SUSPENSION', 'Opschorting van het bakje');
}
if (!defined('_AUTORIZED_BASKET')) {
    define('_AUTORIZED_BASKET', 'Bakje toegelaten');
}
if (!defined('_SUSPENDED_BASKET')) {
    define('_SUSPENDED_BASKET', 'Bakje opgeschort');
}
if (!defined('_NO_BASKET_DEFINED_FOR_YOU')) {
    define('_NO_BASKET_DEFINED_FOR_YOU', 'Geen enkel bakje bepaald voor deze gebruiker');
}
if (!defined('_BASKET_VISIBLE')) {
    define('_BASKET_VISIBLE', 'Bakje zichtbaar');
}
if (!defined('_BASKETS_LIST')) {
    define('_BASKETS_LIST', 'Lijst van de bakjes');
}
if (!defined('_CHOOSE_BASKET')) {
    define('_CHOOSE_BASKET', 'Kies een bakje');
}
if (!defined('_PROCESS_BASKET')) {
    define('_PROCESS_BASKET', 'Uw te verwerken post');
}
if (!defined('_VALIDATION_BASKET')) {
    define('_VALIDATION_BASKET', 'Uw te bevestigen post');
}
if (!defined('_MANAGE_BASKETS')) {
    define('_MANAGE_BASKETS', 'De bakjes beheren');
}
if (!defined('_MANAGE_BASKETS_APP')) {
    define('_MANAGE_BASKETS_APP', 'De bakjes van de applicatie beheren');
}
if (!defined('_ALL_BASKETS')) {
    define('_ALL_BASKETS', 'Alle bakjes');
}
if (!defined('_BASKET_LIST')) {
    define('_BASKET_LIST', 'Lijst van de bakjes');
}
if (!defined('_ADD_BASKET')) {
    define('_ADD_BASKET', 'Een bakje toevoegen');
}
if (!defined('_BASKET_ADDITION')) {
    define('_BASKET_ADDITION', 'Toevoeging van een bakje');
}
if (!defined('_BASKET_MODIFICATION')) {
    define('_BASKET_MODIFICATION', 'Wijziging van een bakje');
}
if (!defined('_BASKET_VIEW')) {
    define('_BASKET_VIEW', 'Zicht op de tabel');
}
if (!defined('_MODIFY_BASKET')) {
    define('_MODIFY_BASKET', 'Het bakje wijzigen');
}
if (!defined('_DEL_GROUPS')) {
    define('_DEL_GROUPS', 'Groep(en) verwijderen');
}
if (!defined('_BASKET_NOT_USABLE')) {
    define('_BASKET_NOT_USABLE', 'Geen groep gekoppeld (het bakje kan momenteel niet gebruikt worden)');
}
if (!defined('_ADD_TO_BASKET')) {
    define('_ADD_TO_BASKET', 'Het bakje koppelen');
}
if (!defined('_NO_REDIRECT_RIGHT')) {
    define('_NO_REDIRECT_RIGHT', 'U hebt geen omleidingsrechten voor dit bakje');
}
if (!defined('_CHOOSE_DEPARTMENT_FIRST')) {
    define('_CHOOSE_DEPARTMENT_FIRST', 'U moet eerst een dienst kiezen vooraleer toegang te krijgen tot de verdelingslijst');
}
if (!defined('_NO_LIST_DEFINED__FOR_THIS_MAIL')) {
    define('_NO_LIST_DEFINED__FOR_THIS_MAIL', 'Er is geen enkele lijst voor deze brief bepaald');
}
if (!defined('_NO_LIST_DEFINED__FOR_THIS_DEPARTMENT')) {
    define('_NO_LIST_DEFINED__FOR_THIS_DEPARTMENT', 'Er is geen enkele lijst voor deze dienst bepaald');
}
if (!defined('_NO_LIST_DEFINED')) {
    define('_NO_LIST_DEFINED', 'Geen lijst gedefinieerd');
}
if (!defined('_REDIRECT_MAIL')) {
    define('_REDIRECT_MAIL', 'Omleiding van het brief');
}
if (!defined('_REDIRECT_TO_OTHER_DEP')) {
    define('_REDIRECT_TO_OTHER_DEP', 'Naar een andere dienst');
}
if (!defined('_REDIRECT_TO_USER')) {
    define('_REDIRECT_TO_USER', 'Naar een gebruiker');
}
if (!defined('_LETTER_SERVICE_REDIRECT')) {
    define('_LETTER_SERVICE_REDIRECT', 'Omleiden naar de uitzendende dienst');
}
if (!defined('_LETTER_SERVICE_REDIRECT_VALIDATION')) {
    define('_LETTER_SERVICE_REDIRECT_VALIDATION', 'Wil u echt omleiden naar de uitzendende dienst');
}
if (!defined('_DOC_REDIRECT_TO_SENDER_ENTITY')) {
    define('_DOC_REDIRECT_TO_SENDER_ENTITY', 'Document omgeleid naar de uitzendende dienst');
}
if (!defined('_DOC_REDIRECT_TO_ENTITY')) {
    define('_DOC_REDIRECT_TO_ENTITY', 'Document omgeleid naar de dienst');
}
if (!defined('_DOC_REDIRECT_TO_USER')) {
    define('_DOC_REDIRECT_TO_USER', 'Document omgeleid naar de gebruiker');
}
if (!defined('_WELCOME_DIFF_LIST')) {
    define('_WELCOME_DIFF_LIST', 'Welkom in het verdelingstool van de brieven');
}
if (!defined('_START_DIFF_EXPLANATION')) {
    define('_START_DIFF_EXPLANATION', 'Om de verdeling te starten, gebruik de navigatie per dienst of per gebruiker hieronder');
}
if (!defined('_ADD_USER_TO_LIST_EXPLANATION')) {
    define('_ADD_USER_TO_LIST_EXPLANATION', 'Om een gebruiker aan de verdelingslijst toe te voegen');
}
if (!defined('_REMOVE_USER_FROM_LIST_EXPLANATION')) {
    define('_REMOVE_USER_FROM_LIST_EXPLANATION', 'Om een gebruiker van de verdelingslijst te verwijderen');
}
if (!defined('_TO_MODIFY_LIST_ORDER_EXPLANATION')) {
    define('_TO_MODIFY_LIST_ORDER_EXPLANATION', 'Om de toekenningsvolgorde van een brief aan de gebruikers te wijzigen, gebruik de iconen');
}
if (!defined('_AND')) {
    define('_AND', 'en');
}
if (!defined('_LINKED_DIFF_LIST')) {
    define('_LINKED_DIFF_LIST', 'Gekoppelde verdelingslijst');
}
if (!defined('_NO_LINKED_DIFF_LIST')) {
    define('_NO_LINKED_DIFF_LIST', 'Geen lijst gekoppeld');
}
if (!defined('_CREATE_LIST')) {
    define('_CREATE_LIST', 'Een verdelingslijst aanmaken ');
}
if (!defined('_MODIFY_LIST')) {
    define('_MODIFY_LIST', 'De lijst wijzigen');
}
if (!defined('_THE_ENTITY_DO_NOT_CONTAIN_DIFF_LIST')) {
    define('_THE_ENTITY_DO_NOT_CONTAIN_DIFF_LIST', 'De geselecteerde dienst heeft geen model van gekoppelde verdelingslijst');
}
if (!defined('_MANAGE_MODEL_LIST_TITLE')) {
    define('_MANAGE_MODEL_LIST_TITLE', 'Aanmaak / Wijziging Model van verdelingslijst');
}
if (!defined('_SORT_BY')) {
    define('_SORT_BY', 'Sorteren op');
}
if (!defined('_WELCOME_MODEL_LIST_TITLE')) {
    define('_WELCOME_MODEL_LIST_TITLE', 'Welkom in het aanmaaktool van het model van de verdelingslijst');
}
if (!defined('_MODEL_LIST_EXPLANATION1')) {
    define('_MODEL_LIST_EXPLANATION1', 'Om de aanmaak te starten, gebruik de navigatie per dienst of per gebruiker hierboven');
}
if (!defined('_VALID_LIST')) {
    define('_VALID_LIST', 'De lijst bevestigen');
}
if (!defined('_COPY_LIST')) {
    define('_COPY_LIST', 'Lijst van de brieven in kopie');
}
if (!defined('_PROCESS_LIST')) {
    define('_PROCESS_LIST', 'Lijst van de te verwerken brieven');
}
if (!defined('_CLICK_LINE_TO_PROCESS')) {
    define('_CLICK_LINE_TO_PROCESS', 'Klik op een lijn om te verwerken');
}
if (!defined('_REDIRECT_TO_SENDER_ENTITY')) {
    define('_REDIRECT_TO_SENDER_ENTITY', 'Omleiden naar de uitzendende dienst');
}
if (!defined('_CHOOSE_DEPARTMENT')) {
    define('_CHOOSE_DEPARTMENT', 'Kies een dienst');
}
if (!defined('_ENTITY_UPDATE')) {
    define('_ENTITY_UPDATE', 'Dienst bijgewerkt');
}
if (!defined('_MY_ABS')) {
    define('_MY_ABS', 'Mijn afwezigheden beheren');
}
if (!defined('_ADMIN_ABS')) {
    define('_ADMIN_ABS', 'De afwezigheden beheren.');
}
if (!defined('_ACTIONS_DONE')) {
    define('_ACTIONS_DONE', 'Acties uitgevoerd op');
}
if (!defined('_PROCESSED_MAIL')) {
    define('_PROCESSED_MAIL', 'Verwerkte brieven');
}
if (!defined('_INDEXED_MAIL')) {
    define('_INDEXED_MAIL', 'Geïndexeerde brieven');
}
if (!defined('_MISSING')) {
    define('_MISSING', 'Afwezig');
}
if (!defined('_BACK_FROM_VACATION')) {
    define('_BACK_FROM_VACATION', 'bij terugkeer na afwezigheid');
}
if (!defined('_CHOOSE_PERSON_TO_REDIRECT')) {
    define('_CHOOSE_PERSON_TO_REDIRECT', 'Kies de persoon naar wie u deze brieven wil omleiden uit de bovenstaande lijst');
}
if (!defined('_TO_SELECT_USER')) {
    define('_TO_SELECT_USER', 'om een gebruiker te selecteren');
}
if (!defined('_DIFFUSION_DISTRIBUTION')) {
    define('_DIFFUSION_DISTRIBUTION', 'Verspreiding en verdeling van de briefwisseling');
}
if (!defined('_VALIDATED_ANSWERS')) {
    define('_VALIDATED_ANSWERS', 'DGS Gevalideerde antwoorden');
}
if (!defined('_REJECTED_ANSWERS')) {
    define('_REJECTED_ANSWERS', 'DGS Verworpen antwoorden');
}
if (!defined('_MUST_HAVE_DIFF_LIST')) {
    define('_MUST_HAVE_DIFF_LIST', 'U moet een verdelingslijst bepalen');
}
if (!defined('_ASSOCIATED_STATUS')) {
    define('_ASSOCIATED_STATUS', 'Gekoppelde status');
}
if (!defined('_SYSTEM_ACTION')) {
    define('_SYSTEM_ACTION', 'Actie systeem');
}
if (!defined('_ASSOCIATED_ACTIONS')) {
    define('_ASSOCIATED_ACTIONS', 'Acties mogelijk op de resultaatpagina');
}
if (!defined('_NO_ACTIONS_DEFINED')) {
    define('_NO_ACTIONS_DEFINED', 'Geen actie gedefinieerd');
}
if (!defined('_CONFIG')) {
    define('_CONFIG', '(instellen)');
}
if (!defined('_CONFIG_ACTION')) {
    define('_CONFIG_ACTION', 'Instelling van de actie');
}
if (!defined('_IN_ACTION')) {
    define('_IN_ACTION', 'in de actie');
}
if (!defined('_USE_ONE')) {
    define('_USE_ONE', 'Actie beschikbaar in de actiepagina');
}
if (!defined('_MUST_CHOOSE_WHERE_USE_ACTION')) {
    define('_MUST_CHOOSE_WHERE_USE_ACTION', 'U moet bepalen waar u een actie wil gebruiken');
}
if (!defined('_MUST_CHOOSE_DEP')) {
    define('_MUST_CHOOSE_DEP', 'U moet een dienst selecteren!');
}
if (!defined('_MUST_CHOOSE_USER')) {
    define('_MUST_CHOOSE_USER', 'U moet een gebruiker selecteren!');
}
if (!defined('_REDIRECT_TO_DEP_OK')) {
    define('_REDIRECT_TO_DEP_OK', 'naar een dienst');
}
if (!defined('_VIEW_BASKETS')) {
    define('_VIEW_BASKETS', 'Mijn bakjes');
}
if (!defined('_VIEW_BASKETS_DESC')) {
    define('_VIEW_BASKETS_DESC', 'Mijn bakjes');
}
if (!defined('_VIEW_BASKETS_TITLE')) {
    define('_VIEW_BASKETS_TITLE', 'Mijn bakjes');
}
if (!defined('_INVOICE_LIST_TO_VAL')) {
    define('_INVOICE_LIST_TO_VAL', 'Te bevestigen facturen');
}
if (!defined('_POSTINDEXING_LIST')) {
    define('_POSTINDEXING_LIST', 'Te videotapen documenten');
}
if (!defined('_MY_BASKETS')) {
    define('_MY_BASKETS', 'Mijn bakjes');
}
if (!defined('_REDIRECT_MY_BASKETS')) {
    define('_REDIRECT_MY_BASKETS', 'De bakjes doorsturen');
}
if (!defined('_NAME')) {
    define('_NAME', 'Naam');
}
if (!defined('_CHOOSE_USER_TO_REDIRECT')) {
    define('_CHOOSE_USER_TO_REDIRECT', 'U moet minstens één van de bakjes naar een gebruiker omleiden.');
}
if (!defined('_FORMAT_ERROR_ON_USER_FIELD')) {
    define('_FORMAT_ERROR_ON_USER_FIELD', 'Een veld heeft het verkeerde formaat: Naam Voornaam (Gebruikersnaam)');
}
if (!defined('_BASKETS_OWNER_MISSING')) {
    define('_BASKETS_OWNER_MISSING', 'De eigenaar van de bakjes is niet bepaald.');
}
if (!defined('_FORM_ERROR')) {
    define('_FORM_ERROR', 'Fout in de overdracht van het formulier...');
}
if (!defined('_ABS_LOG_OUT')) {
    define('_ABS_LOG_OUT', 'Indien u zich opnieuw verbindt, zal de afwezigheidmodus uitgeschakeld worden.');
}
if (!defined('_ABS_USER')) {
    define('_ABS_USER', 'Gebruiker afwezig');
}
if (!defined('_ABSENCE')) {
    define('_ABSENCE', 'Afwezigheid');
}
if (!defined('_BASK_BACK')) {
    define('_BASK_BACK', 'Terug');
}
if (!defined('_CANCEL_ABS')) {
    define('_CANCEL_ABS', 'Annulering van afwezigheid');
}
if (!defined('_REALLY_CANCEL_ABS')) {
    define('_REALLY_CANCEL_ABS', 'Wil u de afwezigheid echt uitschakelen?');
}
if (!defined('_ABS_MODE')) {
    define('_ABS_MODE', 'Afwezigheidsbeheer');
}
if (!defined('_REALLY_ABS_MODE')) {
    define('_REALLY_ABS_MODE', 'Wil u echt in afwezigheidsmodus gaan?');
}
if (!defined('_DOCUMENTS_LIST_WITH_FILTERS')) {
    define('_DOCUMENTS_LIST_WITH_FILTERS', 'Lijst met filters');
}
if (!defined('_AUTHORISED_ENTITIES')) {
    define('_AUTHORISED_ENTITIES', 'Toegelaten dienstenlijst');
}
if (!defined('_ARCHIVE_LIST')) {
    define('_ARCHIVE_LIST', 'Lijst van archiefeenheden');
}
if (!defined('_COUNT_LIST')) {
    define('_COUNT_LIST', 'Lijst van de kopieën');
}
if (!defined('_FILTER_BY')) {
    define('_FILTER_BY', 'Filteren op');
}
if (!defined('_OTHER_BASKETS')) {
    define('_OTHER_BASKETS', 'Andere bakjes');
}
if (!defined('_SPREAD_SEARCH_TO_BASKETS')) {
    define('_SPREAD_SEARCH_TO_BASKETS', 'De opzoeking naar de bakjes uitbreiden');
}
if (!defined('_BASKET_WELCOME_TXT1')) {
    define('_BASKET_WELCOME_TXT1', 'Tijdens het surfen in de bakjes');
}
if (!defined('_BASKET_WELCOME_TXT2')) {
    define('_BASKET_WELCOME_TXT2', 'Klik op elk moment in de bovenstaande lijst <br/>om van bakje te wijzigen');
}
if (!defined('_VIEWED')) {
    define('_VIEWED', 'Gezien?');
}
if (!defined('_SEE_BASKETS_RELATED')) {
    define('_SEE_BASKETS_RELATED', 'De gekoppelde bakjes bekijken');
}
if (!defined('_GO_MANAGE_BASKET')) {
    define('_GO_MANAGE_BASKET', 'Wijzigen');
}
if (!defined('_WF')) {
    define('_WF', 'Workflow');
}
if (!defined('_POSITION')) {
    define('_POSITION', 'Positie');
}
if (!defined('_ADVANCE_TO')) {
    define('_ADVANCE_TO', 'Vooruitgaan naar');
}
if (!defined('_VALID_STEP')) {
    define('_VALID_STEP', 'De stap bevestigen');
}
if (!defined('_BACK_TO')) {
    define('_BACK_TO', 'Teruggaan naar');
}
if (!defined('_FORWARD_IN_THE_WF')) {
    define('_FORWARD_IN_THE_WF', 'Gaat vooruit in de workflow');
}
if (!defined('_BACK_IN_THE_WF')) {
    define('_BACK_IN_THE_WF', 'Gaat achteruit in de workflow');
}
if (!defined('_ITS_NOT_MY_TURN_IN_THE_WF')) {
    define('_ITS_NOT_MY_TURN_IN_THE_WF', 'Het is niet mijn beurt om te behandelen in de workflow');
}
if (!defined('_COMBINATED_ACTION')) {
    define('_COMBINATED_ACTION', 'Gecombineerde actie');
}
if (!defined('_END_OF_THE_WF')) {
    define('_END_OF_THE_WF', 'Einde van de workflow');
}
if (!defined('_ALL_BASKETS')) {
    define('_ALL_BASKETS', 'Mijn volledige perimeter');
}
if (!defined('_CHOOSE_DEPARTMENT_FIRST')) {
    define('_CHOOSE_DEPARTMENT_FIRST', 'U moet eerst een eenheid kiezen vooraleer toegang te krijgen tot de verdelingslijst');
}
if (!defined('_NO_LIST_DEFINED__FOR_THIS_DEPARTMENT')) {
    define('_NO_LIST_DEFINED__FOR_THIS_DEPARTMENT', 'Er is geen enkele lijst voor deze eenheid bepaald');
}
if (!defined('_REDIRECT_TO_OTHER_DEP')) {
    define('_REDIRECT_TO_OTHER_DEP', 'Naar een andere eenheid');
}
if (!defined('_LETTER_SERVICE_REDIRECT')) {
    define('_LETTER_SERVICE_REDIRECT', 'Omleiden naar de uitzendende eenheid');
}
if (!defined('_LETTER_SERVICE_REDIRECT_VALIDATION')) {
    define('_LETTER_SERVICE_REDIRECT_VALIDATION', 'Wilt u echt omleiden naar de uitzendende eenheid?');
}
if (!defined('_DOC_REDIRECT_TO_SENDER_ENTITY')) {
    define('_DOC_REDIRECT_TO_SENDER_ENTITY', 'Document omgeleid naar de uitzendende eenheid');
}
if (!defined('_DOC_REDIRECT_TO_ENTITY')) {
    define('_DOC_REDIRECT_TO_ENTITY', 'Document omgeleid naar de eenheid');
}
if (!defined('_START_DIFF_EXPLANATION')) {
    define('_START_DIFF_EXPLANATION', 'Om de verdeling te starten, gebruik de navigatie per eenheid of per gebruiker hieronder');
}
if (!defined('_THE_ENTITY_DO_NOT_CONTAIN_DIFF_LIST')) {
    define('_THE_ENTITY_DO_NOT_CONTAIN_DIFF_LIST', 'De geselecteerde eenheid heeft geen model van gekoppelde verdelingslijst');
}
if (!defined('_MODEL_LIST_EXPLANATION1')) {
    define('_MODEL_LIST_EXPLANATION1', 'Om de aanmaak te starten, gebruik de navigatie per eenheid of per gebruiker hierboven');
}
if (!defined('_REDIRECT_TO_SENDER_ENTITY')) {
    define('_REDIRECT_TO_SENDER_ENTITY', 'Omleiden naar de uitzendende eenheid');
}
if (!defined('_CHOOSE_DEPARTMENT')) {
    define('_CHOOSE_DEPARTMENT', 'Kies een eenheid');
}
if (!defined('_ENTITY_UPDATE')) {
    define('_ENTITY_UPDATE', 'Bijgewerkte eenheid');
}
if (!defined('_MISSING_ADVERT_01')) {
    define('_MISSING_ADVERT_01', 'Deze account staat momenteel in modus ‘afwezig’.');
}
if (!defined('_MUST_CHOOSE_DEP')) {
    define('_MUST_CHOOSE_DEP', 'U moet een eenheid selecteren!');
}
if (!defined('_REDIRECT_TO_DEP_OK')) {
    define('_REDIRECT_TO_DEP_OK', 'naar een eenheid');
}
if (!defined('_AUTHORISED_ENTITIES')) {
    define('_AUTHORISED_ENTITIES', 'Lijst van de toegelaten eenheden');
}
