<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */

 /******************** Content management  ************/

 if (!defined('_MAARCH_CM_APPLET')) {
     define('_MAARCH_CM_APPLET', 'Lancering bewerking van inhoud');
 }
 if (!defined('_NO_UPDATE')) {
     define('_NO_UPDATE', 'Geen update van de inhoud gevonden');
 }
 if (!defined('_UPDATE_OK')) {
     define('_UPDATE_OK', 'Update van inhoud uitgevoerd');
 }
 if (!defined('_NEW_ATTACHMENT_VERSION')) {
     define('_NEW_ATTACHMENT_VERSION', 'Antwoord op brief');
 }
 if (!defined('_VERSION')) {
     define('_VERSION', 'Versie');
 }
 if (!defined('_NEW_VERSION_ADDED')) {
     define('_NEW_VERSION_ADDED', 'Nieuwe versie toegevoegd');
 }
 if (!defined('_DONT_CLOSE')) {
     define('_DONT_CLOSE', 'Dit venster niet sluiten!');
 }
 if (!defined('_LOADING')) {
     define('_LOADING', 'Aan het laden ...');
 }
