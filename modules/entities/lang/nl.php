<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */

/*************************** Entites management *****************/
if (!defined('_ADD_ENTITY')) {
    define('_ADD_ENTITY', 'Toegevoegde eenheid');
}
if (!defined('_ENTITY_ADDITION')) {
    define('_ENTITY_ADDITION', 'Toevoeging van een eenheid');
}
if (!defined('_ENTITY_MODIFICATION')) {
    define('_ENTITY_MODIFICATION', 'Wijziging van een eenheid');
}
if (!defined('_ENTITY_AUTORIZATION')) {
    define('_ENTITY_AUTORIZATION', 'Toelating van een eenheid');
}
if (!defined('_ENTITY_SUSPENSION')) {
    define('_ENTITY_SUSPENSION', 'Opschorting van een eenheid');
}
if (!defined('_ENTITY_DELETION')) {
    define('_ENTITY_DELETION', 'Verwijdering van een eenheid');
}
if (!defined('_ENTITY_DELETED')) {
    define('_ENTITY_DELETED', 'Verwijderde eenheid');
}
if (!defined('_ENTITY_UPDATED')) {
    define('_ENTITY_UPDATED', 'Gewijzigde eenheid');
}
if (!defined('_ENTITY_AUTORIZED')) {
    define('_ENTITY_AUTORIZED', 'Toegelaten eenheid');
}
if (!defined('_ENTITY_SUSPENDED')) {
    define('_ENTITY_SUSPENDED', 'Opgeschorste eenheid');
}
if (!defined('_ENTITY')) {
    define('_ENTITY', 'Eenheid');
}
if (!defined('_ENTITIES')) {
    define('_ENTITIES', 'Eenheden');
}
if (!defined('_ENTITIES_COMMENT')) {
    define('_ENTITIES_COMMENT', 'Eenheden');
}
if (!defined('_ALL_ENTITIES')) {
    define('_ALL_ENTITIES', 'Alle eenheden');
}
if (!defined('_ENTITIES_LIST')) {
    define('_ENTITIES_LIST', 'Lijst van de eenheden');
}
if (!defined('_MANAGE_ENTITIES')) {
    define('_MANAGE_ENTITIES', 'De Eenheden beheren');
}
if (!defined('_ENTITY_MISSING')) {
    define('_ENTITY_MISSING', 'Deze eenheid bestaat niet');
}
if (!defined('_ENTITY_TREE')) {
    define('_ENTITY_TREE', 'Boomstructuur van de eenheden');
}
if (!defined('_ENTITY_TREE_DESC')) {
    define('_ENTITY_TREE_DESC', 'De boomstructuur van de eenheden bekijken');
}
if (!defined('_ENTITY_HAVE_CHILD')) {
    define('_ENTITY_HAVE_CHILD', 'deze eenheid heeft subeenheden');
}
if (!defined('_ENTITY_IS_RELATED')) {
    define('_ENTITY_IS_RELATED', 'Deze eenheid is aan gebruikers gekoppeld');
}
if (!defined('_TYPE')) {
    define('_TYPE', 'Type');
}
if (!defined('_ENTITY_USER_DESC')) {
    define('_ENTITY_USER_DESC', 'Eenheden en gebruikers met elkaar in relatie brengen');
}
if (!defined('_ENTITIES_USERS')) {
    define('_ENTITIES_USERS', 'Relatie eenheden - gebruikers');
}
if (!defined('_ENTITIES_USERS_LIST')) {
    define('_ENTITIES_USERS_LIST', 'Gebruikerslijst');
}
if (!defined('_USER_ENTITIES_TITLE')) {
    define('_USER_ENTITIES_TITLE', 'De gebruiker behoort tot de volgende eenheden');
}
if (!defined('_USER_ENTITIES_ADDITION')) {
    define('_USER_ENTITIES_ADDITION', 'Relatie Gebruiker  - Eenheden');
}
if (!defined('_USER_BELONGS_NO_ENTITY')) {
    define('_USER_BELONGS_NO_ENTITY', 'De gebruiker behoort tot geen enkele eenheid');
}
if (!defined('_CHOOSE_ONE_ENTITY')) {
    define('_CHOOSE_ONE_ENTITY', 'Kies minstens een eenheid');
}
if (!defined('_CHOOSE_ENTITY')) {
    define('_CHOOSE_ENTITY', 'Kies een eenheid');
}
if (!defined('_CHOOSE_PRIMARY_ENTITY')) {
    define('_CHOOSE_PRIMARY_ENTITY', 'Als primaire eenheid kiezen');
}
if (!defined('_PRIMARY_ENTITY')) {
    define('_PRIMARY_ENTITY', 'Primaire eenheid');
}
if (!defined('_DELETE_ENTITY')) {
    define('_DELETE_ENTITY', 'Eenhe(i)d(en) verwijderen');
}
if (!defined('USER_ADD_ENTITY')) {
    define('USER_ADD_ENTITY', 'Aan een eenheid toevoegen');
}
if (!defined('_ADD_TO_ENTITY')) {
    define('_ADD_TO_ENTITY', 'Aan een eenheid toevoegen');
}
if (!defined('_NO_ENTITY_SELECTED')) {
    define('_NO_ENTITY_SELECTED', 'Geen eenheid geselecteerd');
}
if (!defined('_NO_PRIMARY_ENTITY')) {
    define('_NO_PRIMARY_ENTITY', 'De primaire eenheid is verplicht');
}
if (!defined('_NO_ENTITIES_DEFINED_FOR_YOU')) {
    define('_NO_ENTITIES_DEFINED_FOR_YOU', 'Geen eenheid bepaald voor deze gebruiker');
}
if (!defined('_LABEL_MISSING')) {
    define('_LABEL_MISSING', 'Naam van de eenheid ontbreekt');
}
if (!defined('_SHORT_LABEL_MISSING')) {
    define('_SHORT_LABEL_MISSING', 'Korte naam van de eenheid ontbreekt');
}
if (!defined('_ID_MISSING')) {
    define('_ID_MISSING', 'De gebruikersnaam van de eenheid ontbreekt');
}
if (!defined('_TYPE_MISSING')) {
    define('_TYPE_MISSING', 'Het type eenheid is verplicht');
}
if (!defined('_PARENT_MISSING')) {
    define('_PARENT_MISSING', 'De bovenliggende eenheid is verplicht');
}
if (!defined('_ENTITY_UNKNOWN')) {
    define('_ENTITY_UNKNOWN', 'Onbekende eenheid');
}
if (!defined('_ENTITY_LABEL')) {
    define('_ENTITY_LABEL', 'Naam');
}
if (!defined('_SHORT_LABEL')) {
    define('_SHORT_LABEL', 'Korte naam');
}
if (!defined('_ENTITY_FULL_NAME')) {
    define('_ENTITY_FULL_NAME', 'Blok boomstructuur');
}
if (!defined('_ENTITY_ADR_1')) {
    define('_ENTITY_ADR_1', 'Adres 1');
}
if (!defined('_ENTITY_ADR_2')) {
    define('_ENTITY_ADR_2', 'Adres 2');
}
if (!defined('_ENTITY_ADR_3')) {
    define('_ENTITY_ADR_3', 'Adres 3');
}
if (!defined('_ENTITY_ZIPCODE')) {
    define('_ENTITY_ZIPCODE', 'Postcode');
}
if (!defined('_ENTITY_CITY')) {
    define('_ENTITY_CITY', 'Gemeente');
}
if (!defined('_ENTITY_COUNTRY')) {
    define('_ENTITY_COUNTRY', 'Land');
}
if (!defined('_ENTITY_EMAIL')) {
    define('_ENTITY_EMAIL', 'E-mail');
}
if (!defined('_ENTITY_BUSINESS')) {
    define('_ENTITY_BUSINESS', 'SIRET NR.');
}
if (!defined('_ENTITY_PARENT')) {
    define('_ENTITY_PARENT', 'Bovenliggende eenheid ');
}
if (!defined('_CHOOSE_ENTITY_PARENT')) {
    define('_CHOOSE_ENTITY_PARENT', 'Kies de bovenliggende eenheid');
}
if (!defined('_CHOOSE_FILTER_ENTITY')) {
    define('_CHOOSE_FILTER_ENTITY', 'Filteren op eenheid');
}
if (!defined('_CHOOSE_ENTITY_TYPE')) {
    define('_CHOOSE_ENTITY_TYPE', 'Kies het type eenheid');
}
if (!defined('_ENTITY_TYPE')) {
    define('_ENTITY_TYPE', 'Type eenheid');
}
if (!defined('_TO_USERS_OF_ENTITIES')) {
    define('_TO_USERS_OF_ENTITIES', 'Naar gebruikers van de diensten');
}
if (!defined('_ALL_ENTITIES')) {
    define('_ALL_ENTITIES', 'Alle eenheden');
}
if (!defined('_ENTITIES_JUST_BELOW')) {
    define('_ENTITIES_JUST_BELOW', 'Onmiddellijk onder de primaire eenheid');
}
if (!defined('_ALL_ENTITIES_BELOW')) {
    define('_ALL_ENTITIES_BELOW', 'Onder de primaire eenheid');
}
if (!defined('_ENTITIES_JUST_UP')) {
    define('_ENTITIES_JUST_UP', 'Onmiddellijk boven de primaire eenheid');
}
if (!defined('_ENTITIES_BELOW')) {
    define('_ENTITIES_BELOW', 'Onder al mijn eenheden');
}
if (!defined('_MY_ENTITIES')) {
    define('_MY_ENTITIES', 'Alle eenheden van de gebruiker');
}
if (!defined('_MY_PRIMARY_ENTITY')) {
    define('_MY_PRIMARY_ENTITY', 'Primaire eenheid');
}
if (!defined('_SAME_LEVEL_ENTITIES')) {
    define('_SAME_LEVEL_ENTITIES', 'Zelfde niveau van de primaire eenheid');
}
if (!defined('_INDEXING_ENTITIES')) {
    define('_INDEXING_ENTITIES', 'Indexeren voor de diensten');
}
if (!defined('_SEARCH_DIFF_LIST')) {
    define('_SEARCH_DIFF_LIST', 'Een dienst of een gebruiker zoeken');
}
if (!defined('_ADD_CC')) {
    define('_ADD_CC', 'In kopie toevoegen');
}
if (!defined('_TO_DEST')) {
    define('_TO_DEST', 'Ontvanger');
}
if (!defined('_NO_DIFF_LIST_ASSOCIATED')) {
    define('_NO_DIFF_LIST_ASSOCIATED', 'Geen verdelingslijst');
}
if (!defined('_PRINCIPAL_RECIPIENT')) {
    define('_PRINCIPAL_RECIPIENT', 'Hoofdbestemmeling');
}
if (!defined('_UPDATE_LIST_DIFF_IN_DETAILS')) {
    define('_UPDATE_LIST_DIFF_IN_DETAILS', 'De verdelingslijst vanuit de detailpagina’s updaten');
}
if (!defined('_UPDATE_LIST_DIFF')) {
    define('_UPDATE_LIST_DIFF', 'De verdelingslijst wijzigen');
}
if (!defined('_DIFF_LIST_COPY')) {
    define('_DIFF_LIST_COPY', 'Verdelingslijst');
}
if (!defined('_DIFF_LIST')) {
    define('_DIFF_LIST', 'Verdelingslijst');
}
if (!defined('_NO_USER')) {
    define('_NO_USER', 'Geen gebruiker');
}
if (!defined('_MUST_CHOOSE_DEST')) {
    define('_MUST_CHOOSE_DEST', 'U moet minstens één ontvanger selecteren');
}
if (!defined('_ENTITIES__DEL')) {
    define('_ENTITIES__DEL', 'Verwijderen');
}
if (!defined('_ENTITY_DELETION')) {
    define('_ENTITY_DELETION', 'Verwijdering van de eenheid');
}
if (!defined('_THERE_ARE_NOW')) {
    define('_THERE_ARE_NOW', 'Er zijn momenteel');
}
if (!defined('_DOC_IN_THE_DEPARTMENT')) {
    define('_DOC_IN_THE_DEPARTMENT', 'Brieven aan de eenheid gekoppeld');
}
if (!defined('_DEL_AND_REAFFECT')) {
    define('_DEL_AND_REAFFECT', 'Verwijderen EN opnieuw toewijzen');
}
if (!defined('_NO_REAFFECT')) {
    define('_NO_REAFFECT', 'Niet opnieuw toewijzen');
}
if (!defined('_THE_ENTITY')) {
    define('_THE_ENTITY', 'De eenheid');
}
if (!defined('_USERS_LINKED_TO')) {
    define('_USERS_LINKED_TO', 'gebruiker(s) aan de eenheid gekoppeld');
}
if (!defined('_ENTITY_MANDATORY_FOR_REDIRECTION')) {
    define('_ENTITY_MANDATORY_FOR_REDIRECTION', 'Eenheid verplicht om opnieuw toe te wijzen');
}
if (!defined('_WARNING_MESSAGE_DEL_ENTITY')) {
    define('_WARNING_MESSAGE_DEL_ENTITY', 'Waarschuwing :<br> De verwijdering van een eenheid leidt ertoe dat de brieven en de gebruikers aan een nieuwe eenheid worden toegewezen maar wijst eveneens de brieven in afwachting van verwerking, de modellen van de verdelingslijst en de antwoordmodellen toe aan de vervangingseenheid.');
}
if (!defined('_HELP_KEYWORD1')) {
    define('_HELP_KEYWORD1', 'Alle eenheden die aan de verbonden gebruiker gekoppeld zijn Bevat geen subeenheden');
}
if (!defined('_HELP_KEYWORD2')) {
    define('_HELP_KEYWORD2', 'primaire eenheid van de verbonden gebruiker');
}
if (!defined('_HELP_KEYWORD3')) {
    define('_HELP_KEYWORD3', 'Subeenheden van de argumentenlijst die ook @my_entities of @my_primary_entity kan zijn');
}
if (!defined('_HELP_KEYWORD4')) {
    define('_HELP_KEYWORD4', 'bovenliggende eenheid van de eenheid in het argument');
}
if (!defined('_HELP_KEYWORD5')) {
    define('_HELP_KEYWORD5', 'alle eenheden van hetzelfde niveau van de eenheid in het argument');
}
if (!defined('_HELP_KEYWORD6')) {
    define('_HELP_KEYWORD6', 'alle (actieve) eenheden');
}
if (!defined('_HELP_KEYWORD7')) {
    define('_HELP_KEYWORD7', 'subeenheden die direct onder (n-1) de in het argument gegeven eenheden liggen');
}
if (!defined('_HELP_KEYWORD8')) {
    define('_HELP_KEYWORD8', 'onderliggende eenheden van de in het argument gegeven eenheid tot op de diepte die in het tweede argument gevraagd wordt (of de vierkantswortel indien er geen enkel argument 2 geleverd werd)');
}
if (!defined('_HELP_KEYWORD9')) {
    define('_HELP_KEYWORD9', 'alle eenheden van het type dat in het argument voorkomt');
}
if (!defined('_HELP_KEYWORDS')) {
    define('_HELP_KEYWORDS', 'Hulp voor sleutelwoorden');
}
if (!defined('_HELP_KEYWORD_EXEMPLE_TITLE')) {
    define('_HELP_KEYWORD_EXEMPLE_TITLE', 'Voorbeeld in de bepaling van de veiligheid van een groep (where clause): toegangen op de assets met betrekking tot de belangrijkste dienst waartoe de verbonden gebruiker of de subdiensten van deze dienst behoren.');
}
if (!defined('_HELP_KEYWORD_EXEMPLE')) {
    define('_HELP_KEYWORD_EXEMPLE', 'where_clause : (DESTINATION = @my_primary_entity or DESTINATION in (@subentities[@my_primary_entity]))');
}
if (!defined('_HELP_BY_ENTITY')) {
    define('_HELP_BY_ENTITY', 'Sleutelwoorden van de Eenhedenmodule');
}
if (!defined('_BASKET_REDIRECTIONS_OCCURS_LINKED_TO')) {
    define('_BASKET_REDIRECTIONS_OCCURS_LINKED_TO', 'geval(len) van omleiding(en) van aan de eenheid gekoppeld(e) bakje(s)');
}
if (!defined('_TEMPLATES_LINKED_TO')) {
    define('_TEMPLATES_LINKED_TO', 'aan de eenheid gekoppeld(e) antwoordmodel(len)');
}
if (!defined('_LISTISTANCES_OCCURS_LINKED_TO')) {
    define('_LISTISTANCES_OCCURS_LINKED_TO', 'geval(len) van te verwerken brief(ven) of in kopie aan de eenheid gekoppeld');
}
if (!defined('_LISTMODELS_OCCURS_LINKED_TO')) {
    define('_LISTMODELS_OCCURS_LINKED_TO', 'verdelingsmodel gekoppeld aan de eenheid');
}
if (!defined('_CHOOSE_REPLACEMENT_DEPARTMENT')) {
    define('_CHOOSE_REPLACEMENT_DEPARTMENT', 'Kies een vervangende dienst');
}
if (!defined('_ENTITY_VOL_STAT')) {
    define('_ENTITY_VOL_STAT', 'Briefvolume per eenheid');
}
if (!defined('_ENTITY_VOL_STAT_DESC')) {
    define('_ENTITY_VOL_STAT_DESC', 'Om het aantal brieven weer te geven dat per dienst bewaard wordt.');
}
if (!defined('_NO_DATA_MESSAGE')) {
    define('_NO_DATA_MESSAGE', 'Onvoldoende gegevens');
}
if (!defined('_MAIL_VOL_BY_ENT_REPORT')) {
    define('_MAIL_VOL_BY_ENT_REPORT', 'Briefvolume per dienst');
}
if (!defined('_WRONG_DATE_FORMAT')) {
    define('_WRONG_DATE_FORMAT', 'Fout datumformaat');
}
if (!defined('_ENTITY_PROCESS_DELAY')) {
    define('_ENTITY_PROCESS_DELAY', 'Gemiddelde verwerkingstermijn per eenheid');
}
if (!defined('_ENTITY_PROCESS_DELAY_DESC')) {
    define('_ENTITY_PROCESS_DELAY_DESC', 'Om de termijn (in dagen) weer te geven van de aanmaak tot de afsluiting van een brief.');
}
if (!defined('_ENTITY_LATE_MAIL')) {
    define('_ENTITY_LATE_MAIL', 'Briefvolume te laat per eenheid');
}
if (!defined('_ENTITY_LATE_MAIL_DESC')) {
    define('_ENTITY_LATE_MAIL_DESC', 'Om het aantal brieven weer te geven dat niet afgesloten werd en waarvan de verwerkingsdeadline overschreden werd.');
}
if (!defined('_ALL_LIST')) {
    define('_ALL_LIST', 'De volledige lijst weergeven');
}
if (!defined('_DEST_OR_COPY')) {
    define('_DEST_OR_COPY', 'Ontvanger');
}
if (!defined('_SUBMIT')) {
    define('_SUBMIT', 'Valideren');
}
if (!defined('_CANCEL')) {
    define('_CANCEL', 'Annuleren');
}
if (!defined('_DIFFLIST_TYPE_ROLES')) {
    define('_DIFFLIST_TYPE_ROLES', 'Beschikbare rollen');
}
if (!defined('_NO_AVAILABLE_ROLE')) {
    define('_NO_AVAILABLE_ROLE', 'Geen rol beschikbaar');
}
if (!defined('_ALL_DIFFLIST_TYPES')) {
    define('_ALL_DIFFLIST_TYPES', 'Alle types');
}
if (!defined('_DIFFLIST_TYPES_DESC')) {
    define('_DIFFLIST_TYPES_DESC', 'Types verdelingslijsten');
}
if (!defined('_DIFFLIST_TYPES')) {
    define('_DIFFLIST_TYPES', 'Types verdelingslijsten');
}
if (!defined('_DIFFLIST_TYPE')) {
    define('_DIFFLIST_TYPE', 'Lijsttype(s)');
}
if (!defined('_ADD_DIFFLIST_TYPE')) {
    define('_ADD_DIFFLIST_TYPE', 'Een type toevoegen');
}
if (!defined('_DIFFLIST_TYPE_ID')) {
    define('_DIFFLIST_TYPE_ID', 'Gebruikersnaam');
}
if (!defined('_DIFFLIST_TYPE_LABEL')) {
    define('_DIFFLIST_TYPE_LABEL', 'Beschrijving');
}
if (!defined('_ALLOW_ENTITIES')) {
    define('_ALLOW_ENTITIES', 'Diensten toestaan');
}
if (!defined('_ALL_LISTMODELS')) {
    define('_ALL_LISTMODELS', 'Alle lijsten');
}
if (!defined('_LISTMODELS_DESC')) {
    define('_LISTMODELS_DESC', 'Modellen van verdelingslijsten van brieven en mappen');
}
if (!defined('_LISTMODELS')) {
    define('_LISTMODELS', 'Modellen verdelingslijsten');
}
if (!defined('_MANAGE_LISTMODELS_DESC')) {
    define('_MANAGE_LISTMODELS_DESC', 'De modellen van de berichten- en goedkeuringscircuits beheren die in een brief gebruikt kunnen worden.');
}
if (!defined('_LISTMODEL')) {
    define('_LISTMODEL', 'Lijstmodel(len)');
}
if (!defined('_ADD_LISTMODEL')) {
    define('_ADD_LISTMODEL', 'Nieuw model');
}
if (!defined('_ADMIN_LISTMODEL')) {
    define('_ADMIN_LISTMODEL', 'Model van verdelingslijst');
}
if (!defined('_ADMIN_LISTMODEL_TITLE')) {
    define('_ADMIN_LISTMODEL_TITLE', 'Identificatie van het lijstmodel:');
}
if (!defined('_OBJECT_TYPE')) {
    define('_OBJECT_TYPE', 'Type van het lijstmodel');
}
if (!defined('_SELECT_OBJECT_TYPE')) {
    define('_SELECT_OBJECT_TYPE', 'Selecteer een type...');
}
if (!defined('_SELECT_OBJECT_ID')) {
    define('_SELECT_OBJECT_ID', 'Selecteer een link...');
}
if (!defined('_USER_DEFINED_ID')) {
    define('_USER_DEFINED_ID', 'Vrij');
}
if (!defined('_ALL_OBJECTS_ARE_LINKED')) {
    define('_ALL_OBJECTS_ARE_LINKED', 'Alle lijsten zijn reeds bepaald');
}
if (!defined('_SELECT_OBJECT_TYPE_AND_ID')) {
    define('_SELECT_OBJECT_TYPE_AND_ID', 'U moet een lijsttype en een gebruikersnaam specificeren!');
}
if (!defined('_SAVE_LISTMODEL')) {
    define('_SAVE_LISTMODEL', 'Valideren');
}
if (!defined('_OBJECT_ID_IS_NOT_VALID_ID')) {
    define('_OBJECT_ID_IS_NOT_VALID_ID', 'Gebruikersnaam ongeldig: hij mag slechts alfabetische, numerieke karakters of underscores bevatten (A-Z a-z 0-9 _)');
}
if (!defined('_LISTMODEL_ID_ALREADY_USED')) {
    define('_LISTMODEL_ID_ALREADY_USED', 'Deze gebruikersnaam wordt reeds gebruikt!');
}
if (!defined('_CONFIRM_LISTMODEL_SAVE')) {
    define('_CONFIRM_LISTMODEL_SAVE', 'De lijst opslaan?');
}
if (!defined('_ENTER_DESCRIPTION')) {
    define('_ENTER_DESCRIPTION', 'Verplichte beschrijving');
}
if (!defined('_ENTER_TITLE')) {
    define('_ENTER_TITLE', 'Verplichte titel');
}
if (!defined('_PARAM_AVAILABLE_LISTMODELS_ON_GROUP_BASKETS')) {
    define('_PARAM_AVAILABLE_LISTMODELS_ON_GROUP_BASKETS', 'De modeltypes van de verdelingslijst voor de indexering instellen');
}
if (!defined('_INDEXING_DIFFLIST_TYPES')) {
    define('_INDEXING_DIFFLIST_TYPES', 'Types verdelingslijst');
}
if (!defined('_ADMIN_DIFFLIST_TYPES')) {
    define('_ADMIN_DIFFLIST_TYPES', 'Types verdelingslijst (Administratie)');
}
if (!defined('_ADMIN_DIFFLIST_TYPES_DESC')) {
    define('_ADMIN_DIFFLIST_TYPES_DESC', 'De verschillende types verdelingslijst beheren');
}
if (!defined('_ADMIN_LISTMODELS')) {
    define('_ADMIN_LISTMODELS', 'Verdelingsmodel (Administratie)');
}
if (!defined('_ADMIN_LISTMODELS_DESC')) {
    define('_ADMIN_LISTMODELS_DESC', 'De verschillende verdelingsmodellen beheren');
}
if (!defined('_STANDARD')) {
    define('_STANDARD', 'Standaard');
}
if (!defined('_5_ARCHIVAL')) {
    define('_5_ARCHIVAL', '5 Archiefonderzoek ');
}
if (!defined('_51_IDENTIFICATION')) {
    define('_51_IDENTIFICATION', '5.1 Identificatie');
}
if (!defined('_52_DESCRIPTION')) {
    define('_52_DESCRIPTION', '5.2 Beschrijving');
}
if (!defined('_53_RELATIONS')) {
    define('_53_RELATIONS', '5.3 Relaties');
}
if (!defined('_54_CONTROL')) {
    define('_54_CONTROL', '5.4 Controle');
}
if (!defined('_VISIBLE')) {
    define('_VISIBLE', 'Actief');
}
if (!defined('_NOT_VISIBLE')) {
    define('_NOT_VISIBLE', 'Niet actief');
}
if (!defined('_TARGET_STATUS')) {
    define('_TARGET_STATUS', 'Eindstatus bij de bevestiging van de fase');
}
if (!defined('_TARGET_ROLE')) {
    define('_TARGET_ROLE', 'Rol die vooruit moet gaan in de workflow');
}
if (!defined('_ITS_NOT_MY_TURN_IN_THE_WF')) {
    define('_ITS_NOT_MY_TURN_IN_THE_WF', 'Het is niet mijn beurt om te behandelen in de workflow');
}
if (!defined('_NO_AVAILABLE_ROLE_FOR_ME_IN_THE_WF')) {
    define('_NO_AVAILABLE_ROLE_FOR_ME_IN_THE_WF', 'Er is geen rol voor mij bepaald in de workflow');
}
if (!defined('_NO_FILTER')) {
    define('_NO_FILTER', 'De filters wissen');
}
if (!defined('_AUTO_FILTER')) {
    define('_AUTO_FILTER', 'Voorgestelde lijst');
}
if (!defined('_REDIRECT_NOTE')) {
    define('_REDIRECT_NOTE', 'Woord');
}
if (!defined('_STORE_DIFF_LIST')) {
    define('_STORE_DIFF_LIST', 'De verdelingslijst bewaren');
}
if (!defined('_DIFF_LIST_STORED')) {
    define('_DIFF_LIST_STORED', 'Bewaarde verdelingslijst');
}
if (!defined('_PRINT_SEPS')) {
    define('_PRINT_SEPS', 'Afdrukken van de separatoren');
}
if (!defined('_CHOOSE_ENTITIES')) {
    define('_CHOOSE_ENTITIES', 'Kies de diensten');
}
if (!defined('_DEL_USER_LISTDIFF')) {
    define('_DEL_USER_LISTDIFF', 'De gebruiker van de verdelingslijst halen');
}
if (!defined('_DEL_ENTITY_LISTDIFF')) {
    define('_DEL_ENTITY_LISTDIFF', 'De eenheid van de verdelingslijst halen');
}
if (!defined('_ADD_USER_LISTDIFF')) {
    define('_ADD_USER_LISTDIFF', 'De gebruiker aan de verdelingslijst toevoegen');
}
if (!defined('_ADD_ENTITY_LISTDIFF')) {
    define('_ADD_ENTITY_LISTDIFF', 'De eenheid aan de verdelingslijst toevoegen');
}
if (!defined('_RESPONSE_RATE')) {
    define('_RESPONSE_RATE', 'Antwoordpercentage');
}
if (!defined('_RESPONSE_RATE_BY_ENTITIES')) {
    define('_RESPONSE_RATE_BY_ENTITIES', 'Antwoordpercentage per eenheid');
}
if (!defined('_RESPONSE_RATE_BY_ENTITIES_DESC')) {
    define('_RESPONSE_RATE_BY_ENTITIES_DESC', 'Om het percentage brieven weer te geven waarop er antwoord werd gegeven. Deze statistiek baseert zich op het aantal brieven waarvan er een vertrekdatum ingeschreven werd (het veld custom_d1 moet gebruikt worden)');
}
if (!defined('_ARCHIVAL_AGREEMENT')) {
    define('_ARCHIVAL_AGREEMENT', 'Archiveringsconventie');
}
if (!defined('_ARCHIVAL_AGENCY')) {
    define('_ARCHIVAL_AGENCY', 'Archiveringsdienst');
}
if (!defined('_GO_TO_CC')) {
    define('_GO_TO_CC', 'De gebruiker in kopie zetten');
}
if (!defined('_GO_TO_DEST')) {
    define('_GO_TO_DEST', 'De gebruiker als ontvanger zetten');
}
if (!defined('_UP_USER_ONE_ROW')) {
    define('_UP_USER_ONE_ROW', 'De gebruiker een rang verhogen');
}
if (!defined('_DOWN_USER_ONE_ROW')) {
    define('_DOWN_USER_ONE_ROW', 'De gebruiker een rang verlagen');
}
if (!defined('_UP_ENTITY_ONE_ROW')) {
    define('_UP_ENTITY_ONE_ROW', 'De eenheid een rang verhogen');
}
if (!defined('_DOWN_ENTITY_ONE_ROW')) {
    define('_DOWN_ENTITY_ONE_ROW', 'De eenheid een rang verlagen');
}
if (!defined('_TO_USERS_OF_ENTITIES')) {
    define('_TO_USERS_OF_ENTITIES', 'Naar gebruikers van de eenheden');
}
if (!defined('_INDEXING_ENTITIES')) {
    define('_INDEXING_ENTITIES', 'Indexeren voor de eenheden');
}
if (!defined('_SEARCH_DIFF_LIST')) {
    define('_SEARCH_DIFF_LIST', 'Een eenheid of een gebruiker zoeken');
}
if (!defined('_HELP_KEYWORD_EXEMPLE_TITLE')) {
    define('_HELP_KEYWORD_EXEMPLE_TITLE', 'Voorbeeld in de bepaling van de veiligheid van een groep (where clause): toegangen op de assets met betrekking tot de belangrijkste dienst waartoe de verbonden gebruiker of de subeenheden van deze dienst behoren.');
}
if (!defined('_CHOOSE_REPLACEMENT_DEPARTMENT')) {
    define('_CHOOSE_REPLACEMENT_DEPARTMENT', 'Kies een vervangende eenheid');
}
if (!defined('_ENTITY_VOL_STAT_DESC')) {
    define('_ENTITY_VOL_STAT_DESC', 'Om het aantal brieven weer te geven dat per eenheid bewaard wordt.');
}
if (!defined('_MAIL_VOL_BY_ENT_REPORT')) {
    define('_MAIL_VOL_BY_ENT_REPORT', 'Briefvolume per eenheid');
}
if (!defined('_ALLOW_ENTITIES')) {
    define('_ALLOW_ENTITIES', 'De eenheden toelaten');
}
if (!defined('_CHOOSE_ENTITIES')) {
    define('_CHOOSE_ENTITIES', 'Kies de eenheden');
}
if (!defined('_ARCHIVAL_AGENCY')) {
    define('_ARCHIVAL_AGENCY', 'Archiefeenheid');
}
if (!defined('_TYPE_ID_HISTORY')) {
    define('_TYPE_ID_HISTORY', 'Gebruikersnaam van de geschiedenis');
}
if (!defined('_RES_ID')) {
    define('_RES_ID', 'RES ID');
}
if (!defined('_UPDATED_BY_USER')) {
    define('_UPDATED_BY_USER', 'Bijgewerkt door');
}
