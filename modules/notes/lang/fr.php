<?php
/*
 *
 *    Copyright 2008,2009 Maarch
 *
 *  This file is part of Maarch Framework.
 *
 *   Maarch Framework is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Maarch Framework is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *    along with Maarch Framework.  If not, see <http://www.gnu.org/licenses/>.
 */
if (!defined("_RESTRICTED_SERVICES")) {
    define("_RESTRICTED_SERVICES", "Restreint au(x) entité(s): ");
}
if (!defined("_ADD_NOTE")) {
    define("_ADD_NOTE", "Ajouter une annotation");
}
if (!defined("_READ")) {
    define("_READ", "Lire");
}
if (!defined("_NOTE_UPDATED")) {
    define("_NOTE_UPDATED", "Annotation modifiée");
}
if (!defined("_NOTES")) {
    define("_NOTES", "Annotations");
}
if (!defined("_NOTES_COMMENT")) {
    define("_NOTES_COMMENT", "Annotations");
}
if (!defined("_OF")) {
    define("_OF", "de");
}

//TEMPLATES FOR NOTES
if (!defined("_NOTE")) {
    define("_NOTE", "Annotation");
}
