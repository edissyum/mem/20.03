<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */
if (!defined('_RESTRICTED_SERVICES')) {
    define('_RESTRICTED_SERVICES', 'Beperkt tot de dienst(en):');
}
if (!defined('_ADD_NOTE')) {
    define('_ADD_NOTE', 'Aantekening toevoegen');
}
if (!defined('_READ')) {
    define('_READ', 'Lezen');
}
if (!defined('_NOTE_UPDATED')) {
    define('_NOTE_UPDATED', 'Gewijzigde aantekening');
}
if (!defined('_NOTES')) {
    define('_NOTES', 'Opmerkingen');
}
if (!defined('_NOTES_COMMENT')) {
    define('_NOTES_COMMENT', 'Opmerkingen');
}
if (!defined('_OF')) {
    define('_OF', 'van');
}
if (!defined('_ADD_NOTE')) {
    define('_ADD_NOTE', 'Toevoeging van een opmerking');
}
if (!defined('_NOTE')) {
    define('_NOTE', 'Opmerking');
}
