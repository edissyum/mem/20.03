<?php
/**
* Copyright Maarch since 2008 under licence GPLv3.
* See LICENCE.txt file at the root folder for more details.
* This file is part of Maarch software.

*
* @brief   nl
*
* @author  dev <dev@maarch.org>
* @ingroup reports
*/

/*************************** Report management *****************/
if (!defined('_ADMIN_REPORTS')) {
    define('_ADMIN_REPORTS', 'Statistieken');
}
if (!defined('_REPORTS')) {
    define('_REPORTS', 'Statistieken');
}
if (!defined('_REPORTS_COMMENT')) {
    define('_REPORTS_COMMENT', 'Statistieken');
}
if (!defined('_OTHER_REPORTS')) {
    define('_OTHER_REPORTS', 'Verschillende bewerkingen');
}
if (!defined('_HAVE_TO_SELECT_GROUP')) {
    define('_HAVE_TO_SELECT_GROUP', 'U moet een groep selecteren');
}
if (!defined('_AVAILABLE_REPORTS')) {
    define('_AVAILABLE_REPORTS', 'Statussen beschikbaar voor');
}
if (!defined('_GROUP_REPORTS_ADDED')) {
    define('_GROUP_REPORTS_ADDED', 'Statussen die toegewezen zijn aan de groep');
}
if (!defined('_CLICK_LINE_BELOW_TO_SEE_REPORT')) {
    define('_CLICK_LINE_BELOW_TO_SEE_REPORT', 'Klik op één van de onderstaande lijnen om de overeenkomstige bewerkingen te zien');
}
if (!defined('_CLICK_LINE_BELOW_TO_RETURN_TO_REPORTS')) {
    define('_CLICK_LINE_BELOW_TO_RETURN_TO_REPORTS', 'Klik hier om terug te keren naar de lijst van de beschikbare bewerkingen');
}
if (!defined('_NO_REPORTS_FOR_USER')) {
    define('_NO_REPORTS_FOR_USER', 'Geen enkele statistiek beschikbaar via het menu voor deze gebruiker');
}
if (!defined('_PRINT_NO_RESULT')) {
    define('_PRINT_NO_RESULT', 'Niets weer te geven');
}
if (!defined('_DOCUMENTS_LIST')) {
    define('_DOCUMENTS_LIST', 'Documentenlijst');
}
if (!defined('_DOCUMENTS_LIST_DESC')) {
    define('_DOCUMENTS_LIST_DESC', 'Documentenlijst');
}
if (!defined('_TITLE_STATS_DU')) {
    define('_TITLE_STATS_DU', 'van');
}
if (!defined('_TITLE_STATS_AU')) {
    define('_TITLE_STATS_AU', 'tot');
}
if (!defined('_NB_TOTAL_DOC')) {
    define('_NB_TOTAL_DOC', 'Totaal aantal voorgelegde stukken');
}
if (!defined('_NB_TOTAL_FOLDER')) {
    define('_NB_TOTAL_FOLDER', 'Totaal aantal voorgelegde mappen');
}
if (!defined('_NO_DATA_MESSAGE')) {
    define('_NO_DATA_MESSAGE', 'Onvoldoende gegevens om de grafiek of de tabel te maken');
}
if (!defined('_REPORT')) {
    define('_REPORT', 'Status');
}
if (!defined('_ERROR_REPORT_TYPE')) {
    define('_ERROR_REPORT_TYPE', 'Fout met type status');
}
if (!defined('_ERROR_PERIOD_TYPE')) {
    define('_ERROR_PERIOD_TYPE', 'Fout met type periode');
}
if (!defined('_REPORTS_EVO_PROCESS')) {
    define('_REPORTS_EVO_PROCESS', 'Evolutie van de gemiddelde verwerkingstermijn');
}
if (!defined('_MONTH')) {
    define('_MONTH', 'Maand');
}
if (!defined('_PROCESS_DELAI_AVG')) {
    define('_PROCESS_DELAI_AVG', 'Gemiddelde termijn (in dagen)');
}
if (!defined('_PROCESS_DELAY_GENERIC_EVALUATION_REPORT')) {
    define('_PROCESS_DELAY_GENERIC_EVALUATION_REPORT', 'Gemiddelde verwerkingstermijn per periode');
}
if (!defined('_PROCESS_DELAY_GENERIC_EVALUATION_REPORT_DESC')) {
    define('_PROCESS_DELAY_GENERIC_EVALUATION_REPORT_DESC', 'Om de termijn (in dagen) weer te geven van de aanmaak tot de afsluiting van een brief.');
}
if (!defined('_PROCESS_DELAY_GENERIC_EVALUATION_REPORT_BY_TYPE')) {
    define('_PROCESS_DELAY_GENERIC_EVALUATION_REPORT_BY_TYPE', 'Gemiddelde verwerkingstermijn per type brief');
}
if (!defined('_FILESTAT_LIST_DESC')) {
    define('_FILESTAT_LIST_DESC', 'De lijst van de 10 laatste beschikbare statistische bestanden weergeven.');
}
if (!defined('_FILESTAT_DESC')) {
    define('_FILESTAT_DESC', 'Deze bestanden worden aangemaakt via de <b>geplande taak</b> in de module <b>life_cycle</b>.<br/>Enkel de  <b>10 laatste bestanden</b> worden weergegeven.');
}
if (!defined('_NO_STAT_FILES_AVAILABLE')) {
    define('_NO_STAT_FILES_AVAILABLE', 'Geen enkel statistisch bestand beschikbaar in de map:');
}
if (!defined('_MAARCH_APPLICATION')) {
    define('_MAARCH_APPLICATION', 'Applicatie Maarch Courrier');
}
if (!defined('_GROUP')) {
    define('_GROUP', 'Groepen');
}
if (!defined('_SELECT_GROUP')) {
    define('_SELECT_GROUP', 'Een groep kiezen');
}
