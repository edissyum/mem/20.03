<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */

if (!defined('_SENDMAIL')) {
    define('_SENDMAIL', 'E-mails verzenden');
}
if (!defined('_SENDMAIL_COMMENT')) {
    define('_SENDMAIL_COMMENT', 'E-mails verzenden');
}
if (!defined('_EMAIL_LIST')) {
    define('_EMAIL_LIST', 'E-mails');
}
if (!defined('_EMAIL_LIST_DESC')) {
    define('_EMAIL_LIST_DESC', 'E-maillijst');
}
if (!defined('_SENDED_EMAILS')) {
    define('_SENDED_EMAILS', 'Verzonden elementen');
}
if (!defined('_EMAIL_DRAFT')) {
    define('_EMAIL_DRAFT', 'Klad');
}
if (!defined('_EMAIL_WAIT')) {
    define('_EMAIL_WAIT', 'In afwachting van verzending');
}
if (!defined('_EMAIL_IN_PROGRESS')) {
    define('_EMAIL_IN_PROGRESS', 'Wordt verzonden');
}
if (!defined('_EMAIL_SENT')) {
    define('_EMAIL_SENT', 'Verzonden');
}
if (!defined('_EMAIL_ERROR')) {
    define('_EMAIL_ERROR', 'Fout tijdens de verzending');
}
if (!defined('_FROM')) {
    define('_FROM', 'Afzender');
}
if (!defined('_FROM_SHORT')) {
    define('_FROM_SHORT', 'van');
}
if (!defined('_SEND_TO')) {
    define('_SEND_TO', 'Ontvanger');
}
if (!defined('_SEND_TO_SHORT')) {
    define('_SEND_TO_SHORT', 'naar');
}
if (!defined('_COPY_TO')) {
    define('_COPY_TO', 'In kopie');
}
if (!defined('_COPY_TO_SHORT')) {
    define('_COPY_TO_SHORT', 'Cc');
}
if (!defined('_COPY_TO_INVISIBLE')) {
    define('_COPY_TO_INVISIBLE', 'In onzichtbare kopie');
}
if (!defined('_COPY_TO_INVISIBLE_SHORT')) {
    define('_COPY_TO_INVISIBLE_SHORT', 'Cci');
}
if (!defined('_JOINED_FILES')) {
    define('_JOINED_FILES', 'Bijgevoegde bestanden');
}
if (!defined('_SHOW_OTHER_COPY_FIELDS')) {
    define('_SHOW_OTHER_COPY_FIELDS', 'De velden CC en Cci weergeven/verbergen');
}
if (!defined('_EMAIL_OBJECT')) {
    define('_EMAIL_OBJECT', 'Onderwerp');
}
if (!defined('_HTML_OR_RAW')) {
    define('_HTML_OR_RAW', 'Geavanceerde lay-out /Onbewerkte tekst');
}
if (!defined('_DEFAULT_BODY')) {
    define('_DEFAULT_BODY', 'Uw boodschap is klaar voor verzending met de volgende bijgevoegde bestanden:');
}
if (!defined('_NOTES_FILE')) {
    define('_NOTES_FILE', 'Aantekeningen van het brief');
}
if (!defined('_EMAIL_WRONG_FORMAT')) {
    define('_EMAIL_WRONG_FORMAT', 'Het e-mailadres heeft het verkeerde formaat');
}
if (!defined('_EMAIL_NOT_EXIST')) {
    define('_EMAIL_NOT_EXIST', 'Deze e-mail bestaat niet');
}
if (!defined('_NEW_EMAIL')) {
    define('_NEW_EMAIL', 'Nieuw bericht');
}
if (!defined('_NEW_NUMERIC_PACKAGE')) {
    define('_NEW_NUMERIC_PACKAGE', 'Nieuwe digitale brief');
}
if (!defined('_NUMERIC_PACKAGE_ADDED')) {
    define('_NUMERIC_PACKAGE_ADDED', 'Digitale brief toegevoegd');
}
if (!defined('_NUMERIC_PACKAGE_IMPORTED')) {
    define('_NUMERIC_PACKAGE_IMPORTED', 'Digitale brief geïmporteerd');
}
if (!defined('_NUMERIC_PACKAGE_SENT')) {
    define('_NUMERIC_PACKAGE_SENT', 'Digitale brieven verzonden');
}
if (!defined('_NUMERIC_PACKAGE')) {
    define('_NUMERIC_PACKAGE', 'Digitale brief');
}
if (!defined('_NO_COMMUNICATION_MODE')) {
    define('_NO_COMMUNICATION_MODE', 'Geen communicatiemiddel');
}
if (!defined('_NOTHING')) {
    define('_NOTHING', 'Geen');
}
if (!defined('_CREATE_EMAIL')) {
    define('_CREATE_EMAIL', 'Aanmaken');
}
if (!defined('_EMAIL_ADDED')) {
    define('_EMAIL_ADDED', 'E-mail toegevoegd');
}
if (!defined('_SEND_EMAIL')) {
    define('_SEND_EMAIL', 'Verzenden');
}
if (!defined('_RESEND_EMAIL')) {
    define('_RESEND_EMAIL', 'Terugsturen');
}
if (!defined('_SAVE_EMAIL')) {
    define('_SAVE_EMAIL', 'Bewaren');
}
if (!defined('_READ_EMAIL')) {
    define('_READ_EMAIL', 'E-mail weergeven');
}
if (!defined('_TRANSFER_EMAIL')) {
    define('_TRANSFER_EMAIL', 'E-mail doorzenden');
}
if (!defined('_EDIT_EMAIL')) {
    define('_EDIT_EMAIL', 'E-mail wijzigen');
}
if (!defined('_SAVE_EMAIL')) {
    define('_SAVE_EMAIL', 'Bewaren');
}
if (!defined('_SAVE_COPY_EMAIL')) {
    define('_SAVE_COPY_EMAIL', 'Een kopie bewaren');
}
if (!defined('_EMAIL_UPDATED')) {
    define('_EMAIL_UPDATED', 'E-mail gewijzigd');
}
if (!defined('_REMOVE_EMAIL')) {
    define('_REMOVE_EMAIL', 'Verwijderen');
}
if (!defined('_REMOVE_EMAILS')) {
    define('_REMOVE_EMAILS', 'De e-mails verwijderen');
}
if (!defined('_REALLY_REMOVE_EMAIL')) {
    define('_REALLY_REMOVE_EMAIL', 'Wilt u het bericht verwijderen');
}
if (!defined('_EMAIL_REMOVED')) {
    define('_EMAIL_REMOVED', 'E-mail verwijderd');
}
if (!defined('_Label_ADD_TEMPLATE_MAIL')) {
    define('_Label_ADD_TEMPLATE_MAIL', 'Begeleidingsmodel:');
}
if (!defined('_ADD_TEMPLATE_MAIL')) {
    define('_ADD_TEMPLATE_MAIL', 'Selecteer het gewenste model');
}
if (!defined('_EMAIL_OBJECT_ANSWER')) {
    define('_EMAIL_OBJECT_ANSWER', 'Antwoord op uw brief van');
}
if (!defined('_INCORRECT_SENDER')) {
    define('_INCORRECT_SENDER', 'Foute verzender');
}
if (!defined('_OPERATION_DATE')) {
    define('_OPERATION_DATE', 'Datum van behandeling');
}
if (!defined('_RECEPTION_DATE')) {
    define('_RECEPTION_DATE', 'Ontvangstdatum');
}
if (!defined('_SENDS_FAIL')) {
    define('_SENDS_FAIL', 'De verzending is mislukt');
}
if (!defined('_WRONG_FILE_TYPE_M2M')) {
    define('_WRONG_FILE_TYPE_M2M', 'Enkel ZIP-bestanden worden aanvaard');
}
if (!defined('_ERROR_RECEIVE_FAIL')) {
    define('_ERROR_RECEIVE_FAIL', 'Fout tijdens de verzending van de zip.');
}
if (!defined('_ERROR_CONTACT_UNKNOW')) {
    define('_ERROR_CONTACT_UNKNOW', 'Contact onbekend.');
}
if (!defined('_NO_RECIPIENT')) {
    define('_NO_RECIPIENT', 'Geen ontvanger');
}
if (!defined('_NO_SENDER')) {
    define('_NO_SENDER', 'Geen verzender');
}
if (!defined('_SIMPLE_DOWNLOAD')) {
    define('_SIMPLE_DOWNLOAD', 'Downloaden');
}
if (!defined('_MORE_INFORMATIONS')) {
    define('_MORE_INFORMATIONS', 'Informatie over de transfer');
}
if (!defined('_REPLY_RESPONSE_SENT')) {
    define('_REPLY_RESPONSE_SENT', 'Behandelingsbewijs verzonden op');
}
if (!defined('_M2M_ARCHIVETRANSFER')) {
    define('_M2M_ARCHIVETRANSFER', 'Digitale brief');
}
if (!defined('_M2M_ARCHIVETRANSFERREPLY')) {
    define('_M2M_ARCHIVETRANSFERREPLY', 'Antwoord verzonden');
}
if (!defined('_M2M_ACTION_DONE')) {
    define('_M2M_ACTION_DONE', 'in werking gesteld door');
}
if (!defined('_M2M_ENTITY_DESTINATION')) {
    define('_M2M_ENTITY_DESTINATION', 'De brief is in de dienst');
}
if (!defined('_M2M_FOLLOWUP_REQUEST')) {
    define('_M2M_FOLLOWUP_REQUEST', 'Opvolging van de vraag');
}
if (!defined('_M2M_ENTITY_DESTINATION')) {
    define('_M2M_ENTITY_DESTINATION', 'De brief is in de eenheid');
}
