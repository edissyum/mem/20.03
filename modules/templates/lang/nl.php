<?php
/**
* Copyright Maarch since 2008 under licence GPLv3.
* See LICENCE.txt file at the root folder for more details.
* This file is part of Maarch software.

*
* @brief   fr
*
* @author  dev <dev@maarch.org>
* @ingroup templates
*/

/*********************** Module Board ***********************************/
if (!defined('_ADMIN_TEMPLATE_ANSWER')) {
    define('_ADMIN_TEMPLATE_ANSWER', 'De modellen beheren');
}
if (!defined('_EXPLOIT_TEMPLATE_ANSWER')) {
    define('_EXPLOIT_TEMPLATE_ANSWER', 'De modellen gebruiken');
}
if (!defined('_TEMPLATE_NAME')) {
    define('_TEMPLATE_NAME', 'Naam van het model');
}
if (!defined('_TEMPLATES_COMMENT')) {
    define('_TEMPLATES_COMMENT', 'Documentmodellen');
}
if (!defined('_TEMPLATE_EMPTY')) {
    define('_TEMPLATE_EMPTY', 'Het model is leeg');
}
if (!defined('_TEMPLATE_LABEL')) {
    define('_TEMPLATE_LABEL', 'Naam van het model');
}
if (!defined('_TEMPLATE_COMMENT')) {
    define('_TEMPLATE_COMMENT', 'Beschrijving');
}
if (!defined('_TEMPLATE_TYPE')) {
    define('_TEMPLATE_TYPE', 'Modeltype');
}
if (!defined('_TEMPLATE_STYLE')) {
    define('_TEMPLATE_STYLE', 'Aard van het model');
}
if (!defined('_EDIT_TEMPLATE')) {
    define('_EDIT_TEMPLATE', 'Bewerking van het model');
}
if (!defined('_TEMPLATE_ID')) {
    define('_TEMPLATE_ID', 'ID van het model');
}
if (!defined('_ATTACH_TEMPLATE_TO_ENTITY')) {
    define('_ATTACH_TEMPLATE_TO_ENTITY', 'Het model moet aan minstens één dienst gekoppeld zijn');
}
if (!defined('_TEMPLATE_DATASOURCE')) {
    define('_TEMPLATE_DATASOURCE', 'Gegevensbron');
}
if (!defined('_OFFICE')) {
    define('_OFFICE', 'KANTOOR');
}
if (!defined('_HTML')) {
    define('_HTML', 'HTML');
}
if (!defined('_MANAGE_TEMPLATES')) {
    define('_MANAGE_TEMPLATES', 'De briefmodellen beheren');
}
if (!defined('_MANAGE_TEMPLATES_APP')) {
    define('_MANAGE_TEMPLATES_APP', 'De briefmodellen van de applicatie beheren');
}
if (!defined('_TEMPLATES_LIST')) {
    define('_TEMPLATES_LIST', 'Lijst van de modellen');
}
if (!defined('_ALL_TEMPLATES')) {
    define('_ALL_TEMPLATES', 'Alle modellen');
}
if (!defined('_TEMPLATE')) {
    define('_TEMPLATE', 'Model');
}
if (!defined('_ADD_TEMPLATE')) {
    define('_ADD_TEMPLATE', 'Een model toevoegen');
}
if (!defined('_THE_TEMPLATE')) {
    define('_THE_TEMPLATE', 'Het model');
}
if (!defined('_TEMPLATE_ADDITION')) {
    define('_TEMPLATE_ADDITION', 'Toevoeging van een model');
}
if (!defined('_TEMPLATE_MODIFICATION')) {
    define('_TEMPLATE_MODIFICATION', 'Wijziging van een model');
}
if (!defined('_TEMPLATE_DELETION')) {
    define('_TEMPLATE_DELETION', 'Verwijdering van een model');
}
if (!defined('_MODIFY_TEMPLATE')) {
    define('_MODIFY_TEMPLATE', 'De wijzigingen bevestigen');
}
if (!defined('_TEMPLATE_ADDED')) {
    define('_TEMPLATE_ADDED', 'Nieuw model aangemaakt');
}
if (!defined('_TEMPLATE_UPDATED')) {
    define('_TEMPLATE_UPDATED', 'Model gewijzigd');
}
if (!defined('_CHOOSE_ENTITY_TEMPLATE')) {
    define('_CHOOSE_ENTITY_TEMPLATE', 'Kies de dienst(en) waarmee u dit model wil koppelen');
}
if (!defined('_REALLY_DEL_TEMPLATE')) {
    define('_REALLY_DEL_TEMPLATE', 'Wilt u dit model echt verwijderen?');
}
if (!defined('_NEW_TEMPLATE')) {
    define('_NEW_TEMPLATE', 'Nieuw model');
}
if (!defined('_CHOOSE_TEMPLATE')) {
    define('_CHOOSE_TEMPLATE', 'Kies een model');
}
if (!defined('_TEMPLATE_DELETED')) {
    define('_TEMPLATE_DELETED', 'Model verwijderd');
}
if (!defined('_DELETED_TEMPLATE')) {
    define('_DELETED_TEMPLATE', 'Model verwijderd');
}
if (!defined('_ASSOCIATED_TEMPLATES')) {
    define('_ASSOCIATED_TEMPLATES', 'Gekoppelde modellen');
}
if (!defined('_NO_DEFINED_TEMPLATE')) {
    define('_NO_DEFINED_TEMPLATE', 'Geen model bepaald');
}
if (!defined('_EDIT_YOUR_TEMPLATE')) {
    define('_EDIT_YOUR_TEMPLATE', 'Gelieve het model minstens eenmaal te bewerken');
}
if (!defined('_TEMPLATE_NAME2')) {
    define('_TEMPLATE_NAME2', 'Naam van het model');
}
if (!defined('_TEMPLATE_CONTENT')) {
    define('_TEMPLATE_CONTENT', 'Inhoud van het model');
}
if (!defined('_TEMPLATES')) {
    define('_TEMPLATES', 'model(len)');
}
if (!defined('_ADMIN_TEMPLATES')) {
    define('_ADMIN_TEMPLATES', 'Documentmodellen');
}
if (!defined('_LOADED_FILE')) {
    define('_LOADED_FILE', 'Geïmporteerd bestand');
}
if (!defined('_GENERATED_FILE')) {
    define('_GENERATED_FILE', 'Aangemaakt bestand');
}
if (!defined('_MUST_CHOOSE_TEMPLATE')) {
    define('_MUST_CHOOSE_TEMPLATE', 'U moet een model kiezen');
}
if (!defined('_GENERATE')) {
    define('_GENERATE', 'Bijlage genereren');
}
if (!defined('_PLEASE_SELECT_TEMPLATE')) {
    define('_PLEASE_SELECT_TEMPLATE', 'Gelieve een bijlagemodel te selecteren');
}
if (!defined('_NO_MODE_DEFINED')) {
    define('_NO_MODE_DEFINED', 'Fout: geen modus');
}
if (!defined('_TEMPLATE_OR_ANSWER_ERROR')) {
    define('_TEMPLATE_OR_ANSWER_ERROR', 'Fout: probleem bij het laden van het model of van het antwoord');
}
if (!defined('_NO_CONTENT')) {
    define('_NO_CONTENT', 'Fout: Inhoud van het antwoord leeg');
}
if (!defined('_FILE_OPEN_ERROR')) {
    define('_FILE_OPEN_ERROR', 'Openen van het bestand onmogelijk');
}
if (!defined('_ANSWER_OPEN_ERROR')) {
    define('_ANSWER_OPEN_ERROR', 'Fout: probleem bij het openen van het antwoord');
}
if (!defined('_TEMPLATE_UPDATE')) {
    define('_TEMPLATE_UPDATE', 'Model bijgewerkt');
}
if (!defined('_ANSWER_UPDATED')) {
    define('_ANSWER_UPDATED', 'Bijlage bijgewerkt');
}
if (!defined('_ANSWER_TITLE')) {
    define('_ANSWER_TITLE', 'Titel van de bijlage');
}
if (!defined('_VALID_TEXT')) {
    define('_VALID_TEXT', 'Tekst valideren');
}
if (!defined('_NO_DATASOURCE')) {
    define('_NO_DATASOURCE', 'Geen gegevensbron');
}
if (!defined('_ALREADY_RESERVED')) {
    define('_ALREADY_RESERVED', 'Wijziging document aan de gang');
}
if (!defined('_TXT')) {
    define('_TXT', 'TXT');
}
if (!defined('_TEMPLATE_TARGET')) {
    define('_TEMPLATE_TARGET', 'Doel van het model');
}
if (!defined('_NO_TARGET')) {
    define('_NO_TARGET', 'Geen doel');
}
if (!defined('_EXTENSION_NOT_ALLOWED')) {
    define('_EXTENSION_NOT_ALLOWED', 'Niet toegestane extensie');
}
if (!defined('_ATTACH_TEMPLATE_TO_ENTITY')) {
    define('_ATTACH_TEMPLATE_TO_ENTITY', 'Het model moet aan minstens een eenheid gekoppeld zijn');
}
if (!defined('_CHOOSE_ENTITY_TEMPLATE')) {
    define('_CHOOSE_ENTITY_TEMPLATE', 'Kies de Eenheid (eenheden) waaraan u dit model wil koppelen');
}
