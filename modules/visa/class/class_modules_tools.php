<?php
/*
*   Copyright 2008-2015 Maarch and Document Image Solutions
*
*   This file is part of Maarch Framework.
*
*   Maarch Framework is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   Maarch Framework is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Maarch Framework.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* @brief Contains the functions to manage visa and notice workflow.
*
* @file
* @author Nicolas Couture <couture@docimsol.com>
* @date $date$
* @version $Revision$
* @ingroup visa
*/

require_once 'modules/visa/class/class_modules_tools_Abstract.php';

class visa extends visa_Abstract
{
	// custom
}


class PdfNotes extends PdfNotes_Abstract
{
	// custom
}

class ConcatPdf extends ConcatPdf_Abstract
{
    // custom
}

?>
