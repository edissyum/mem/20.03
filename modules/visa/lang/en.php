<?php

// CIRCUIT DE VISA
if (!defined('_VISA_WORKFLOW')) {
    define('_VISA_WORKFLOW', 'Visa workflow');
}
if (!defined('_INTERRUPT_WORKFLOW')) {
    define('_INTERRUPT_WORKFLOW', 'Stop the visa workflow');
}
if (!defined('_VISA_WORKFLOW_COMMENT')) {
    define('_VISA_WORKFLOW_COMMENT', 'Visa workflow management');
}

if (!defined('_NO_VISA')) {
    define('_NO_VISA', 'No designed person on visa');
}
if (!defined('_NO_RESPONSE_PROJECT_VISA')) {
    define('_NO_RESPONSE_PROJECT_VISA', 'Please, integrate at least one attachment to signature book');
}

// CIRCUIT D"AVIS
if (!defined('_AVIS_WORKFLOW')) {
    define('_AVIS_WORKFLOW', 'Recommendation flow');
}

if (!defined('_DISSMARTCARD_SIGNER_APPLET')) {
    define('_DISSMARTCARD_SIGNER_APPLET', 'Electronic signature in progress...');
}

if (!defined('_IMG_SIGN_MISSING')) {
    define('_IMG_SIGN_MISSING', 'Missing signature picture');
}
if (!defined('_SEND_TO_SIGNATURE')) {
    define('_SEND_TO_SIGNATURE', 'Submit');
}

if (!defined('_SUBMIT_COMMENT')) {
    define('_SUBMIT_COMMENT', 'Visa comments (optional) ');
}

if (!defined('_NO_FILE_PRINT')) {
    define('_NO_FILE_PRINT', 'None of the file to print');
}

if (!defined('_BAD_PIN')) {
    define('_BAD_PIN', 'Wrong PIN code. Warning : Attention, 3 essais maximum !');
}

if (!defined('_PRINT_DOCUMENT')) {
    define('_PRINT_DOCUMENT', ' Show and print the document');
}

if (!defined('_VISA_BY')) {
    define('_VISA_BY', 'Visa by');
}

if (!defined('_INSTEAD_OF')) {
    define('_INSTEAD_OF', 'Instead of');
}

if (!defined('_WAITING_FOR_SIGN')) {
    define('_WAITING_FOR_SIGN', 'Signature on hold');
}

if (!defined('_SIGNED')) {
    define('_SIGNED', 'Signed');
}

if (!defined('_WAITING_FOR_VISA')) {
    define('_WAITING_FOR_VISA', 'Visa on hold');
}

if (!defined('_VISED')) {
    define('_VISED', 'Aimed ?');
}

if (!defined('DOWN_USER_WORKFLOW')) {
    define('DOWN_USER_WORKFLOW', 'move down the user');
}

if (!defined('UP_USER_WORKFLOW')) {
    define('UP_USER_WORKFLOW', 'Move up the user');
}

if (!defined('ADD_USER_WORKFLOW')) {
    define('ADD_USER_WORKFLOW', 'Add an user in the workflow');
}

if (!defined('DEL_USER_WORKFLOW')) {
    define('DEL_USER_WORKFLOW', 'Remove the user from the flow');
}

if (!defined('_NO_NEXT_STEP_VISA')) {
    define('_NO_NEXT_STEP_VISA', "Impossible to do this action. The flow doesn't contain more steps.");
}

if (!defined('_VISA_USERS')) {
    define('_VISA_USERS', 'Person(s) for visa / signature');
}

if (!defined('_TMP_SIGNED_FILE_FAILED')) {
    define('_TMP_SIGNED_FILE_FAILED', 'Failure on document generation with signature');
}

if (!defined('NO_PLACE_SIGNATURE')) {
    define('NO_PLACE_SIGNATURE', 'No place for signature');
}

if (!defined('_ENCRYPTED')) {
    define('_ENCRYPTED', 'Encoded');
}

if (!defined('_ADD_VISA_ROLE')) {
    define('_ADD_VISA_ROLE', 'Add visa user');
}

if (!defined('_ADD_VISA_MODEL')) {
    define('_ADD_VISA_MODEL', 'Use visa circuit model');
}

if (!defined('_NO_SIGNATORY')) {
    define('_NO_SIGNATORY', 'No signatory');
}

if (!defined('_SIGNATORY')) {
    define('_SIGNATORY', 'Signatory');
}

if (!defined('_SIGNED_TO')) {
    define('_SIGNED_TO', 'Signed to');
}

if (!defined('_SIGN_IN_PROGRESS')) {
    define('_SIGN_IN_PROGRESS', 'Signature in progress');
}

if (!defined('_DOCUMENTS_LIST_WITH_SIGNATORY')) {
    define('_DOCUMENTS_LIST_WITH_SIGNATORY', 'Documents list with signatory');
}

/***** Signature Book *****/
if (!defined('_DEFINE_MAIL')) {
    define('_DEFINE_MAIL', 'Mail');
}
if (!defined('_PROGRESSION')) {
    define('_PROGRESSION', 'Progression');
}
if (!defined('_ACCESS_TO_DETAILS')) {
    define('_ACCESS_TO_DETAILS', 'Access to details');
}
if (!defined('_SB_INCOMING_MAIL_ATTACHMENTS')) {
    define('_SB_INCOMING_MAIL_ATTACHMENTS', 'incoming mail attachment(s)');
}
if (!defined('_DOWNLOAD_ATTACHMENT')) {
    define('_DOWNLOAD_ATTACHMENT', 'Download the attachment');
}
if (!defined('_DEFINE_FOR')) {
    define('_DEFINE_FOR', 'For');
}
if (!defined('_CHRONO')) {
    define('_CHRONO', 'Chrono');
}
if (!defined('_DRAFT')) {
    define('_DRAFT', 'Draft');
}
if (!defined('_UPDATE_ATTACHMENT')) {
    define('_UPDATE_ATTACHMENT', 'Update the attachment');
}
if (!defined('_DELETE_ATTACHMENT')) {
    define('_DELETE_ATTACHMENT', 'Delete the attachment');
}
if (!defined('_DISPLAY_ATTACHMENTS')) {
    define('_DISPLAY_ATTACHMENTS', 'Display attachments list');
}
/***** Signature Book *****/

if (!defined('_NO_USER_SIGNED_DOC')) {
    define('_NO_USER_SIGNED_DOC', 'you have NOT signed any attachment!');
}

if (!defined('_IS_ALL_ATTACHMENT_SIGNED_INFO')) {
    define('_IS_ALL_ATTACHMENT_SIGNED_INFO', "You can't request a signed document to users, no attachment in signature book");
}

if (!defined('_IS_ALL_ATTACHMENT_SIGNED_INFO2')) {
    define('_IS_ALL_ATTACHMENT_SIGNED_INFO2', "You can't request a signed document to users, all attachments in signature book are signed.");
}

if (!defined('_REQUESTED_SIGNATURE')) {
    define('_REQUESTED_SIGNATURE', 'Requested signature');
}

if (!defined('_SELECT_MESSAGE_MODEL_IXBUS')) {
    define('_SELECT_MESSAGE_MODEL_IXBUS', "Choose a circuit");
}

if (!defined('_RESPONSES_WILL_BE_GENERATED')) {
    define('_RESPONSES_WILL_BE_GENERATED', "answer(s) will be generated.");
}

if (!defined('_GENERATE_PDF')) {
    define('_GENERATE_PDF', "Generate PDF version");
}

if (!defined('_CONVERSION_FAILED')) {
    define('_CONVERSION_FAILED', "Conversion failed !");
}

if (!defined('_NATURE_IXBUS')) {
    define('_NATURE_IXBUS', "Document nature");
}

if (!defined('_WORKFLOW_MODEL_IXBUS')) {
    define('_WORKFLOW_MODEL_IXBUS', "Workflow model");
}

if (!defined('_ID_IXBUS')) {
    define('_ID_IXBUS', "Identifiant IxBus");
}

if (!defined('_PASSWORD_IXBUS')) {
    define('_PASSWORD_IXBUS', "Password IxBus");
}

if (!defined('_WRONG_ID_PASSWORD_IXBUS')) {
    define('_WRONG_ID_PASSWORD_IXBUS', "Invalid IxBus ID or password");
}

if (!defined('_HANDWRITTEN_SIGN')) {
    define('_HANDWRITTEN_SIGN', "Handwritten signature");
}

if (!defined('_ESIGN')) {
    define('_ESIGN', "Electronique signature");
}

if (!defined('_MAIL_NOTE')) {
    define('_MAIL_NOTE', "Annotation of the main document");
}
if (!defined('_ATTACHMENT_SIGNATURE')) {
    define('_ATTACHMENT_SIGNATURE', "Signature of attachment in signature book");
}
if (!defined('_NOTE_USER')) {
    define('_NOTE_USER', "Note user");
}
if (!defined('_WF_SEND_TO')) {
    define('_WF_SEND_TO', "Send to :");
}
if (!defined('_VISA_USER_MIN')) {
    define('_VISA_USER_MIN', "Visa user");
}
