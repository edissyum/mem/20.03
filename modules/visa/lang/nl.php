<?php
/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 */

if (!defined('_VISA_WORKFLOW')) {
    define('_VISA_WORKFLOW', 'Goedkeuringscircuit');
}
if (!defined('_INTERRUPT_WORKFLOW')) {
    define('_INTERRUPT_WORKFLOW', 'Het goedkeuringscircuit onderbreken');
}
if (!defined('_VISA_WORKFLOW_COMMENT')) {
    define('_VISA_WORKFLOW_COMMENT', 'Beheer van het goedkeuringscircuit');
}
if (!defined('_NO_VISA')) {
    define('_NO_VISA', 'Geen persoon aangeduid in de goedkeuring');
}
if (!defined('_NO_RESPONSE_PROJECT_VISA')) {
    define('_NO_RESPONSE_PROJECT_VISA', 'Gelieve minstens één bijlage in de parafeerder te integreren.');
}
if (!defined('_AVIS_WORKFLOW')) {
    define('_AVIS_WORKFLOW', 'Berichtencircuit');
}
if (!defined('_DISSMARTCARD_SIGNER_APPLET')) {
    define('_DISSMARTCARD_SIGNER_APPLET', 'Elektronische handtekening bezig...');
}
if (!defined('_IMG_SIGN_MISSING')) {
    define('_IMG_SIGN_MISSING', 'Beeld van handtekening ontbreekt');
}
if (!defined('_SEND_TO_SIGNATURE')) {
    define('_SEND_TO_SIGNATURE', 'Voorleggen');
}
if (!defined('_SUBMIT_COMMENT')) {
    define('_SUBMIT_COMMENT', 'Commentaar van goedkeuring (optioneel)');
}
if (!defined('_NO_FILE_PRINT')) {
    define('_NO_FILE_PRINT', 'Geen bestand om af te drukken');
}
if (!defined('_BAD_PIN')) {
    define('_BAD_PIN', 'Pincode fout Opgelet maximum 3 pogingen!');
}
if (!defined('_PRINT_DOCUMENT')) {
    define('_PRINT_DOCUMENT', 'Het document weergeven en afdrukken');
}
if (!defined('_VISA_BY')) {
    define('_VISA_BY', 'Goedkeuring door');
}
if (!defined('_INSTEAD_OF')) {
    define('_INSTEAD_OF', 'in plaats van');
}
if (!defined('_WAITING_FOR_SIGN')) {
    define('_WAITING_FOR_SIGN', 'In afwachting van ondertekening');
}
if (!defined('_SIGNED')) {
    define('_SIGNED', 'Ondertekend');
}
if (!defined('_WAITING_FOR_VISA')) {
    define('_WAITING_FOR_VISA', 'In afwachting van goedkeuring');
}
if (!defined('_VISED')) {
    define('_VISED', 'Goedgekeurd');
}
if (!defined('DOWN_USER_WORKFLOW')) {
    define('DOWN_USER_WORKFLOW', 'De gebruiker naar beneden verplaatsen');
}
if (!defined('UP_USER_WORKFLOW')) {
    define('UP_USER_WORKFLOW', 'De gebruiker naar boven verplaatsen');
}
if (!defined('ADD_USER_WORKFLOW')) {
    define('ADD_USER_WORKFLOW', 'Een gebruiker in het circuit toevoegen');
}
if (!defined('DEL_USER_WORKFLOW')) {
    define('DEL_USER_WORKFLOW', 'De gebruiker uit het circuit halen');
}
if (!defined('_NO_NEXT_STEP_VISA')) {
    define('_NO_NEXT_STEP_VISA', 'Onmogelijk om deze actie uit te voeren. Het circuit bevat geen bijkomende stap.');
}
if (!defined('_VISA_USERS')) {
    define('_VISA_USERS', 'Personen voor goedkeuring(en) / ondertekening');
}
if (!defined('_TMP_SIGNED_FILE_FAILED')) {
    define('_TMP_SIGNED_FILE_FAILED', 'Aanmaken van document met handtekening mislukt');
}
if (!defined('NO_PLACE_SIGNATURE')) {
    define('NO_PLACE_SIGNATURE', 'Geen plaats voor handtekening');
}
if (!defined('_ENCRYPTED')) {
    define('_ENCRYPTED', 'versleuteld');
}
if (!defined('_VISA_USER_COU')) {
    define('_VISA_USER_COU', 'U bent de huidige goedkeurder');
}
if (!defined('_VISA_USER_COU_DESC')) {
    define('_VISA_USER_COU_DESC', 'U keurt goed in plaats van');
}
if (!defined('_SIGN_USER_COU')) {
    define('_SIGN_USER_COU', 'U bent de huidige ondertekenaar');
}
if (!defined('_SIGN_USER_COU_DESC')) {
    define('_SIGN_USER_COU_DESC', 'U ondertekent in plaats van');
}
if (!defined('_SIGN_USER')) {
    define('_SIGN_USER', 'Ondertekenende persoon');
}
if (!defined('_ADD_VISA_ROLE')) {
    define('_ADD_VISA_ROLE', 'Een goedkeurder toevoegen');
}
if (!defined('_ADD_VISA_MODEL')) {
    define('_ADD_VISA_MODEL', 'Een goedkeuringscircuitmodel gebruiken');
}
if (!defined('_NO_SIGNATORY')) {
    define('_NO_SIGNATORY', 'Geen ondertekenaar');
}
if (!defined('_SIGNATORY')) {
    define('_SIGNATORY', 'ONDERTEKENAAR');
}
if (!defined('_SIGNED_TO')) {
    define('_SIGNED_TO', 'Getekend op');
}
if (!defined('_SIGN_IN_PROGRESS')) {
    define('_SIGN_IN_PROGRESS', 'Ondertekening bezig');
}
if (!defined('_DOCUMENTS_LIST_WITH_SIGNATORY')) {
    define('_DOCUMENTS_LIST_WITH_SIGNATORY', 'Lijst van de documenten met ondertekenaar');
}
if (!defined('_DEFINE_MAIL')) {
    define('_DEFINE_MAIL', 'Brief');
}
if (!defined('_PROGRESSION')) {
    define('_PROGRESSION', 'Vooruitgang');
}
if (!defined('_ACCESS_TO_DETAILS')) {
    define('_ACCESS_TO_DETAILS', 'Krijg toegang tot de gedetailleerde fiche');
}
if (!defined('_SB_INCOMING_MAIL_ATTACHMENTS')) {
    define('_SB_INCOMING_MAIL_ATTACHMENTS', 'bijkomend(e) stuk(ken)');
}
if (!defined('_DOWNLOAD_ATTACHMENT')) {
    define('_DOWNLOAD_ATTACHMENT', 'De bijlage downloaden');
}
if (!defined('_DEFINE_FOR')) {
    define('_DEFINE_FOR', 'Voor');
}
if (!defined('_CHRONO')) {
    define('_CHRONO', 'Chrono');
}
if (!defined('_DRAFT')) {
    define('_DRAFT', 'Klad');
}
if (!defined('_UPDATE_ATTACHMENT')) {
    define('_UPDATE_ATTACHMENT', 'De bijlage wijzigen');
}
if (!defined('_DELETE_ATTACHMENT')) {
    define('_DELETE_ATTACHMENT', 'De bijlage verwijderen');
}
if (!defined('_DISPLAY_ATTACHMENTS')) {
    define('_DISPLAY_ATTACHMENTS', 'De bijlagenlijst weergeven');
}
if (!defined('_PARAMETER_IDENTIFIER')) {
    define('_PARAMETER_IDENTIFIER', 'Gebruikersnaam');
}
if (!defined('_DESCRIPTION')) {
    define('_DESCRIPTION', 'Beschrijving');
}
if (!defined('_VALUE')) {
    define('_VALUE', 'Waarde');
}
if (!defined('_TYPE')) {
    define('_TYPE', 'Type');
}
if (!defined('_STRING')) {
    define('_STRING', 'Karakterreeks');
}
if (!defined('_INTEGER')) {
    define('_INTEGER', 'Geheel getal');
}
if (!defined('_VALIDATE')) {
    define('_VALIDATE', 'Valideren');
}
if (!defined('_CANCEL')) {
    define('_CANCEL', 'Annuleren');
}
if (!defined('_MODIFY_PARAMETER')) {
    define('_MODIFY_PARAMETER', 'Instelling wijzigen');
}
if (!defined('_DELETE_PARAMETER')) {
    define('_DELETE_PARAMETER', 'Instelling verwijderen');
}
if (!defined('_PAGE')) {
    define('_PAGE', 'Pagina');
}
if (!defined('_OUT_OF')) {
    define('_OUT_OF', 'van');
}
if (!defined('_SEARCH')) {
    define('_SEARCH', 'Zoeken');
}
if (!defined('_RECORDS_PER_PAGE')) {
    define('_RECORDS_PER_PAGE', 'resultaten per pagina');
}
if (!defined('_DISPLAY')) {
    define('_DISPLAY', 'Weergeven');
}
if (!defined('_NO_RECORDS')) {
    define('_NO_RECORDS', 'Geen resultaat');
}
if (!defined('_AVAILABLE')) {
    define('_AVAILABLE', 'beschikbaar');
}
if (!defined('_FILTERED_FROM')) {
    define('_FILTERED_FROM', 'gefilterd op een geheel van');
}
if (!defined('_RECORDS')) {
    define('_RECORDS', 'resultaten');
}
if (!defined('_FIRST')) {
    define('_FIRST', 'eerste');
}
if (!defined('_LAST')) {
    define('_LAST', 'laatste');
}
if (!defined('_NEXT')) {
    define('_NEXT', 'volgende');
}
if (!defined('_PREVIOUS')) {
    define('_PREVIOUS', 'vorige');
}
if (!defined('_PARAMETER')) {
    define('_PARAMETER', 'instelling');
}
if (!defined('_DELETE_CONFIRM')) {
    define('_DELETE_CONFIRM', 'Wilt u de instelling echt verwijderen');
}
if (!defined('_NO_USER_SIGNED_DOC')) {
    define('_NO_USER_SIGNED_DOC', 'U heeft de bijlage NIET ondertekend!');
}
if (!defined('_IS_ALL_ATTACHMENT_SIGNED_INFO')) {
    define('_IS_ALL_ATTACHMENT_SIGNED_INFO', 'U kan geen handtekening aan de gebruikers vragen. Geen bijlage aanwezig in de parafeerder');
}
if (!defined('_IS_ALL_ATTACHMENT_SIGNED_INFO2')) {
    define('_IS_ALL_ATTACHMENT_SIGNED_INFO2', 'Alle bijlagen die in de parafeerder aanwezig zijn, werden ondertekend.');
}
if (!defined('_REQUESTED_SIGNATURE')) {
    define('_REQUESTED_SIGNATURE', 'Handtekening gevraagd');
}
if (!defined('_SELECT_MESSAGE_MODEL_IXBUS')) {
    define('_SELECT_MESSAGE_MODEL_IXBUS', 'Kies een circuit');
}
if (!defined('_RESPONSES_WILL_BE_GENERATED')) {
    define('_RESPONSES_WILL_BE_GENERATED', 'antwoord(en) aangemaakt');
}
if (!defined('_GENERATE_PDF')) {
    define('_GENERATE_PDF', 'De PDF-versie aanmaken');
}
if (!defined('_CONVERSION_FAILED')) {
    define('_CONVERSION_FAILED', 'Conversie mislukt!');
}
if (!defined('_NATURE_IXBUS')) {
    define('_NATURE_IXBUS', 'Aard van het document');
}
if (!defined('_WORKFLOW_MODEL_IXBUS')) {
    define('_WORKFLOW_MODEL_IXBUS', 'Model van het goedkeuringscircuit');
}
if (!defined('_ID_IXBUS')) {
    define('_ID_IXBUS', 'Gebruikersnaam IxBus');
}
if (!defined('_PASSWORD_IXBUS')) {
    define('_PASSWORD_IXBUS', 'Wachtwoord IxBus');
}
if (!defined('_WRONG_ID_PASSWORD_IXBUS')) {
    define('_WRONG_ID_PASSWORD_IXBUS', 'Gebruikersnaam of wachtwoord IxBus onjuist');
}
if (!defined('_HANDWRITTEN_SIGN')) {
    define('_HANDWRITTEN_SIGN', 'Handgeschreven handtekening');
}
if (!defined('_ESIGN')) {
    define('_ESIGN', 'Elektronische handtekening');
}
if (!defined('_PJ_NUMBER')) {
    define('_PJ_NUMBER', 'Bijlage nr.');
}
if (!defined('_AND_DOC_ORIG')) {
    define('_AND_DOC_ORIG', 'en origineel document nr.');
}
if (!defined('_EXTERNAL_ID_EMPTY')) {
    define('_EXTERNAL_ID_EMPTY', 'De externe referentie is leeg.');
}
if (!defined('_SEND_TO_IPARAPHEUR')) {
    define('_SEND_TO_IPARAPHEUR', 'Verzenden naar het vloeiboek?');
}
if (!defined('_SEND_TO_FAST')) {
    define('_SEND_TO_FAST', 'Verzenden naar het FAST vloeiboek?');
}
if (!defined('_PROJECT_NUMBER')) {
    define('_PROJECT_NUMBER', 'Project brief nummer ');
}
if (!defined('_MAIL_NOTE')) {
    define('_MAIL_NOTE', 'Opmerking van het aangekomen document');
}
if (!defined('_ATTACHMENT_SIGNATURE')) {
    define('_ATTACHMENT_SIGNATURE', 'Handtekening van de documenten die in het vloeiboek geïntegreerd zijn');
}
if (!defined('_NOTE_USER')) {
    define('_NOTE_USER', "Note user _TO_TRANSLATE");
}
if (!defined('_WF_SEND_TO')) {
    define('_WF_SEND_TO', "Send to : _TO_TRANSLATE");
}
if (!defined('_VISA_USER_MIN')) {
    define('_VISA_USER_MIN', "Visa user _TO_TRANSLATE");
}
