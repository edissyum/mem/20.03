<?php

/**
 * Copyright Maarch since 2008 under licence GPLv3.
 * See LICENCE.txt file at the root folder for more details.
 * This file is part of Maarch software.
 *
 */

/**
 * @brief Export Seda Model
 * @author dev@maarch.org
 */

namespace ExportSeda\models;

abstract class ExportSedaModelAbstract
{
}
