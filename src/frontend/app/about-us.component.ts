import { Component, OnInit } from '@angular/core';
import { LANG } from './translate.component';
import { HeaderService } from '../service/header.service';
import { AppService } from '../service/app.service';
import {HttpClient} from "@angular/common/http"; // EDISSYUM - NCH01
import {NotificationService} from "./notification.service"; // EDISSYUM - NCH01
import {catchError, tap} from "rxjs/operators"; // EDISSYUM - NCH01
import {of} from "rxjs/internal/observable/of"; // EDISSYUM - NCH01


declare function $j(selector: any): any;

declare var angularGlobals: any;

@Component({
    templateUrl: "about-us.component.html",
    styleUrls: ['about-us.component.css'],
    providers: [AppService]
})
export class AboutUsComponent implements OnInit {

    applicationVersion: string;
    lang: any = LANG;

    loading: boolean = false;
    version : any; // EDISSYUM - NCH01

    constructor(
        public http: HttpClient, // EDISSYUM - NCH01
        private notify: NotificationService, // EDISSYUM - NCH01
        private headerService: HeaderService,
        public appService: AppService) {
        $j("link[href='merged_css.php']").remove();
    }

    ngOnInit(): void {
        // EDISSYUM - NCH01 - Récupération du tag
        this.http.get('../../rest/versionsUpdate').pipe(
            tap((data: any) => {
                this.version = data.outputEdissyum;
                this.loading = false;
            }),
            catchError(err => {
                this.notify.handleErrors(err);
                return of(false);
            })
        ).subscribe();
        // END EDISSYUM - NCH01
        this.headerService.setHeader(this.lang.aboutUs);

        this.applicationVersion = angularGlobals.applicationVersion;
        this.loading = false;
    }
}
