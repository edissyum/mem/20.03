import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { LANG } from '../../translate.component';
import { NotificationService } from '../../notification.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { NoteEditorComponent } from '../../notes/note-editor.component';
import { tap, exhaustMap, catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
    templateUrl: 'export-pesv2-action.component.html',
    styleUrls: ['export-pesv2-action.component.scss'],
})
export class ExportPesv2ActionComponent implements OnInit {

    lang: any = LANG;
    loading: boolean = false;

    @ViewChild('noteEditor', { static: true }) noteEditor: NoteEditorComponent;

    constructor(
        public http: HttpClient,
        private notify: NotificationService,
        public dialogRef: MatDialogRef<ExportPesv2ActionComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    ngOnInit(): void {}

    onSubmit() {
        this.loading = true;
        this.http.post('../../rest/exportPESv2', this.data.resIds).subscribe((data: any) => {
            if (data.errors){
                this.notify.handleSoftErrors(data.errors);
                return of(false);
            }else{
                this.dialogRef.close(this.data.resIds);
            }
        });
    }
}
