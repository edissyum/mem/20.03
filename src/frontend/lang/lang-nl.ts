export const LANG_NL = {
    "ARsimple"                              : "AR simple", //_TO_TRANSLATE
    "ARsva"                                 : "AR SVA", //_TO_TRANSLATE
    "ARsvr"                                 : "AR SVR", //_TO_TRANSLATE
    "aboutUs"                               : "Over Maarch Courrier",
    "abs"                                   : "Afwezig",
    "absOff"                                : "Afwezigheid uitgeschakeld",
    "absOn"                                 : "Afwezigheid ingeschakeld",
    "makeUpdate"                            : "Update",
    "accordingTemplateParameters"           : "According to the parameters defined in the model", //_TO_TRANSLATE
    "accountAdded"                          : "Account added", //_TO_TRANSLATE
    "accountDeleted"                        : "Account deleted", //_TO_TRANSLATE
    "acknowledgementReceipt"                : "Acknowledgement of receipt", //_TO_TRANSLATE
    "acknowledgementReceipts"                : "Acknowledgement of receipts", //_TO_TRANSLATE
    "acknowledgementReceiptType"            : "Type of acknowledgement of receipt", //_TO_TRANSLATE
    "acknowledgementSendDate"               : "Acknowledgement send date", //_TO_TRANSLATE
    "action"                                : "Actie",
    "actionAdded"                           : "Actie toegevoegd",
    "actionAvailable"                       : "Beschikbare acties",
    "actionCarriedOut"                      : "System action", //_TO_TRANSLATE
    "actionChosen"                          : "Geselecteerde acties",
    "actionCreation"                        : "Aanmaak van een actie",
    "actionDeleted"                         : "Actie verwijderd",
    "actionDone"                            : "Action effectuée", //_TO_TRANSLATE
    "actionHistory"                         : "De actie traceren",
    "actionHistoryDesc"                     : "Staat traceren toe in de geschiedenis van het brief. Het wordt sterk aangeraden deze optie aan te vinken.",
    "actionMassForbidden"                   : "This action is forbidden in mass", //_TO_TRANSLATE
    "actionModification"                    : "Wijziging van de actie",
    "actionName"                            : "Actienaam",
    "actionNotExist"                        : "This action does not exist", //_TO_TRANSLATE
    "actionPage"                            : "Resultaatpagina van de actie",
    "actionParameters"                      : "Instelling van de actie",
    "actions"                               : "Actie(s)",
    "actionsGroupBasketUpdated"             : "Instelling van de acties gewijzigd",
    "actionUpdated"                         : "Actie gewijzigd",
    "activateAbs"                           : "De afwezigheid inschakelen",
    "activateAbsence"                       : "De afwezigheid inschakelen",
    "activateMyAbs"                         : "Mijn afwezigheid inschakelen",
    "activateNotification"                  : "De melding activeren",
    "active"                                : "Actief",
    "activeCron"                            : "Actieve ta(a)k(en)",
    "actives"                               : "Actief",
    "add"                                   : "Toevoegen",
    "addAllData"                           : "_TO_TRANSLATE",
    "addContacts"                           : "Contacten toevoegen",
    "addCustomUnit"                         : "_TO_TRANSLATE",
    "addDoctype"                            : "Een documenttype toevoegen",
    "addFirstLevel"                         : "Een map toevoegen",
    "additionalRoles"                       : "Bijkomende rol(len)",
    "addNewAccount"                         : "Add new account", //_TO_TRANSLATE
    "addNote"                               : "Add note", //_TO_TRANSLATE
    "addPerson"                             : "Add a person", //_TO_TRANSLATE
    "address"                               : "Adres",
    "addSecondLevel"                        : "Een submap toevoegen",
    "addStatus"                             : "Een status toevoegen",
    "addUser"                               : "Een gebruiker toevoegen",
    "addUserOrEntity"                       : "Een gebruiker/ een eenheid toevoegen",
    "addVisaSignUser"                       : "Een goedkeurder / ondertekenaar toevoegen",
    "administration"                        : "Beheer",
    "administrationServices"                : "Beheerrechten",
    "admissionDate"                         : "_TO_TRANSLATE",
    "adrPriority"                           : "Sequentieprioriteit",
    "alignCenter"                           : "Center", //_TO_TRANSLATE
    "alignLeft"                             : "Left", //_TO_TRANSLATE
    "alignRight"                            : "Right", //_TO_TRANSLATE
    "all"                                   : "Alle",
    "allActions"                            : "Alle acties",
    "allAttachments"                        : "Alle bijlagen",
    "allEntities"                           : "All entities", //_TO_TRANSLATE
    "alreadyExist"                          : "De gebruikersnaam bestaat reeds",
    "alt_identifier"                        : "_TO_TRANSLATE",
    "application"                           : "App",
    "appName"                               : "_TO_TRANSLATE",
    "appUpToDate"                           : "Uw applicatie is bijgewerkt",
    "april"                                 : "April",
    "aprilShort"                            : "Apr.",
    "arAlreadyGenerated"                    : "Acknowledgement receipt generated but not sent", //_TO_TRANSLATE
    "arAlreadySend"                         : "Acknowledgement receipt already send", //_TO_TRANSLATE
    "archival"                              : "Archivering",
    "archivalAgency"                        : "Archiveringsconventie",
    "archivalAgreement"                     : "Archiveringsdienst",
    "arrivalDate"                           : "Aankomstdatum",
    "ascending"                             : "Oplopend",
    "ascOrder"                              : "_TO_TRANSLATE",
    "askRedirectBasketBeforeAbsence"        : "Wilt u de bakjes doorsturen vooraleer u afwezig te zetten?",
    "assignBy"                              : "Toegewezen door",
    "associatedStatus"                      : "Gekoppelde status",
    "at"                                    : "om",
    "atRange"                               : "om", //_TO_TRANSLATE
    "attachment"                            : "Bijlage",
    "attachments"                           : "Bijlagen",
    "attachmentSignature"                   : "Signature of attachment in signature book", //_TO_TRANSLATE
    "attachmentType"                        : "Type bijlage",
    "august"                                : "Augustus",
    "augustShort"                           : "Augustus",
    "authorize"                             : "Verlenen",
    "autoLogoutAbsence"                     : "Nadat u uw bakjesomleidingen heeft bepaald, zal uw verbinding automatisch verbroken worden.",
    "available"                             : "beschikbaar",
    "availableDatas"                        : "_TO_TRANSLATE",
    "availableGroups"                       : "Beschikbare groepen",
    "availableMinorVersions"                : "Beschikbare versies",
    "availableStatuses"                     : "Beschikbare statussen",
    "avis"                                  : "Berichtencircuit",
    "back"                                  : "Terug",
    "barcode"                               : "_TO_TRANSLATE",
    "basket"                                : "Bakje",
    "basketAdded"                           : "Bakje toegevoegd",
    "basketCreation"                        : "Aanmaak van een bakje",
    "basketDeleted"                         : "Bakje verwijderd",
    "basketHelpDesc"                        : "De bakjes slepen-neerzetten om de volgorde te wijzigen",
    "basketModification"                    : "Wijziging van een bakje",
    "basketNotFound"                        : "Bakjes niet gevonden",
    "basketNotification"                    : "De melding van dit bakje inschakelen / uitschakelen",
    "baskets"                               : "Bakjes",
    "adminBasketsDesc"                      : "De inhoud van de bakjes bepalen en deze aan gebruikersgroepen toewijzen. De mogelijke omleidingen opnoemen bij het gebruik van het bakje door een gegeven groep. Een weergaveformaat van het bakje toekennen door deze groep.",
    "basketsAssigned"                       : "Bakjes door een andere gebruiker toegewezen",
    "basketsAssignedShort"                  : "_TO_TRANSLATE",
    "basketsColor"                          : "Kleur van de bakjes",
    "basketsColorAdmin"                     : "Uw kleuren van de bakjes beheren",
    "basketsOrder"                          : "De volgorde van de bakjes beheren",
    "basketsRedirected"                     : "Bakjes doorgestuurd naar een andere gebruiker",
    "basketsUpdated"                        : "Gewijzigde(e) bakje(s)",
    "basketUpdated"                         : "Bakje gewijzigd",
    "beginSendTest"                         : "_TO_TRANSLATE",
    "belowAllMyEntities"                    : "Below all my entities", //_TO_TRANSLATE
    "belowMyPrimaryEntity"                  : "Below my primary entity", //_TO_TRANSLATE
    "browsing"                              : "Surfen",
    "by"                                    : "By", //_TO_TRANSLATE
    "calDays"                               : "kalenderdag(en)",
    "canBeModified"                         : "Wijziging van de indexen",
    "canBeSearched"                         : "Zoeken",
    "cancel"                                : "Annuleren",
    "canNotMakeAction"                      : "Can make action on this mail", //_TO_TRANSLATE
    "canNotUpdateApplication"               : "Some files are modified. Can not update application", // TO TRANSLATE
    "cantMoveDoctype"                       : "Het documenttype kan enkel in een submap verplaatst worden",
    "cantMoveFirstLevel"                    : "De submap kan enkel in een map verplaatst worden",
    "categories"                            : "_TO_TRANSLATE",
    "category_id"                           : "_TO_TRANSLATE",
    "changeMyPassword"                      : "Mijn wachtwoord wijzigen",
    "changePassword"                        : "Het wachtwoord wijzigen",
    "changePasswordInfo"                    : "u wordt uitgenodigd om uw wachtwoord te wijzigen",
    "chars"                                 : "teken(s),",
    "checkSendmail"                         : "_TO_TRANSLATE",
    "chooseAnotherEntityUser"               : "Choose another person from the entity", //_TO_TRANSLATE
    "chooseBasket"                          : "Kies een bakje",
    "chooseCategoryAssociation"             : "Kies één of meerdere gekoppelde categorieën",
    "chooseCategoryAssociationHelp"         : "Indien er geen enkele categorie geselecteerd wordt, dan is de actie geldig voor alle categorieën",
    "chooseColor"                           : "Kies een kleur",
    "chooseContactType"                     : "Kies een contacttype",
    "chooseDefaultAction"                   : "Kies een hoofdactie",
    "chooseEntity"                          : "Kies een eenheid",
    "chooseEntityAssociationModel"          : "Het model aan de dienst(en) toewijzen",
    "chooseGroup"                           : "Kies een groep",
    "chooseIndexationProfil"                : "Kies een indexeringsprofiel",
    "chooseModel"                           : "Kies een model",
    "chooseNewDest"                         : "is een <b>bestemmeling</b> van de volgende modellen van de verdelingslijst, kies een <b>vervangings</b>gebruiker",
    "chooseNewDestUser"                     : "is the <b>recipient</b> of mail being processed, please select a <b>replacement user</b>", //_TO_TRANSLATE_
    "chooseRedirectGroup"                   : "Kies een vervangingsgroep",
    "chosenDatas"                           : "_TO_TRANSLATE",
    "chosenModel"                           : "Gebruikt model",
    "loadedFile"                            : "Loaded file", //_TO_TRANSLATE
    "chrono"                                : "Chrono",
    "chronoNumber"                          : "Chrononummer",
    "chronoNumberShort"                     : "Chrono Num", //_TO_TRANSLATE
    "city"                                  : "Gemeente",
    "classifying"                           : "STANDEN",
    "clause"                                : "Clausule",
    "clauseGroup"                           : "Documentaire perimeter",
    "clickOn"                               : "Klik op",
    "close"                                 : "Afsluiten",
    "closingDate"                           : "Afsluitingsdatum",
    "collection"                            : "Verzameling",
    "color"                                 : "Kleur",
    "comment"                               : "_TO_TRANSLATE",
    "comments"                              : "_TO_TRANSLATE",
    "community"                             : "Gemeenschap",
    "complementaryFields"                   : "Bijkomende velden",
    "confidentiality"                       : "_TO_TRANSLATE",
    "configurationType"                     : "Configuration type", //_TO_TRANSLATE
    "configurationUpdated"                  : "_TO_TRANSLATE",
    "confirm"                               : "Bevestigen", //_TO_TRANSLATE
    "confirmAction"                         : "Wilt u deze actie echt uitvoeren?",
    "confirmDeleteAccount"                  : "Do you want to delete this account ?", //_TO_TRANSLATE
    "confirmDeleteMailSignature"            : "Wilt u de mailhandtekening echt verwijderen?",
    "confirmDeleteSignature"                : "Wilt u de handtekening echt verwijderen?",
    "confirmDuplicate"                      : "Wilt u dit sjabloon echt dupliceren?",
    "confirmToBeAbsent"                     : "Wilt u de afwezigheid echt inschakelen? Uw verbinding zal automatisch verbroken worden",
    "connectionFailed"                      : "_TO_TRANSLATE",
    "conservation"                          : "Bewaring",
    "contact"                               : "Contact",
    "contactAdded"                          : "Toegevoegd(e) contact(en)",
    "contactDeletedFromGroup"               : "Het contact werd uit de groep verwijderd",
    "contactGroupCreate"                    : "Een contactgroepering aanmaken",
    "contactGroupCreation"                  : "Aanmaak van een contactgroepering",
    "contactGroupList"                      : "Lijst met contactgroepen",
    "contactInfo"                           : "Contactfiche",
    "contacts"                              : "Contacts", //_TO_TRANSLATE
    "contactsAlt"                              : "Contact(s)",//_TO_TRANSLATE
    "contactsParameters_addressAdditional1"    : "Toren, gebouw, pand, residentie",
    "contactsParameters_addressCountry"       : "Land",
    "contactsParameters_addressNumber"           : "Straatnummer",
    "contactsParameters_addressPostcode"   : "Postcode",
    "contactsParameters_addressStreet"        : "Spoor",
    "contactsParameters_addressTown"          : "Gemeente",
    "contactsParameters_department"           : "Dienst",
    "contactsParameters_email"                 : "E-mail",
    "contactsParameters_firstname"             : "Voornaam",
    "contactsParameters_function"              : "Functie",
    "contactsParameters_lastname"              : "Naam",
    "contactsParameters_addressAdditional2"   : "Additional Line",//_TO_TRANSLATE
    "contactsParameters_phone"                 : "Telefoon",
    "contactsParameters_notes":                    "Notes", //_TO_TRANSLATE
    "contactsParameters_company"               : "Structuur",
    "contactsParameters_civility"              : "Aanspreking",
    "contactsFillingAdministration"         : "Volledigheid van de contactinformatie",
    "contactsFillingCriteria"               : "Volledigheidscriteria",
    "contactsFillingStep1"                  : "Eerste stap",
    "contactsFillingStep2"                  : "Tweede stap",
    "contactsFillingStep3"                  : "Laatste stap",
    "contactsFillingStr"                    : "voor volledigheid.",
    "contactsGroupAdded"                    : "Aangemaakte contactgroepering",
    "contactsGroupDeleted"                  : "Verwijderde contactgroepering",
    "contactsGroupDesc"                     : "Indien de groepering openbaar is, kunnen alle gebruikers deze gebruiken. Indien deze privé is, kan enkel de maker deze gebruiken",
    "contactsGroupModification"             : "Wijziging van de contactgroepering",
    "contactsGroups"                        : "Contactgroepering(en)",
    "contactsGroupsAlt"                     : "Contact(s) group(s)",//_TO_TRANSLATE
    "contactsGroupUpdated"                  : "Gewijzigde contactgroepering",
    "content_management"                    : "Versiebeheer van het document",
    "content"                               : "_TO_TRANSLATE",
    "contentHtmlTemplate"                   : "Inhoud van het html-model",
    "contentTxtTemplate"                    : "Inhoud van het tekstmodel",
    "contextParaph"                         : "Context", //_TO_TRANSLATE
    "copyUsersEntities"                     : "_TO_TRANSLATE",
    "country"                               : "Land",
    "createAtt"                             : "Een bijlage aanmaken",
    "createdBy"                             : "Aangemaakt door",
    "createdOn"                             : "Aangemaakt op",
    "createNewEntity"                       : "Maak een nieuwe eenheid",
    "createScriptNotification"              : "Het script aanmaken",
    "creation_date"                         : "_TO_TRANSLATE",
    "creationDate"                          : "Aanmaakdatum",
    "criteria"                              : "Criterium",
    "currentApplicationVersion"             : "Huidige versie van de toepassing",
    "currentFilters"                        : "_TO_TRANSLATE",
    "currentPsw"                            : "Huidig wachtwoord",
    "currentVersion"                        : "Huidige versie",
    "databaseColumn"                        : "Kolom Database",
    "dataOfMonth"                           : "Maandgegevens",
    "datasToExport"                         : "_TO_TRANSLATE",
    "date"                                  : "Datum",
    "day"                                   : "Dag",
    "dayOfMonth"                            : "Dag van de maand",
    "days"                                  : "dag(en)",
    "dayS"                                  : "Dag(en)",
    "december"                              : "December",
    "decemberShort"                         : "Dec.",
    "default"                               : "standaard",
    "defaultAction"                         : "Standaardactie",
    "defaultOrder"                          : "_TO_TRANSLATE",
    "defaultSort"                           : "Standaard sortering",
    "defaultTemplate"                       : "Aard van het model",
    "delay1"                                : "Termijn herinnering 1 voor einde (in dagen)",
    "delay2"                                : "Termijn herinnering 2 na einde (in dagen)",
    "delete"                                : "Verwijderen",
    "deleteAssignation"                     : "De toekenning verwijderen",
    "deleteAtt"                             : "De bijlage verwijderen",
    "deleteMsg"                             : "Wilt u dit element echt verwijderen?",
    "deleteRedirection"                     : "Delete redirection", //_TO_TRANSLATE
    "deleteUnit"                            : "_TO_TRANSLATE",
    "delimiter"                             : "_TO_TRANSLATE",
    "department"                            : "_TO_TRANSLATE",
    "departureDate"                         : "_TO_TRANSLATE",
    "desactivateAbsence"                    : "De afwezigheid uitschakelen",
    "descending"                            : "Aflopend",
    "descOrder"                             : "_TO_TRANSLATE",
    "description"                           : "Beschrijving",
    "dest_user"                             : "_TO_TRANSLATE",
    "destination"                           : "_TO_TRANSLATE",
    "destinationChangingInfo"               : "This mail will be reassigned to this entity", //_TO_TRANSLATE
    "destinationChangingInfoMass"           : "These mails following external entity of", //_TO_TRANSLATE
    "destinationChangingInfoMass2"          : "will be redirected to entity", //_TO_TRANSLATE
    "destinationEntity"                     : "_TO_TRANSLATE",
    "destinationEntityType"                 : "_TO_TRANSLATE",
    "destruction"                           : "Vernietiging",
    "detailLink"                            : "_TO_TRANSLATE",
    "details"                               : "Details",
    "diffusionList"                         : "Verdelingslijst",
    "diffusionModelCreation"                : "Aanmaak van een verdelingslijst",
    "diffusionModelDeleted"                 : "Verdelingsmodel verwijderd",
    "diffusionModelModification"            : "Wijziging van een verdelingsmodel",
    "diffusionModels"                       : "Verdelingsmodellen",
    "diffusionModelUpdated"                 : "Verdelingsmodel bijgewerkt",
    "diffusionModelAdded"                   : "Diffusion model added", //_TO_TRANSLATE
    "diffusionType"                         : "Modeltype",
    "disable"                               : "Uitschakelen",
    "disableBasket"                         : "Het bakje uitschakelen",
    "disabledContactsFilling"               : "Volledigheid ingeschakeld",
    "display"                               : "weergave",
    "displayAtt"                            : "Het bakje uitschakelen",
    "displayProcessLimitRes"                : "_TO_TRANSLATE",
    "dlAttachment"                          : "De bijlage downloaden",
    "docDate"                               : "_TO_TRANSLATE",
    "docserverAdded"                        : "Opslagruimte toegevoegd",
    "docserverCreation"                     : "Een opslagruimte aanmaken",
    "docserverDeleted"                      : "Opslagruimte verwijderd",
    "docserverdeleteWarning"                : "De opslagruimte lijkt opgeslagen bestanden te bevatten, ze zullen niet meer beschikbaar zijn in de toepassing.\n\nWilt u verdergaan?",
    "docserverDoesNotExists"                : "Docserver does not exists", //_TO_TRANSLATE
    "docservers"                            : "Opslagruimtes",
    "docserverType"                         : "Opslagruimtetype",
    "docserverUpdated"                      : "Opslagruimte gewijzigd",
    "doctype"                               : "Documenttype",
    "doctypes"                              : "Documenttypes",
    "documentTypes"                         : "Documenttypes",
    "documentTypesAlt"                      : "Documenttypologie(ën)",
    "adminDocumentTypesDesc"                : "De interne structuur van een map bepalen (map / submap / documenttype).",
    "documentation"                         : "Documentatie",
    "documentOutOfPerimeter"                : "Dit document ligt buiten uw perimeter",
    "documents"                             : "document(en)",
    "documentTypeAdded"                     : "Documenttype toegevoegd ",
    "documentTypeDeleted"                   : "Documenttype verwijderd",
    "documentTypeReplacement"               : "Vervanging van het documenttype",
    "documentTypeUpdated"                   : "Documenttype bijgewerkt",
    "done"                                  : "done", //_TO_TRANSLATE
    "doNothingRedirect"                     : "De entiteit loskoppelen van het profiel van de gebruiker", //_TO_TRANSLATE
    "doNothingRedirectInformations"         : "De mailinglijsten van de brieven en sjablonen van de entiteit worden niet gewijzigd", //_TO_TRANSLATE
    "doNotModifyUnlessExpert"               : "Wijzig dit onderdeel niet tenzij u weet wat u doet. Een verkeerde instelling kan leiden tot een slechte werking van de toepassing!",
    "doNotReply"                            : "_TO_TRANSLATE",
    "doShippingParameter"                   : "Please set up a template in <b>Administration</b> > <b>Shippings Maileva templates</b>", //_TO_TRANSLATE
    "downloadOriginalFile"                  : "Download the original file", //_TO_TRANSLATE
    "draft"                                 : "Klad",
    "duplicate"                             : "Dupliceren",
    "durationCurrentUse"                    : "Gewoonlijke nutsduur (in dagen)",
    "eachDay"                               : "Dagelijks",
    "eachHour"                              : "Elk uur",
    "eachMinute"                            : "Elke minuut",
    "eachMonth"                             : "Maandelijks",
    "edirectBaskets"                        : "De geselecteerde bakjes doorsturen",
    "editAttachment"                        : "Edit the attachment", //_TO_TRANSLATE
    "editingAttachmentInterrupted"          : "Editing of the attachment has been interrupted. <br/><sub>Please re-edit it. </sub>", //_TO_TRANSLATE
    "editModelFirst"                        : "Gelieve het model te bewerken vooraleer te bevestigen",
    "edtion"                                : "Edition", //_TO_TRANSLATE
    "electronicTemplate"                    : "Electronic template", //_TO_TRANSLATE
    "elements"                              : "element(s)", //_TO_TRANSLATE
    "email"                                 : "E-mail",
    "emails"                                : "E-mails",
    "emailSendFailed"                       : "_TO_TRANSLATE",
    "emailSendInProgress"                   : "_TO_TRANSLATE",
    "emailSendSuccess"                      : "_TO_TRANSLATE",
    "emailSendTest"                         : "_TO_TRANSLATE",
    "emailSignatureAdded"                   : "Mailhandtekening toegevoegd",
    "emailSignatureDeleted"                 : "Mailhandtekening verwijderd",
    "emailSignatures"                       : "Handtekeningen van e-mail",
    "emailSignatureUpdated"                 : "Mailhandtekening gewijzigd",
    "emptyBasket"                           : "Empty basket", //_TO_TRANSLATE
    "enable"                                : "Inschakelen",
    "enableAuth"                            : "_TO_TRANSLATE",
    "enableBasket"                          : "Het bakje activeren",
    "enabledContactsFilling"                : "Volledigheid uitgeschakeld",
    "enterValue"                            : "U moet een waarde invoeren",
    "entities"                              : "Eenheden",
    "adminEntitiesDesc"                     : "De structuur van uw organisatie beheren (polen, diensten en kantoren). Evenals de verdelingsmodellen en de goedkeuringscircuits die aan de eenheden gekoppeld zijn.",
    "workflowModels"                        : "Circuitmodellen",
    "adminWorkflowModelsDesc"               : "De modellen van de berichten- en goedkeuringscircuits beheren die in een brief gebruikt kunnen worden.",
    "entity_label"                          : "_TO_TRANSLATE",
    "entityAdded"                           : "Toegevoegde eenheid",
    "entityDeleted"                         : "Verwijderde eenheid",
    "entityFullName"                        : "Blok boomstructuur",
    "entityReplacement"                     : "Vervanging van de eenheid",
    "entityTooglePrimary"                   : "Overgang naar primaire eenheid",
    "entityTreeInfo"                        : "Klik op een eenheid van de boomstructuur om tot zijn informatie toegang te hebben",
    "entityTreeInfoCreation"                : "Klik op een eenheid van de boomstructuur om de nieuwe eenheid eraan te koppelen",
    "entityType"                            : "Type eenheid",
    "entityUpdated"                         : "Gewijzigde eenheid",
    "entityWithoutParentMessage"            : "Indien u niet onmiddellijk lid bent van deze eenheid, zal u het automatisch worden.",
    "entries"                               : "ingang(en)",
    "eraseAll"                              : "Erase all", //_TO_TRANSLATE
    "eraseAllFilters"                       : "_TO_TRANSLATE",
    "errorOccured"                          : "Er heeft zich een fout voorgedaan",
    "event"                                 : "Evenement",
    "expectedReturnDate"                    : "Expected return date", //_TO_TRANSLATE
    "export_seda"                           : "Export seda",
    "exportDatas"                           : "_TO_TRANSLATE",
    "externalComponents"                    : "Externe onderdelen",
    "february"                              : "Februari",
    "februaryShort"                         : "Feb.",
    "fewSeconds"                            : "enkele seconden",
    "field"                                 : "Veld",
    "fieldNature"                           : "Aard veld",
    "fieldType"                             : "Type veld",
    "fileDoesNotExists"                     : "File does not exists", //_TO_TRANSLATE
    "filterBy"                              : "Filteren",
    "filteredFrom"                          : "gefilterd op een geheel van",
    "filters"                               : "_TO_TRANSLATE",
    "fingerprint"                           : "Digitale afdruk",
    "firstLevelAdded"                       : "Map aangemaakt",
    "firstLevelAttached"                    : "Aan de map koppelen",
    "firstLevelDeleted"                     : "Map verwijderd",
    "firstLevelDoctype"                     : "Map",
    "firstLevelUpdated"                     : "Map bijgewerkt",
    "firstname"                             : "Voornaam",
    "firstSummarySheetsGenerated"           : "Only first 500 summary sheets will be generated.", //_TO_TRANSLATE
    "folder"                                : "Map",
    "folderName"                            : "_TO_TRANSLATE",
    "folders"                               : "Mappen",
    "folderStatus"                          : "_TO_TRANSLATE",
    "folderTypeList"                        : "Lijst van de maptypes",
    "fontBig"                               : "Big", //_TO_TRANSLATE
    "fontBold"                              : "Bold", //_TO_TRANSLATE
    "for"                                   : "voor",
    "forCapital"                            : "Voor",
    "format"                                : "Indeling",
    "freeNote"                              : "_TO_TRANSLATE",
    "friday"                                : "Vrijdag",
    "from"                                  : "Vanaf",
    "fromRange"                             : "van",
    "functionnalities"                      : "Functies",
    "genSummarySheets"                      : "_TO_TRANSLATE",
    "getAssignee"                           : "User destination (entity destination)", //_TO_TRANSLATE
    "getAssigneeSample"                     : "Barbara BAIN (Finance division)", //_TO_TRANSLATE
    "getCategory"                           : "Category", //_TO_TRANSLATE
    "getCreationAndProcessLimitDates"       : "Creation date - Process limit date", //_TO_TRANSLATE
    "getCreationAndProcessLimitDatesSample" : "1st jan. - <i class=\"fa fa-stopwatch\"></i>&nbsp;<b color=\"warn\">3 day(s)</b>", //_TO_TRANSLATE
    "getDoctype"                            : "Mail type", //_TO_TRANSLATE
    "getDoctypeSample"                      : "Reclamation", //_TO_TRANSLATE
    "getModificationDate"                   : "Modification date", //_TO_TRANSLATE
    "getOpinionLimitDate"                   : "Opinion limit date", //_TO_TRANSLATE
    "getParallelOpinionsNumber"             : "Number of opinions sent", //_TO_TRANSLATE
    "getParallelOpinionsNumberSample"       : "<b>3</b> opinion(s) sent", //_TO_TRANSLATE
    "getPriority"                           : "Priority", //_TO_TRANSLATE
    "getRecipients"                         : "Recipient", //_TO_TRANSLATE
    "getSenders"                            : "Sender", //_TO_TRANSLATE
    "getSignatories"                        : "Signatories", //_TO_TRANSLATE
    "getVisaWorkflow"                       : "Goedkeuringscircuit", //_TO_TRANSLATE
    "groupAdded"                            : "Toegevoegde groep",
    "groupCreation"                         : "Aanmaak van een groep",
    "groupDeleted"                          : "Verwijderde groep",
    "groupeCompany"                         : "vennootschap van de Xelians groep",
    "grouping"                              : "Grouping", //_TO_TRANSLATE
    "groupModification"                     : "Wijziging van een groep",
    "groupRedirect"                         : "Groepswijziging",
    "groups"                                : "Groepen",
    "adminGroupsDesc"                       : "Gebruikersgroepen toevoegen, opschorten of wijzigen. Privileges of toegangstoelatingen aan assets toekennen.",
    "groupServicesUpdated"                  : "Gewijzigde voorrechten",
    "groupUpdated"                          : "Gewijzigde groep",
    "hello"                                 : "Hallo",
    "history"                               : "Geschiedenis",
    "historyBatch"                          : "Geschiedenis van de batches",
    "home"                                  : "Homepage",
    "homePage"                              : "Homepagina",
    "host"                                  : "_TO_TRANSLATE",
    "hour"                                  : "Uur",
    "html"                                  : "HTML",
    "id"                                    : "Gebruikersnaam",
    "imgRelated"                            : "Gekoppeld beeld",
    "immediatelyBelowMyPrimaryEntity"       : "Immediately below my primary entity", //_TO_TRANSLATE
    "immediatelySuperiorMyPrimaryEntity"    : "Immediately superior to my primary entity", //_TO_TRANSLATE
    "importFile"                            : "Een bestand importeren",
    "inactive"                              : "Niet actief",
    "inactives"                             : "Niet actief",
    "incoming"                              : "Brief Aangekomen",
    "incompleteAddressForPostal"            : "Incomplete address for postal TRANSLATE",
    "indexing"                              : "Indexeringen",
    "informations"                          : "Gegevens",
    "infosActions"                          : "U moet minstens een status en/of een script kiezen.",
    "initials"                              : "Initialen",
    "initiator"                             : "_TO_TRANSLATE",
    "initiatorEntity"                       : "_TO_TRANSLATE",
    "integer"                               : "Geheel",
    "internal"                              : "Interne brief",
    "ip"                                    : "IP-adres",
    "isAssociatedTo"                        : "is gekoppeld aan",
    "isCopyTo"                              : "is in kopie van",
    "isDestTo"                              : "is de bestemming van",
    "isFolderAction"                        : "Mapactie",
    "isFolderActionDesc"                    : "Om deze actie in een mapbakje te gebruiken",
    "isFolderBasket"                        : "Mapbakje",
    "isFolderStatus"                        : "Mapstatus",
    "isLinkedTo"                            : "is gekoppeld aan",
    "isMulticontact"                        : "_TO_TRANSLATE",
    "isNotAvailable"                        : "is not available", //_TO_TRANSLATE
    "isSearchBasket"                        : "Enkel opzoekingsbakje",
    "isSytemAction"                         : "Actie systeem",
    "january"                               : "Januari",
    "januaryShort"                          : "Jan.",
    "july"                                  : "Juli",
    "julyShort"                             : "Juli",
    "june"                                  : "Juni",
    "juneShort"                             : "Juni",
    "keyword"                               : "Sleutelwoord",
    "keywordHelp"                           : "Hulp voor sleutelwoorden",
    "keywordHelpDesc_1"                     : "Gebruikersnaam van de verbonden gebruiker",
    "keywordHelpDesc_12"                    : "Technical user id of logged user", //_TO_TRANSLATE
    "keywordHelpDesc_10"                    : "Subeenheden die direct onder (n-1) de in het argument gegeven eenheden liggen",
    "keywordHelpDesc_11"                    : "Voorbeeld van documentaire perimeter toegang tot assets met betrekking tot de hoofddienst van de verbonden gebruiker of tot de subdiensten van deze dienst",
    "keywordHelpDesc_2"                     : "E-mail van de verbonden gebruiker",
    "keywordHelpDesc_3"                     : "Alle eenheden die aan de verbonden gebruiker gekoppeld zijn Bevat geen subeenheden",
    "keywordHelpDesc_4"                     : "Primaire eenheid van de verbonden gebruiker",
    "keywordHelpDesc_5"                     : "Subeenheden van de argumentenlijst die ook @my_entities of @my_primary_entity kan zijn",
    "keywordHelpDesc_6"                     : "Bovenliggende eenheid van de eenheid in het argument",
    "keywordHelpDesc_7"                     : "Alle eenheden van hetzelfde niveau van de eenheid in het argument",
    "keywordHelpDesc_8"                     : "Alle eenheden van hetzelfde type dat in het argument voorkomt",
    "keywordHelpDesc_9"                     : "Alle (actieve) eenheden",
    "keywordHelper"                         : "De hulp voor de sleutelwoorden weergeven / verbergen",
    "label"                                 : "Omschrijving",
    "last"                                  : "laatste",
    "lastname"                              : "Naam",
    "life_cycle"                            : "Levenscyclus",
    "limitDataReached_1000"                 : "Limiet van <b>1000 ingangen</b> bereikt.",
    "limitDataReached"                      : "U heeft de weergavegrens bereikt (<b>25000 ingangen</b>) verfijn uw <b>databereik</b>",
    "linkContact"                           : "Een contact koppelen",
    "linkDetails"                           : "Krijg toegang tot de gedetailleerde fiche",
    "linkEntityToUserInfo"                  : "Klik op een eenheid van de boomstructuur om deze te koppelen aan / ontkoppelen van de gebruiker",
    "linkGroup"                             : "Een groep koppelen",
    "links"                                 : "Verbindingen",
    "linkUser"                              : "Een gebruiker koppelen",
    "listTemplatesRolesUpdated"             : "Gewijzigde rollen",
    "loading"                               : "Aan het laden ...",
    "lockDocserver"                         : "De opslagruimte vergrendelen (enkel lezen)",
    "lockedBy"                              : "locked by", //_TO_TRANSLATE
    "logout"                                : "Verbinding verbreken",
    "maarchApplication"                     : "Maarch Applicatie",
    "maarchLicence"                         : "Maarch Courrier wordt verspreid onder de voorwaarden van de",
    "mail"                                  : "Brief",
    "mails"                                  : "mails", //_TO_TRANSLATE
    "mailAttachments"                       : "bijkomend(e) stuk(ken)",
    "mailDate"                              : "_TO_TRANSLATE",
    "mailEntitiesList"                      : "List of entities attached to mails", //_TO_TRANSLATE
    "maileva_addressPage"                   : "Address page", //_TO_TRANSLATE
    "maileva_color"                         : "Color", //_TO_TRANSLATE
    "maileva_duplexPrinting"                : "Both sides", //_TO_TRANSLATE
    "maileva_economic"                      : "Economic (J+4)", //_TO_TRANSLATE
    "maileva_fast"                          : "Fast (J+2)", //_TO_TRANSLATE
    "maileva_firstPagePrice"                : "1st page", //_TO_TRANSLATE
    "maileva_nextPagePrice"                 : "Next page(s)", //_TO_TRANSLATE
    "maileva_postagePrice"                  : "Postage up to 20g", //_TO_TRANSLATE
    "maileva_registered_mail_ar"            : "Registered mail AR", //_TO_TRANSLATE
    "maileva_registered_mail"               : "Registered mail", //_TO_TRANSLATE
    "mailevaAccount"                        : "Maileva account", //_TO_TRANSLATE
    "mailFrom"                              : "_TO_TRANSLATE",
    "mailNote"                              : "Annotation of the main document", //_TO_TRANSLATE
    "mailTo"                                : "_TO_TRANSLATE",
    "mailType"                              : "_TO_TRANSLATE",
    "makeActionOn"                          : "Do you want to do this action on", //_TO_TRANSLATE
    "manageAbsences"                        : "Mijn bakjes doorsturen",
    "manageSignatures"                      : "De handtekeningen beheren",
    "mandatory"                             : "Verplicht",
    "march"                                 : "Maart",
    "marchShort"                            : "Maart",
    "maxSize"                               : "Maximumgrootte",
    "may"                                   : "Mei",
    "mayShort"                              : "Mei",
    "memberAllUsers"                        : "Alle gebruikers leden",
    "memberDiffTypeUsers"                   : "Personen die lid zijn van het verdelingstype",
    "memberUserDest"                        : "Gebruiker bestemmeling lid",
    "memberUsersCopy"                       : "Gebruiker(s) in kopie lid (leden)",
    "menu"                                  : "Menu",
    "menus"                                 : "Menu’s",
    "minute"                                : "Minuut",
    "minutes"                               : "minu(u)t(en),",
    "missingAdvert"                         : "Deze account staat momenteel in modus ‘afwezig’.",
    "missingAdvert2"                        : "Indien u zich met deze account wil verbinden, zal de modus ‘afwezig’ verwijderd worden.",
    "missingAdvertTitle"                    : "Afwezigheidsbeheer",
    "missingBasket"                         : "_TO_TRANSLATE",
    "missingChoose"                         : "Wenst u uw account opnieuw te activeren?",
    "missingMailevaConfig"                  : "Maileva configuration does not exist _TO_TRANSLATE",
    "disabledMailevaConfig"                 : "Maileva configuration is disabled",//TRANSLATE
    "modelUpdated"                          : "Het model werd bijgewerkt",
    "modificationSaved"                     : "Wijziging bewaard",
    "modifiedBy"                            : "Modified by", //_TO_TRANSLATE
    "modifiedOn"                            : "Modified on", //_TO_TRANSLATE
    "module"                                : "Module",
    "modules"                               : "Modules",
    "monday"                                : "Maandag",
    "month"                                 : "Maand",
    "moreOptions"                           : "Meer opties",
    "move"                                  : "Verplaatsen",
    "mustCompleteAR"                        : "You must complete one of two acknowledgement of receipts", //_TO_TRANSLATE
    "mustReconnect"                         : "_TO_TRANSLATE",
    "myBaskets"                             : "Mijn Bakjes",
    "myBasketsDesc"                         : "Mijn bakjes naar andere gebruikers doorsturen",
    "myContactsGroups"                      : "Mijn contactgroeperingen",
    "myContactsGroupsDesc"                  : "Mijn contactlijsten beheren",
    "myEntities"                            : "My entities", //_TO_TRANSLATE
    "myHistoric"                            : "mijn geschiedenis",
    "myInformations"                        : "Mijn informatie",
    "myLastResources"                       : "Mijn laatste geraadpleegde mails",
    "myMenu"                                : "Persoonlijk menu",
    "myParameters"                          : "Mijn instellingen",
    "myPreferences"                         : "Mijn voorkeuren",
    "myPrimaryEntity"                       : "My primary entity", //_TO_TRANSLATE
    "myProfile"                             : "Mijn profiel",
    "myProfileAccess"                       : "Toegang hebben tot mijn profiel",
    "mySignMail"                            : "Mijn handtekeningen (e-mail)",
    "mySignMailDesc"                        : "Mijn e-mailhandtekeningen beheren",
    "mySignSignatureBook"                   : "Mijn handtekeningen (vloeiboek)",
    "mySignSignatureBookDesc"               : "Mijn parafen beheren",
    "nature"                                : "_TO_TRANSLATE",
    "newAccount"                            : "New account", //_TO_TRANSLATE
    "newAction"                             : "Nieuwe actie",
    "newAssignee"                           : "Nieuwe ontvanger", //_TO_TRANSLATE
    "newDest"                               : "New recipient for", //_TO_TRANSLATE_
    "newDestRes"                            : "New recipient for mails", //_TO_TRANSLATE
    "newElement"                            : "Nieuw element",
    "newPsw"                                : "Nieuw wachtwoord",
    "newSignature"                          : "Nieuwe handtekening",
    "newVersionAvailable"                   : "Er is een nieuwe versie beschikbaar",
    "next"                                  : "Volgende",
    "no"                                    : "Nee",
    "noAction"                              : "No action", //_TO_TRANSLATE
    "noAttachment"                          : "Zonder bijlage",
    "noAttachmentClickToAddOne"             : "No attachments available<br/><sub>Click on <i class=\"fa fa-paperclip fa-lg\" ></i><i class=\"fa fa-plus\"></i> to add one.</sub>", //_TO_TRANSLATE
    "noAttachmentContact"                   : "No contact (external) linked to this attachment",//TRANSLATE
    "noAttachmentConversion"                : "No PDF conversion for this attachment TRANSLATE",
    "noAttachmentInSignatoryBook"           : "No item in signature book", //_TO_TRANSLATE
    "noSignableAttachmentInSignatoryBook"   : "No signable attachment in signature book", //_TO_TRANSLATE
    "noAttachmentToSend"                    : "No attachment to send", //_TO_TRANSLATE
    "noDocumentToSend"                      : "No document to send", //_TO_TRANSLATE
    "noAvailableBasket"                     : "Geen bakje beschikbaar",
    "noDataAvailable"                       : "_TO_TRANSLATE",
    "noDatasource"                          : "Geen gegevensbron",
    "noDiffusionList"                       : "No diffusion list", //_TO_TRANSLATE
    "noDoctypeSelected"                     : "Geen documenttype geselecteerd ",
    "noEntity"                              : "Geen enkele eenheid",
    "noFirstLevelSelected"                  : "Geen enkele submap geselecteerd",
    "noFranceContact"                       : "Address is not in France for this contact TRANSLATE",
    "noMailConversion"                      : "Mail does not have PDF conversion", //_TO_TRANSLATE
    "noMove"                                : "Deze eenheid / gebruiker kan niet verplaatst worden",
    "none"                                  : "_TO_TRANSLATE",
    "noNote"                                : "Geen notities", //_TO_TRANSLATE
    "noNumericPackageSelected"              : "Geen enkele digitale brief geselecteerd",
    "noOverviewAvailable"                   : "No overview available", //_TO_TRANSLATE
    "noPerson"                              : "Geen persoon", //_TO_TRANSLATE
    "noRecord"                              : "Geen onderdelen",
    "noReplacement"                         : "Geen vervanging",
    "noResult"                              : "Geen resultaat",
    "noSettingsAvailable"                   : "Geen instellingen beschikbaar", //_TO_TRANSLATE
    "noShippingTemplate"                    : "No common shipping Maileva templates available for these entities", //_TO_TRANSLATE
    "noTemplate"                            : "No template", //_TO_TRANSLATE
    "notes"                                 : "Opmerkingen",
    "noteTemplates"                         : "Note Templates", //_TO_TRANSLATE
    "notificationAdded"                     : "Melding toegevoegd",
    "notificationCreation"                  : "Aanmaak van een melding",
    "notificationDeleted"                   : "Melding verwijderd",
    "NotificationDiffusionType"             : "Verdelingstype",
    "NotificationEnabled"                   : "Geactiveerd",
    "NotificationEvent"                     : "Evenement",
    "notificationJoinDocument"              : "Het hoofddocument bijvoegen",
    "notificationModel"                     : "Model van de verzendingsmail",
    "notificationModification"              : "Wijziging van de melding",
    "notifications"                         : "Melding(en)",
    "notificationSchedule"                  : "De meldingen plannen",
    "NotificationScheduleInfo"              : "Ter herinnering\n\nformaat van crontab : [minuut]  [uur]  [dag van de maand]  [maand]  [dag van de week]",
    "notificationScheduleUpdated"           : "Geplande ta(a)k(en) bijwerking",
    "notificationsSchedule"                 : "Planning van de meldingen",
    "NotificationToEnable"                  : "Inschakelen",
    "notificationUpdated"                   : "Melding van de bijwerking",
    "notUsed"                               : "Niet gebruikt",
    "noUserDefinedInMaarchParapheur"        : "No user created in Maarch Parapheur", //_TO_TRANSLATE
    "noUserReplacement"                     : "Geen vervangingsgebruiker",
    "november"                              : "November",
    "novemberShort"                         : "Nov.",
    "noVisaWorkflowNoSignature"             : "No visa workflow set up. <br/>><sub>No signature possible. </sub>", //_TO_TRANSLATE
    "numericPackageImported"                : "Digitale brief correct geïmporteerd",
    "object"                                : "Onderwerp",
    "objectSample"                          : "Subject sample", //_TO_TRANSLATE
    "october"                               : "Oktober",
    "octoberShort"                          : "Okt.",
    "office"                                : "Kantoor",
    "officialWebsite"                       : "Officiële site",
    "oneHour"                               : "Een uur",
    "oneMinute"                             : "een minuut",
    "onRange"                               : "op", //_TO_TRANSLATE
    "openIndexation"                        : "Indexing page will be open so that you can create a new mail", //_TO_TRANSLATE
    "opinionsSent"                          : "opinion(s) sent", //_TO_TRANSLATE
    "optional"                              : "Optioneel",
    "OrderBy"                               : "_TO_TRANSLATE",
    "orderBy"                               : "Sorteren",
    "organization"                          : "ORGANISATIE",
    "otherActions"                          : "Bijkomende actie(s)",
    "otherInformations"                     : "Bijkomend(e) informatie",
    "otherParameters"                       : "Ander(e) instelling(en)",
    "others"                                : "Ander(e)",
    "othersBaskets"                         : "Andere bakjes",
    "outdated"                              : "Verstreken",
    "outgoing"                              : "Brief vertrek",
    "outOf"                                 : "van",
    "page"                                  : "Pagina",
    "paperTemplate"                         : "Paper template", //_TO_TRANSLATE
    "parameter"                             : "Instelling",
    "parameterAdded"                        : "Instelling toegevoegd",
    "parameterCreation"                     : "Aanmaak van een instelling",
    "parameterDeleted"                      : "Instelling verwijderd",
    "parameterModification"                 : "Wijziging van een instelling",
    "parameters"                            : "Instellingen",
    "parameterUpdated"                      : "Instelling gewijzigd",
    "parentFolder"                          : "_TO_TRANSLATE",
    "password"                              : "Wachtwoord,",
    "password2renewal"                      : "Na deze termijn moet u er een nieuw kiezen",
    "passwordChanged"                       : "Uw wachtwoord werd gewijzigd",
    "passwordcomplexityNumber"              : "minstens 1 cijfer",
    "passwordcomplexityNumberRequired"      : "Cijfer vereist",
    "passwordcomplexitySpecial"             : "minstens 1 speciaal karakter",
    "passwordcomplexitySpecialRequired"     : "Speciaal teken vereist",
    "passwordcomplexityUpper"               : "minstens 1 hoofdletter",
    "passwordcomplexityUpperRequired"       : "Hoofdletter vereist",
    "passwordhistoryLastUseDesc"            : "U mag het/de",
    "passwordhistoryLastUseDesc2"           : "laatste wachtwoord(en) niet gebruiken",
    "passwordhistoryLastUseRequired"        : "Aantal opgeslagen wachtwoorden",
    "passwordlockAttemptsRequired"          : "Aantal verbindingspogingen",
    "passwordlockTimeRequired"              : "Blokkeringstijd",
    "passwordMatch"                         : "De wachtwoorden zijn identiek",
    "passwordminLength"                     : "karakter(s) minimum",
    "passwordminLengthRequired"             : "Minimumlengte",
    "passwordModification"                  : "Wijziging van wachtwoord",
    "passwordNotMatch"                      : "De wachtwoorden komen niet overeen",
    "passwordrenewal"                       : "Hou er rekening mee dat uw nieuw wachtwoord maar geldig zal zijn voor",
    "passwordrenewalRequired"               : "Wachtwoord vervallen",
    "passwordRulesUpdated"                  : "Veiligheidsregels bijgewerkt",
    "passwordUpdated"                       : "Wachtwoord gewijzigd",
    "passwordValid"                         : "Conform wachtwoord",
    "path"                                  : "Path", //_TO_TRANSLATE
    "patternId"                             : "De waarde mag enkel alfanumerieke karakters, punten, underscores of streepjes bevatten",
    "pdfVersionFile"                        : "The PDF version of the file", //_TO_TRANSLATE
    "phoneNumber"                           : "Telefoonnummer",
    "port"                                  : "_TO_TRANSLATE",
    "preview"                               : "Preview", //_TO_TRANSLATE
    "previous"                              : "Vorige",
    "pricesInformations"                    : "Prices informations", //_TO_TRANSLATE
    "primary"                               : "Primair",
    "primaryEntity"                         : "Primaire eenheid",
    "primaryInformations"                   : "_TO_TRANSLATE",
    "printDate"                             : "_TO_TRANSLATE",
    "printResultList"                       : "_TO_TRANSLATE",
    "priorities"                            : "Prioriteit(en)",
    "prioritiesAlt"                         : "Prioriteiten",
    "prioritiesHelpDesc"                    : "De prioriteiten slepen-neerzetten om de volgorde te wijzigen",
    "prioritiesOrder"                       : "De volgorde van de prioriteiten beheren",
    "priority"                              : "Prioriteit",
    "priorityAdded"                         : "Toegevoegde prioriteit",
    "priorityCreation"                      : "Aanmaak van een prioriteit",
    "priorityDeleted"                       : "Verwijderde prioriteit",
    "priorityModification"                  : "Wijziging van een prioriteit",
    "prioritySample"                        : "Hight", //_TO_TRANSLATE
    "priorityUpdated"                       : "Gewijzigde prioriteit",
    "private"                               : "Privé",
    "privileges"                            : "Voorrechten",
    "process_limit_date"                    : "_TO_TRANSLATE",
    "processAction"                         : "Verwerkingsactie(s)",
    "processDate"                           : "_TO_TRANSLATE",
    "processDelay"                          : "Verwerkingstermijn",
    "processDelayDay"                       : "Verwerkingstermijn (in dagen)",
    "processLimitDate"                      : "Verwerkingsdeadline",
    "processMode"                           : "Verwerkingswijze for acknowledgement receipt", //_TO_TRANSLATE
    "processType"                           : "Verwerkingstype",
    "production"                            : "PRODUCTIE",
    "progression"                           : "Vooruitgang",
    "pswReseted"                            : "Wachtwoord opnieuw ingesteld",
    "public"                                : "Openbaar",
    "putInSendAttach"                       : "Put in send attachement Maileva", //_TO_TRANSLATE
    "putInSignatureBook"                    : "Put in signature book", //_TO_TRANSLATE
    "quickSearchInfo"                       : "Target search ", //_TO_TRANSLATE
    "inSignatureBook"                       : "In signature Book", //_TO_TRANSLATE
    "inShipping"                            : "In shipping", //_TO_TRANSLATE
    "quickSearchTarget"                     : "subject (mail / attachment), chrono number (courrier / attachment), notes content, barcode, EDM number (mail)", //_TO_TRANSLATE
    "quota"                                 : "Quota",
    "quotaExceeded"                         : "Quota overschreden",
    "reactivateUserDeleted"                 : "De ingevoerde gebruikersnaam is toegekend aan een verwijderde gebruiker. Wilt u deze opnieuw activeren?",
    "readyToGeneratePaperAr"                : "paper acknowledgement receipt ready to be generated", //_TO_TRANSLATE
    "readyToSendElectronicAr"               : "electronic acknowledgement receipt ready to be send", //_TO_TRANSLATE
    "reaffectUserRedirect"                  : "Terug toekennen aan de gebruiker",
    "reaffectUserRedirectInformations"      : "De gebruiker wordt vervangen door de geselecteerde gebruiker in de mailinglijsten van de mail en template van de entiteit", //_TO_TRANSLATE
    "reallyWantToDeleteContactFromGroup"    : "Weet u zeker dat u dit contact uit de groep wil verwijderen?",
    "reassign"                              : "Opnieuw toewijzen",
    "recipient"                             : "_TO_TRANSLATE",
    "recipients"                            : "_TO_TRANSLATE",
    "record"                                : "element(en)",
    "records"                               : "resultaten",
    "recordsPerPage"                        : "resultaten per pagina",
    "redirectBasket"                        : "Het bakje doorsturen",
    "redirectBaskets"                       : "De bakjes doorsturen",
    "redirectConfidentialInfo"              : "Deze gebruiker heeft <b>vertrouwelijke brieven</b> die aan deze dienst gekoppeld zijn.<br/>Het is beter om deze aan een andere gebruiker van de dienst toe te wijzen.",
    "redirectedTo"                          : "Doorgestuurd aan",
    "redirectModelInfo"                     : "Deze gebruiker is aanwezig in het <b>verdelingsmodel</b> van de eenheid.",
    "redirects"                             : "Doorsturingen",
    "redirectTo"                            : "Doorverwijzen naar", //_TO_TRANSLATE
    "redirectUserListDiff"                  : "Wijziging van bestemmeling van de modellen van de verdelingslijst",
    "redirectUserListInstances"             : "Change of recipient for mail being processed", //_TO_TRANSLATE_
    "redirectWhenAbscence"                  : "Het bakje aan een persoon doorsturen",
    "reference"                             : "_TO_TRANSLATE",
    "relatedBaskets"                        : "Gekoppeld(e) bakje(s)",
    "relatedContactNumber"                  : "Gekoppeld(e) contact(en)",
    "relatedContacts"                       : "Gekoppeld(e) contact(en)",
    "relatedUsers"                          : "Gekoppelde gebruiker(s)",
    "removeAllDatas"                        : "_TO_TRANSLATE",
    "removeUserRedirect"                    : "Verwijder de gebruiker van de mailinglijsten",
    "removeUserRedirectInformations"        : "<em>De gebruiker wordt verwijderd uit de mailinglijsten en sjabloon van de entiteit <b>behalve in het geval dat het wordt toegekend</b></em>", //_TO_TRANSLATE
    "renewPsw"                              : "Wachtwoord opnieuw intypen",
    "reports"                               : "Statistieken",
    "requiredField"                         : "Vereist veld",
    "resetColor"                            : "De kleur resetten",
    "resId"                                 : "GED gebruikersnaam",
    "restrictedEntity"                      : "Restricted entity", //_TO_TRANSLATE
    "resultPage"                            : "Resultaatpagina",
    "retentionFinalDisposition"             : "Uiteindelijke lot",
    "retentionRule"                         : "Bewaringsregel",
    "returnDate"                            : "Datum van terugkeer", //_TO_TRANSLATE
    "retypeNewPassword"                     : "Voer het wachtwoord opnieuw in",
    "role"                                  : "Functie",
    "roles"                                 : "Roles", //_TO_TRANSLATE
    "roleUsedInTemplateInfo"                : "De functie wordt gebruikt in de volgende diensten",
    "roleUsedInTemplateInfo2"               : "De gebruikers die aan deze rol gekoppeld zijn, zullen van de verdelingslijst geschrapt worden.",
    "sameLevelMyPrimaryEntity"              : "Same level of my primary entity", //_TO_TRANSLATE
    "saturday"                              : "Zaterdag",
    "save"                                  : "Bewaren",
    "manageNumericPackage"                  : "Een digitale brief bewaren",
    "saveNumericPackage"                    : "Save numeric package", //_TO_TRANSLATE
    "sbSignatures"                          : "Handtekeningen van de parafeerder",
    "script"                                : "Script",
    "scriptCreated"                         : "Script aangemaakt",
    "search"                                : "Zoeken",
    "searchByAttachmentType"                : "Search by attachment type", //_TO_TRANSLATE
    "searchDatas"                           : "_TO_TRANSLATE",
    "searchEntities"                        : "Een eenheid zoeken",
    "searchMailInBasket"                    : "_TO_TRANSLATE",
    "searchMails"                           : "Mails zoeken",
    "searchNewAssignee"                     : "Search for a new assignee", //_TO_TRANSLATE
    "secondaryEntity"                       : "Secundaire eenheid",
    "secondaryInformations"                 : "_TO_TRANSLATE",
    "secondLevelAdded"                      : "Submap aangemaakt",
    "secondLevelAttached"                   : "Aan de submap koppelen",
    "secondLevelDeleted"                    : "Submap verwijderd",
    "secondLevelDoctype"                    : "Submap",
    "secondLevelUpdated"                    : "Submap bijgewerkt",
    "securitiesAdministration"              : "Veiligheidsbeheer",
    "securityConstraints"                   : "Veiligheidsregels",
    "selectAction"                          : "Select an action", //_TO_TRANSLATE
    "selectAll"                             : "Alles selecteren",
    "selectAllResInBasket"                  : "_TO_TRANSLATE",
    "selected"                              : "selected", //_TO_TRANSLATE
    "selectedAccount"                       : "Selected account", //_TO_TRANSLATE
    "selectedDocumentStatus"                : "Rekening houden met de status van het brief",
    "selectedElements"                      : "_TO_TRANSLATE",
    "sender"                                : "_TO_TRANSLATE",
    "senderRecipientInformations"           : "_TO_TRANSLATE",
    "senders"                               : "_TO_TRANSLATE",
    "sendmail"                              : "E-mails verzenden",
    "sendmailShort"                         : "_TO_TRANSLATE",
    "sendMode"                              : "Send mode", //_TO_TRANSLATE
    "sendModes"                             : "Send modes", //_TO_TRANSLATE
    "sendTo"                                : "Verzenden aan",
    "september"                             : "September",
    "septemberShort"                        : "Sept.",
    "setByDefault"                          : "set by default", //_TO_TRANSLATE
    "settingsList"                          : "_TO_TRANSLATE",
    "shapingOptions"                        : "Shapings options", //_TO_TRANSLATE
    "shippingAdded"                         : "Shipping Maileva added", //_TO_TRANSLATE
    "shippingCreation"                      : "Shipping Maileva creation", //_TO_TRANSLATE
    "shippingDeleted"                       : "Shipping Maileva deleted", //_TO_TRANSLATE
    "shippingModification"                  : "Shipping Maileva modification", //_TO_TRANSLATE
    "shippingNotEligible"                   : "Ineligible mail(s) ", //_TO_TRANSLATE
    "shippingReadyToSend"                   : "document(s) ready to be sent", //_TO_TRANSLATE
    "shippings"                             : "Shippings Maileva templates", //_TO_TRANSLATE
    "shippingUpdated"                       : "Shipping Maileva updated", //_TO_TRANSLATE
    "shortcut"                              : "Sneltoetsen",
    "shortLabel"                            : "Korte omschrijving",
    "signAdded"                             : "Handtekening toegevoegd",
    "signatureAdded"                        : "Handtekening toegevoegd",
    "signatureDate"                         : "_TO_TRANSLATE",
    "signatureDeleted"                      : "Verwijderde handtekening",
    "signatures"                            : "Handtekeningen",
    "signatureUpdated"                      : "Bijgewerkte handtekening",
    "signDeleted"                           : "Verwijderde handtekening",
    "signed"                                : "Ondertekend",
    "signsSynchronized"                     : "Signatures have been updated in Maarch Parapheur", //_TO_TRANSLATE
    "signUpdated"                           : "Gewijzigde handtekening",
    "signUser"                              : "Ondertekenaar",
    "since"                                 : "Sinds",
    "siret"                                 : "SIRET number", //_TO_TRANSLATE
    "siretCode"                             : "SIRET-nummer",
    "phpmail"                               : "Function php 'mail'", //_TO_TRANSLATE
    "qmail"                                 : "Qmail",
    "smtprelay"                             : "Sendmail",
    "smtpclient"                            : "Smtp",
    "smtpAuth"                              : "_TO_TRANSLATE",
    "status"                                : "Status",
    "statusAdded"                           : "Status toegevoegd",
    "statusCreation"                        : "Aanmaak van een status",
    "statusDeleted"                         : "Status verwijderd",
    "statuses"                              : "Status(sen)",
    "statusModification"                    : "Wijziging van status",
    "statusName"                            : "Statusnaam",
    "statusUpdated"                         : "Bijgewerkte status",
    "stringInput"                           : "Karakterreeks",
    "subEntities"                           : "_TO_TRANSLATE",
    "subject"                               : "_TO_TRANSLATE",
    "summarySheet"                          : "_TO_TRANSLATE",
    "summarySheets"                         : "_TO_TRANSLATE",
    "summarySheetsAlt"                      : "_TO_TRANSLATE",
    "sunday"                                : "Zondag",
    "supervision"                           : "TOEZICHT",
    "suspend"                               : "Opschorten",
    "suspended"                             : "Opgeschort",
    "suspendNotification"                   : "De melding opschorten",
    "sveStartDate"                          : "_TO_TRANSLATE",
    "syncSignsToMaarchParapheur"            : "Synchronize signatures to Maarch Parapheur", //_TO_TRANSLATE
    "system"                                : "Systeem",
    "systemParameters"                      : "systeeminstellingen",
    "tags"                                  : "Sleutelwoorden",
    "adminTagsDesc"                         : "Voor het wijzigen, verwijderen, toevoegen of fusioneren van sleutelwoorden vanuit de beheerinterface.",
    "templateAdded"                         : "Model toegevoegd",
    "templateAssociateEntities"             : "List of entities already associated with a template", //_TO_TRANSLATE
    "templateCheckEntities"                 : "The following entities already have a registered template for the type : ", //_TO_TRANSLATE
    "templateCreation"                      : "Aanmaak van het model",
    "templateDatasource"                    : "Gegevensbron",
    "templateDeleted"                       : "Model verwijderd",
    "templateDuplicated"                    : "Model gedupliceerd",
    "templateEdition"                       : "Bewerking van het model",
    "templateModification"                  : "Wijziging van het model",
    "templateName"                          : "Naam van het model",
    "templates"                             : "Documentmodellen",
    "templateServiceInfo"                   : "Kies de dienst(en) waarmee u dit model wil koppelen",
    "templateTarget"                        : "Doel van het model",
    "templateType"                          : "Modeltype",
    "templateUpdated"                       : "Model bijgewerkt",
    "thanksToCommunity"                     : "En de volledige Maarch-community!",
    "thanksToExtDev"                        : "Maarch Courrier steunt op enkele externe onderdelen. Hartelijk dank aan de ontwikkelaars ervan!",
    "thisRes"                               : "Following mail(s)", //_TO_TRANSLATE
    "tuesday"                               : "Dinsdag",
    "thursday"                              : "Donderdag",
    "title"                                 : "Titel",
    "to"                                    : "naar",
    "toAnUser"                              : "Naar een gebruiker", //_TO_TRANSLATE
    "toAService"                            : "Naar een dienst", //_TO_TRANSLATE
    "toEntities"                            : "Naar de diensten",
    "toExport"                              : "_TO_TRANSLATE",
    "toggleQrcode"                          : "_TO_TRANSLATE",
    "tools"                                 : "Tools",
    "toolTipDeleteDoctype"                  : "Indien er aan dit documenttype brieven zijn gekoppeld zal er een scherm van nieuwe toewijzing weergegeven worden",
    "tooltipFolderStatus"                   : "Indien dit aangevinkt is, zal de status gebruikt kunnen worden voor mapbakjes",
    "tooltipIndexStatus"                    : "Indien dit aangevinkt is, zal u de metagegevens van de brieven met deze status kunnen wijzigen",
    "tooltipSearchStatus"                   : "Indien  aangevinkt zal de status u voorgesteld worden in het opzoekcriterium STATUSSEN van de geavanceerde opzoeking",
    "tooMuchDatas"                          : "Too much datas to export", //_TO_TRANSLATE
    "toRange"                               : "naar",
    "toSchedule"                            : "Plannen",
    "toStatuses"                            : "Naar de statussen",
    "toStatusesDesc"                        : "U kunt geen status kiezen Voor deze actie is er reeds een status voorgedefinieerd.",
    "totalErrors"                           : "Foutief (ve) element(en)",
    "totalPrice"                            : "Total price", //_TO_TRANSLATE
    "totalProcessed"                        : "Geanalyseerd(e) element(en)",
    "toUsersEntities"                       : "Naar de gebruikers van de diensten",
    "transferToDoctype"                     : "Vervangen door het documenttype",
    "triggerAction"                         : "Activerende actie",
    "triggerSystem"                         : "Starten van het systeem",
    "txt"                                   : "Tekst",
    "type_label"                            : "_TO_TRANSLATE",
    "type"                                  : "Type",
    "typeCurrentPassword"                   : "Voer uw huidig wachtwoord in",
    "typeNewPassword"                       : "Voer een nieuw wachtwoord in",
    "typist"                                : "_TO_TRANSLATE",
    "unableToDelete"                        : "Niet te onderdrukken", //_TO_TRANSLATE
    "unableToSuspend"                       : "Schorsing onmogelijk", //_TO_TRANSLATE
    "undefined"                             : "_TO_TRANSLATE",
    "undefinedStatus"                       : "_TO_TRANSLATE",
    "unlinkAction"                          : "De actie ontkoppelen",
    "unlinkGroup"                           : "De groep ontkoppelen",
    "unselectAll"                           : "Selectie volledig ongedaan maken",
    "unsign"                                : "Unsign", //_TO_TRANSLATE
    "until"                                 : "Tot",
    "update"                                : "Wijzigen",
    "updateAcknowledgementSendDate"         : "This will update send date for paper acknowledgement receipt", //_TO_TRANSLATE
    "updateAtt"                             : "De bijlage wijzigen",
    "updateClosingDate"                     : "This will update mail <b class=\"highlight\">closing date</b>.", //_TO_TRANSLATE
    "updateDepartureDate"                   : "This will update mail <b class=\"highlight\">departure date</b>.", //_TO_TRANSLATE
    "updateStatus"                          : "Statuswijziging van de brief",
    "updateStatusInformationsChrono"        : "Door het CHRONO NUMMER van het document in te geven, wijzigt u de status van de brief. Deze brief zal beschikbaar zijn in het gebruikersbakje waaraan hij toegewezen was volgens de status die u gedefinieerd hebt.",
    "updateStatusInformationsGed"           : "Door de GED GEBRUIKERSNAAM van het document in te geven, wijzigt u de status van de brief. Deze brief zal beschikbaar zijn in het gebruikersbakje waaraan hij toegewezen was volgens de status die u gedefinieerd hebt.",
    "updateVersionControl"                  : "Controle van de bijwerking",
    "uploadSignFile"                        : "Een afbeelding op dit kader plaatsen of klikken om uw bestanden te doorlopen (JPG PNG JPEG)",
    "uploadSignFileInfo"                    : "Klikken om een handtekening toe te voegen vanuit uw post",
    "used"                                  : "Gebruikt",
    "usedInActionPage"                      : "Actie op de actiepagina",
    "usedInBasketlist"                      : "Actie op de resultatenlijst",
    "user"                                  : "gebruiker",
    "userAdded"                             : "Toegevoegde gebruiker",
    "userAuthorized"                        : "Toegelaten gebruiker",
    "userCreation"                          : "Aanmaak van een gebruiker",
    "userDeleted"                           : "Verwijderde gebruiker",
    "userIsDeletableBy"                     : "Only a user with the <b>totality of access rights</b> can delete or suspend this user.", //_TO_TRANSLATE
    "userIsNotDeletable"                    : "You do not have <b>full access rights</b> to diffusion list or mails for the user ", //_TO_TRANSLATE
    "userMaarchParapheur"                   : "Maarch Parapheur user", //_TO_TRANSLATE
    "userModification"                      : "Wijziging van de gebruiker",
    "userReplacement"                       : "Vervangingsgebruiker",
    "users"                                 : "Gebruikers",
    "adminUsersDesc"                        : "Gebruikersprofielen toevoegen, opschorten of wijzigen.",
    "usersAlt"                              : "Gebruiker(s)",
    "userSuspended"                         : "Geschorste gebruiker",
    "userToRedirect"                        : "Hier klikken om door te sturen",
    "userUpdated"                           : "Gewijzigde gebruiker",
    "validate"                              : "Valideren",
    "validateAction"                        : "Kwalificatieactie(s)",
    "value"                                 : "waarde",
    "version"                               : "Version", //_TO_TRANSLATE
    "view"                                  : "Raadplegen",
    "visa"                                  : "Goedkeurings",
    "visaUser"                              : "Goedkeurder",
    "visaWorkflow"                          : "Goedkeuringscircuit",
    "warnLockRes"                           : "mail(s) are locked by", //_TO_TRANSLATE
    "warnLockRes2"                          : "\n\nThis action will process ONLY mails which are NOT locked", //_TO_TRANSLATE
    "warnLockResInProgress"                 : "This mail is in progress by", //_TO_TRANSLATE
    "warnMaxDataList"                       : "Maximum of displayed datas is reached", //_TO_TRANSLATE
    "warnShapingOption"                     : "<b>Be carefull !</b> The address will be setted in the first page of sent documents", //_TO_TRANSLATE
    "warnTooMuchSelect"                     : "only 500 elements can be processed with an action", //_TO_TRANSLATE
    "webserviceAccount"                     : "Webservice-account",
    "webserviceAccountDesc"                 : "Indien ingeschakeld zal deze account niet gebruikt kunnen worden om zich grafisch met de applicatie te verbinden.",
    "wednesday"                             : "Woensdag",
    "welcomeInGEC"                          : "Welkom bij uw <b>E</b>lektronisch <b>E</b>-mail<b>b</b>eheer,<br/><br/>we zijn",
    "whereClauseAction"                     : "Voorwaarde voor optreden van de actie (where clause)",
    "willBeAutomaticallyInCopy"             : "wordt automatisch <b>gekopieerd</b>", //_TO_TRANSLATE
    "workingDays"                           : "werkdag(en)",
    "yes"                                   : "Ja",
    "zipcode"                               : "Postcode",
    "printSeparatorInfo" : "You can print separators <b>by entity</b> or <b>generic</b> in order to use in <b>maarchCapture</b> to record scanned mails in right entity.", //_TO_TRANSLATE
    "generic" : "Generic", //_TO_TRANSLATE
    "qrcode" : "QR code", //_TO_TRANSLATE
    "separatorType" : "Separator type", //_TO_TRANSLATE
    "separatorTarget" : "Separator target", //_TO_TRANSLATE
    "generateAndDownload" : "Generate and download", //_TO_TRANSLATE
    "separators" : "Separator(s)", //_TO_TRANSLATE
    "entityTreeInfoSeparator" : "Click on entity tree in order to generate the separator", //_TO_TRANSLATE
    "userOK" : "Active", //_TO_TRANSLATE
    "userSPD" : "Inactive", //_TO_TRANSLATE
    "userABS" : "Absent", //_TO_TRANSLATE
    "noVisaWorkflow" : "No visa workflow set up.", //_TO_TRANSLATE
    "visaWorkflowWillBeSend" : "Visa workflow will be send to Maarch Parapheur", //_TO_TRANSLATE
    "usersNotExistedInMaarchParapheur" : "Some users does not exist in Maarch Parapheur", //_TO_TRANSLATE
    "usersMissingInSignatureBook" : "missing in Maarch Parapheur", //_TO_TRANSLATE
    "unlinkAccount" : "Unlink account", //_TO_TRANSLATE
    "accountLinked" : "Account linked", //_TO_TRANSLATE
    "accountUnlinked" : "Account unlinked", //_TO_TRANSLATE
    "maarchParapheurAccountMsg" : "A <b>new</b> account with login", //_TO_TRANSLATE
    "maarchParapheurAccountMsg2" : " will be created in Maarch Parapheur.", //_TO_TRANSLATE
    "searchUserInMaarchParapheur" : "Search unlinked an user in Maarch Parapheur", //_TO_TRANSLATE
    "newLoginInMaarchParapheur" : "New Maarch Parapheur Login", //_TO_TRANSLATE
    "loginAlreadyExistsInMaarchParapheur" : "Login already exists in Maarch Parapheur", //_TO_TRANSLATE
    "maarchParapheurLinkbroken" : "Maarch Parapheur link is broken", //_TO_TRANSLATE
    "maarchParapheurDocuments" : "Maarch Parapheur document(s)", //_TO_TRANSLATE
    "to_visa" : "To approve", //_TO_TRANSLATE
    "to_sign" : "To sign", //_TO_TRANSLATE
    "to_note" : "To note", //_TO_TRANSLATE
    "confidential" : "Confidential", //_TO_TRANSLATE
    "ok" : "Ok", //_TO_TRANSLATE
    "updateOk" : "Update successfull !", //_TO_TRANSLATE
    "updateInProgress" : "Update in progress ...", //_TO_TRANSLATE
    "updateInfo" : "This action cannot be undone and will update all instances of the application! Do not make any actions during process.", //_TO_TRANSLATE
    "isAvailable" : "is available", //_TO_TRANSLATE
    "updateWarn" : "Update is not possible.<br/>These files are modified : <br/>", //_TO_TRANSLATE
    "chooseValue" : "Choose a value", //_TO_TRANSLATE
    "availableValues" : "Available values", //_TO_TRANSLATE
    "noAvailableValue" : "No available value", //_TO_TRANSLATE
    "autocompleteInfo" : "The criteria must contain at least&nbsp;<b>3 letters</b>", //_TO_TRANSLATE
    "availableUsers" : "Available users", //_TO_TRANSLATE
    "userUnauthorized" : "User Unauthorized", //_TO_TRANSLATE
    "warnIndex" : "This group can not index document.", //_TO_TRANSLATE
    "authorizedEntities" : "Authorized entities destination", //_TO_TRANSLATE
    "enableIndex" : "Activate mail indexing", //_TO_TRANSLATE
    "disableIndex" : "Disable mail indexing", //_TO_TRANSLATE
    "indexEnabled" : "Mail indexation enabled", //_TO_TRANSLATE
    "indexDisabled" : "Mail indexation disabled", //_TO_TRANSLATE
    "indexation" : "Mail indexation", //_TO_TRANSLATE
    "keywordAdded" : "Keyword added", //_TO_TRANSLATE
    "keywordDeleted" : "Keyword deleted", //_TO_TRANSLATE
    "availableIndexingActions" : "Available actions when indexing",//_TO_TRANSLATE
    "actionsInfo" : "Only actions with \"simple confirmation\", \"close mail\", \"no confirmation\" and \"send to visa\" will be displayed", //_TO_TRANSLATE
    "eventList" : "Open page", //_TO_TRANSLATE
    "folderAdded" : "Folder added", //_TO_TRANSLATE
    "folderDeleted" : "Folder deleted", //_TO_TRANSLATE
    "removedFromFolder" : "Mail removed from folder", //_TO_TRANSLATE
    "removeFromFolder" : "Remove from folder", //_TO_TRANSLATE
    "classifyInFolder" : "Classify this mail in a folder", //_TO_TRANSLATE
    "getFolders" : "Folders (fixed postion)", //_TO_TRANSLATE
    "getFoldersSample" : "Litigation", //_TO_TRANSLATE
    "classifyIn" : "Classify in ...", //_TO_TRANSLATE
    "setInParentFolder" : "Set in parent folder ...", //_TO_TRANSLATE
    "searchFolder" : "Search folder", //_TO_TRANSLATE
    "shareToEntities" : "Share to entities ...", //_TO_TRANSLATE
    "canManageFolder" : "Can manage this folder", //_TO_TRANSLATE
    "searchMailInFolder" : "Search mails in folder", //_TO_TRANSLATE
    "addSubFolder" : "Add subfolder", //_TO_TRANSLATE
    "sharedFolder" : "Shared folder", //_TO_TRANSLATE
    "addFolder" : "Add folder", //_TO_TRANSLATE
    "more" : "More", //_TO_TRANSLATE
    "less" : "Less", //_TO_TRANSLATE
    "actionsAlt": "Actions", //_TO_TRANSLATE
    "addRootFolder": "Add root folder", //_TO_TRANSLATE
    "folderUpdated" : "Folder updated", //_TO_TRANSLATE
    "in" : "in", //_TO_TRANSLATE
    "classifyQuestion" : "Do you want to classify", //_TO_TRANSLATE
    "moveQuestion" : "Do you want to move", //_TO_TRANSLATE
    "classify" : "Classify", //_TO_TRANSLATE
    "mailClassified" : "Mail(s) classified", //_TO_TRANSLATE
    "noAvailableFolder" : "No available folder", //_TO_TRANSLATE
    "unclassifyQuestion" : "Do you want to unclassify", //_TO_TRANSLATE
    "mailsInFolder" : "mail(s) in folder", //_TO_TRANSLATE
    "customFields" : "custom field(s)", //_TO_TRANSLATE
    "avaiblableCustomFieldsTypes" : "Available(s) custom type(s)", //_TO_TRANSLATE
    "customField" : "custom field", //_TO_TRANSLATE
    "selectInput" : "Select input", //_TO_TRANSLATE
    "dateInput" : "Date input", //_TO_TRANSLATE
    "radioInput" : "Radio input", //_TO_TRANSLATE
    "checkboxInput" : "Checbox input", //_TO_TRANSLATE
    "customFieldAlreadyExists" : "Custom fields already exists with this name", //_TO_TRANSLATE
    "newField" : "New field", //_TO_TRANSLATE
    "newValue" : "New value", //_TO_TRANSLATE
    "customFieldAdded" : "Custom field added", //_TO_TRANSLATE
    "customFieldUpdated" : "Custom field updated", //_TO_TRANSLATE
    "customFieldDeleted" : "Custom field deleted", //_TO_TRANSLATE
    "usableFields" : "Usable field(s)", //_TO_TRANSLATE
    "noCustomFieldInfo" : "<b>No custom field</b> available, choose avaiblable custom fields types on <b>right panel</b> in order to create new custom fields.", //_TO_TRANSLATE
    "defaultValue" : "Default value", //_TO_TRANSLATE
    "valuesList" : "List of values", //_TO_TRANSLATE
    "addValue" : "Add a value", //_TO_TRANSLATE
    "orderValues" : "Order values", //_TO_TRANSLATE
    "customFieldsTypesAvailable" : "Custom fields types available", //_TO_TRANSLATE
    "typeValue" : "Type a value", //_TO_TRANSLATE
    "chooseDate" : "Choose a date", //_TO_TRANSLATE
    "selectedValue" : "Selected value", //_TO_TRANSLATE
    "choice" : "Choice", //_TO_TRANSLATE
    "owner" : "Owner", //_TO_TRANSLATE
    "privateFolder" : "Private folder", //_TO_TRANSLATE
    "visibleBy" : "Visible by", //_TO_TRANSLATE
    "noteVisibleBy" : "Note visible by :", //TRANSLATE
    "folderInformations" : "Folder Informations", //_TO_TRANSLATE
    "process" : "Process", //_TO_TRANSLATE
    "modelName" : "Model name", //_TO_TRANSLATE
    "defaultModel" : "Default model", //_TO_TRANSLATE
    "indexingForm" : "Indexing form", //_TO_TRANSLATE
    "availableFields" : "Available field(s)", //_TO_TRANSLATE
    "availableCustomFields" : "Available custom field(s)", //_TO_TRANSLATE
    "indexingModel" : "Indexing model", //_TO_TRANSLATE
    "indexingModelModification" : "Indexing model modification", //_TO_TRANSLATE
    "mandatoryField" : "Mandatory field", //_TO_TRANSLATE
    "optionalField" : "Optional field", //_TO_TRANSLATE
    "indexingModels" : "Indexing models", //_TO_TRANSLATE
    "perimeter" : "Perimeter", //_TO_TRANSLATE
    "indexingModelDeleted" : "Indexing model deleted", //_TO_TRANSLATE
    "indexingModelAdded" : "Indexing model added", //_TO_TRANSLATE
    "indexingModelUpdated" : "Indexing model updated", //_TO_TRANSLATE
    "indexingModelCreation" : "Indexing model creation", //_TO_TRANSLATE
    "integerInput" : "Input number", //_TO_TRANSLATE
    "noSelectedValue" : "No selected value", //_TO_TRANSLATE 
    "searchValue" : "Search a value", //_TO_TRANSLATE 
    "initiatorEntityAlt" : "Initiator entity", //_TO_TRANSLATE 
    "autocompleteInput" : "Autocomplete input", //_TO_TRANSLATE
    "typeEnterToCreate" : "Type on <b>enter</b> to create element", //_TO_TRANSLATE
    "ged_doc" : "GED Document", //_TO_TRANSLATE
    "trackThisMail" : "Track this mail", //_TO_TRANSLATE
    "untrackThisMail" : "Untrack this mail", //_TO_TRANSLATE
    "noFieldInModelMsg" : "No field found ! Default fields have been loaded", //_TO_TRANSLATE
    "fieldNotExist" : "Field not exist", //_TO_TRANSLATE
    "saveAsModel" : "Save as Model", //_TO_TRANSLATE
    "noAvailableIndexingModel" : "No indexing model found<br /><br /><small>Please create a model in Administation > Indexing model</small>", //_TO_TRANSLATE
    "noAvailableAction" : "<b>No available action</b>, please set an action in <b>Administration > User groups > Mail indexing</b>.", //_TO_TRANSLATE
    "noAvailableActionProcess" : "<b>No available action</b>, please set an action in <b>Administration > Baskets > Current basket</b>.", //_TO_TRANSLATE
    "eraseValue" : "Erase value", //_TO_TRANSLATE
    "todayDate" : "Today date", //_TO_TRANSLATE
    "noDefaultIndexingModel" : "No default indexing model found, the first model of the list have been loaded.", //_TO_TRANSLATE
    "indexing_incoming" : "incoming", //_TO_TRANSLATE
    "indexing_outgoing" : "outgoing", //_TO_TRANSLATE
    "indexing_internal" : "internal", //_TO_TRANSLATE
    "indexing_ged_doc" : "GED", //_TO_TRANSLATE
    "noSelectedFolder" : "No selected folder", //_TO_TRANSLATE
    "noSelectedTag" : "No selected tag", //_TO_TRANSLATE
    "searchTag" : "Search tag", //_TO_TRANSLATE
    "tagAdded" : "Tag added", //_TO_TRANSLATE
    "tagDeleted" : "Tag deleted", //_TO_TRANSLATE
    "modifyDiffusionList" : "Modify diffusion list", //_TO_TRANSLATE
    "notInAuthorizedEntities" : "is not in one of the <b>authorized entities</b>.", //_TO_TRANSLATE
    "indexingModelDisabled" : "Indexing model disabled", //_TO_TRANSLATE
    "indexingModelEnabled" : "Indexing model enabled", //_TO_TRANSLATE
    "canNotDeleteIndexingModel" : "This indexing model can not be deleted because at least one mail used it", //_TO_TRANSLATE
    "loadingFile" : "Loading file", //_TO_TRANSLATE
    "convertingFile" : "Convert file in progress", //_TO_TRANSLATE
    "notAllowedExtension" : "Extension not allowed", //_TO_TRANSLATE
    "allowedExtensions" : "Allowed extensions", //_TO_TRANSLATE
    "maxFileSizeReached" : "Maximum file size reached", //_TO_TRANSLATE
    "maxFileSize" : "Maximum file size", //_TO_TRANSLATE
    "uploadAnOtherFile" : "Upload an other file", //_TO_TRANSLATE
    "removeFile" : "Remove file", //_TO_TRANSLATE
    "loaded" : "loaded", //_TO_TRANSLATE
    "noAvailablePreview" : "No available preview", //_TO_TRANSLATE
    "file" : "File", //_TO_TRANSLATE
    "DownloadConvertedFile" : "Download converted file", //_TO_TRANSLATE
    "linkNewAction" : "Link new action", //_TO_TRANSLATE
    "addPrivateIndexingModelMsg" : "This model will be <b>private</b> and will keep as model", //_TO_TRANSLATE
    "addPrivateIndexingModelMsg2" : "with <b>actual</b> values.", //_TO_TRANSLATE
    "privateIndexingModelCreation" : "Private indexing model creation", //_TO_TRANSLATE
    "createPrivateIndexingModel" : "Create an indexing model with current datas", //_TO_TRANSLATE
    "currentIndexingMail" : "the current indexing mail", //_TO_TRANSLATE
    "mustFixErrors" : "You must fix the errors.", //_TO_TRANSLATE
    "badActionParam" : "Bad action parameter", //_TO_TRANSLATE
    "technicalId"    : "Technical identifier", //_TO_TRANSLATE
    "foldersTree" : "Folders tree", //_TO_TRANSLATE
    "myPinnedFolder" : "My pinned folders", //_TO_TRANSLATE
    "noPinnedFolder" : "No pinned folder", //_TO_TRANSLATE
    "pinFolder" : "Pin folder", //_TO_TRANSLATE
    "unpinFolder" : "Unpin folder", //_TO_TRANSLATE
    "showFoldersTree" : "Show tree", //_TO_TRANSLATE
    "hideFoldersTree" : "Hide tree", //_TO_TRANSLATE
    "folderPinned" : "Folder pinned", //_TO_TRANSLATE  
    "folderUnpinned" : "Folder unpinned", //_TO_TRANSLATE
    "entitiesSeparator" : "Print limiters by department/services",//_TO_TRANSLATE
    "backHome" : "Back to home",  //_TO_TRANSLATE
    "viewDocHistory" : "De geschiedenis van de acties op het brief bekijken",
    "viewHistoryDesc" : "De geschiedenis van de gebeurtenissen met betrekking tot het gebruik van Maarch Courrier raadplegen.",
    "viewFullHistory" : "De volledige geschiedenis van het document bekijken",
    "viewFullHistoryDesc" : "De volledige geschiedenis van de evenementen over het gebruik van GED Maarch raadplegen.",
    "editResource" : "Een document in de gedetailleerde",
    "editResourceDesc" : "Een document in de gedetailleerde. Dat zal ook van de instelling van de status afhangen (Wijziging van de indexen)",
    "addLinks" : "Add/remove links", //_TO_TRANSLATE
    "deleteDocumentInDetail" : "Een document in de gedetailleerde fiche verwijderen",
    "manageTagsInApplication" : "Create tags", //_TO_TRANSLATE
    "manageTagsInApplicationDesc" : "Create tags while indexing, processing and in details", //_TO_TRANSLATE
    "updateDiffusionWhileIndexing" : "Update all parts of diffusion list when indexing.", //_TO_TRANSLATE
    "updateDiffusionExceptRecipientWhileIndexing" : "Update diffusion list except Assignee when indexing.", //_TO_TRANSLATE
    "updateDiffusionWhileProcess" : "Update all parts of diffusion list when processing.", //_TO_TRANSLATE
    "updateDiffusionExceptRecipientWhileProcess" : "Update diffusion list except Assignee when processing.", //_TO_TRANSLATE
    "updateDiffusionWhileDetails" : "Update all parts of diffusion list when in details.", //_TO_TRANSLATE
    "updateDiffusionExceptRecipientWhileDetails" : "Update diffusion list except Assignee when in details.", //_TO_TRANSLATE
    "useMailServices" : "De e-mails van zijn diensten gebruiken als verzender",
    "viewDocumentsWithNotes": "View mail with notes", //_TO_TRANSLATE
    "viewDocumentsWithNotesDesc": "View attachment of 'document_with_notes' type", //_TO_TRANSLATE
    "viewTechnicalInformation" : "De technische informatie bekijken",
    "configAvisWorkflow" : "Configuratie van het berichtencircuit",
    "configAvisWorkflowDesc" : "Om het berichtencircuit van de brief in te stellen",
    "configAvisWorkflowInDetail" : "Configuratie van het berichtencircuit vanuit de gedetailleerde fiche",
    "configAvisWorkflowInDetailDesc" : "Om het circuit rechtstreeks vanuit de gedetailleerde fiche van de brief te bepalen",
    "avisAnswer" : "Zijn mening geven",
    "avisAnswerDesc" : "Laat deze groep verschijnen in de lijst van de Consultants (berichtencircuit)",
    "configVisaWorkflow" : "Configuratie van het goedkeuringscircuit",
    "configVisaWorkflowDesc" : "Om het goedkeuringscircuit in te stellen dat de brief zal moeten volgen",
    "configVisaWorkflowInDetail" : "Configuratie van het goedkeuringscircuit vanuit de gedetailleerde fiche",
    "configVisaWorkflowInDetailDesc" : "Om het goedkeuringscircuit vanuit de gedetailleerde fiche te configureren",
    "noFile" : "No file", //_TO_TRANSLATE
    "noFileMsg" : "<b>Be careful</b>, no file uploaded.<br/><br/>Continue ?", //_TO_TRANSLATE
    "visaAnswers" : "De antwoordontwerpen goedkeuren",
    "visaAnswersDesc" : "Om de antwoordontwerpen te kunnen goedkeuren",
    "signDocs": "De documenten ondertekenen",
    "modifyVisaInSignatureBook" : "De huidige goedkeurder wijzigen vanuit het vloeiboek",
    "modifyVisaInSignatureBookDesc" : "Nuttig indien het vloeiboek als controlevloeiboek dient",
    "printFolderDoc" : "De volledige map afdrukken",
    "statusesAdmin" : "Statussen aanmaken of wijzigen",
    "actionsAdmin" : "Acties aanmaken of wijzigen",
    "contactsAdmin" : "Contactenbeheer",
    "templatesAdmin" : "De modellen die voor de bijlagen gebruikt worden, de meldingen, de aanmaak van brieven, het verzenden van e-mails en de opmerkingen beheren",
    "customFieldsAdmin" : "Custom fields", //_TO_TRANSLATE
    "notificationsAdmin" : "Meldingen voor de gebruikers aanmaken en beheren die gebaseerd zijn op gebeurtenissen van de applicatie",
    "docserversAdmin" : "Opslagruimtes toevoegen opschorten of wijzigen. De opslagruimtes per type plaatsen waartoe ze behoren en hun primaire groep bepalen",
    "securities" : "Veiligheden",
    "emailServerParam" : "Mail server", //_TO_TRANSLATE
    "emailServerParamDesc" : "Link your mail server with application in order to send emails", //_TO_TRANSLATE
    "mailevaAdmin" : "Maileva shipping admin", //_TO_TRANSLATE
    "mailevaAdminDesc" : "Configure mailing templates for Maileva", //_TO_TRANSLATE
    "infoMailevaAccount": "Indicate here your username and the INITIAL password communicated by Maileva when opening your account", //_TO_TRANSLATE
    "reportsAdmin" : "Administratie van de statistieken",
    "historyBatchAdmin" : "De geschiedenis van de batches raadplegen",
    "updateControl" : "Controle van de bijwerking",
    "mailsSent" : "Resource(s) sent", //_TO_TRANSLATE
    "dashboard" : "Dashboard", //_TO_TRANSLATE
    "noSendmail" : "No mail sent", //_TO_TRANSLATE
    "noEvent" : "No event", //_TO_TRANSLATE
    "hours" : "hours", //_TO_TRANSLATE
    "editInProgress" : "Edit in progress", //_TO_TRANSLATE
    "chooseFile" : "Choose a file", //_TO_TRANSLATE
    "or" : "or", //_TO_TRANSLATE
    "dragAndDrop" : "Drag and drop", //_TO_TRANSLATE
    "backBasket" : "Back to basket", //_TO_TRANSLATE
    "openInExternalModal" : "Open in external modal", //_TO_TRANSLATE
    "openedInExternalModal" : "Opened in external modal", //_TO_TRANSLATE
    "showMore" : "Show more", //_TO_TRANSLATE
    "notesAlt" : "Notes", //_TO_TRANSLATE
    "mailsSentAlt" : "Resources sent", //_TO_TRANSLATE
    "newsFeed" : "News feed", //_TO_TRANSLATE
    "sendError" : "Send error", //_TO_TRANSLATE
    "historyDisabled" : "History disabled", //_TO_TRANSLATE
    "ARPaper" : "Paper acknowledgement of receipt", //_TO_TRANSLATE
    "ARelectronic" : "Electronic acknowledgement of receipt", //_TO_TRANSLATE
    "allRoles" : "All roles", //_TO_TRANSLATE
    "rolesExceptAssignee" : "Role(s) without assignee", //_TO_TRANSLATE
    "noRole" : "No role", //_TO_TRANSLATE
    "diffListPrivilegeMsgIndexing" : "In indexing page, modify user in roles: ", //_TO_TRANSLATE
    "diffListPrivilegeMsgProcess" : "In process page, modify user in roles: ", //_TO_TRANSLATE
    "diffListPrivilegeMsgDetails" : "In details page, modify user in roles: ",//TRANSLATE
    "recordMail" : "Record a mail", //_TO_TRANSLATE
    "closed" : "Closed", //_TO_TRANSLATE
    "manageAttachments": "Update or delete attachments", //_TO_TRANSLATE
    "viewPersonalData": "Viewable", //_TO_TRANSLATE
    "managePersonalData": "Viewable and updatable", //_TO_TRANSLATE
    "attachmentDeleted": "Attachment deleted", //_TO_TRANSLATE
    "attachment_SEND_MASS": "Send to mailing", //_TO_TRANSLATE
    "attachment_A_TRA": "To process", //_TO_TRANSLATE
    "attachment_SIGN": "Signed", //_TO_TRANSLATE
    "attachment_FRZ": "Freez", //_TO_TRANSLATE
    "attachment_TRA": "Processed", //_TO_TRANSLATE
    "afterClickingSendLinkChangePassword": "After clicking on <b>Send</b>, you will receive an email containing a link to change your password. This link will be active <b>1 hour</b>.", //_TO_TRANSLATE
    "enterLogin": "Enter your username", //_TO_TRANSLATE
    "send" : "Send", //_TO_TRANSLATE
    "generation" : "Generation...", //_TO_TRANSLATE
    "requestSentByEmail" : "The request has been sent to you by email.", //_TO_TRANSLATE
    "logInOncePasswordChanged": "You will be asked to log in once the password has been changed.", //_TO_TRANSLATE
    "createNewVersion" : "Create a new version", //_TO_TRANSLATE
    "deleteSignedVersion" : "Delete signed version", //_TO_TRANSLATE
    "signedAttachment" : "Signed attachment", //_TO_TRANSLATE
    "attachmentAdded" : "Attachment added", //_TO_TRANSLATE
    "attachmentUpdated" : "Attachment updated", //_TO_TRANSLATE
    "signedVersionDeleted" : "Signed version deleted", //_TO_TRANSLATE
    "newVersionAdded" : "New version added", //_TO_TRANSLATE
    "editionAttachmentConfirmFirst" : "All merge variables will be replaced taking into account what you entered in the form on the left.", //_TO_TRANSLATE
    "editionAttachmentConfirmSecond" : "Only <b> chrono number </b> ([res_letterbox.alt_identifier]) will be replaced after mail registration", //_TO_TRANSLATE
    "editionAttachmentConfirmThird" : "Only <b> chrono number </b> ([res_letterbox.alt_identifier]) and <b>barcode</b> will be replaced after mail registration",//TRANSLATE
    "othersInfos" : "Other(s) information(s)", //_TO_TRANSLATE
    "contactDetails" : "Details", //_TO_TRANSLATE
    "contactsFillingRate" : "Filling rate", //_TO_TRANSLATE
    "contact_physical" : "Physical contact", //_TO_TRANSLATE
    "contact_corporate" : "Corporate contact", //_TO_TRANSLATE
    "contact_internal" : "Internal user", //_TO_TRANSLATE
    "goToAdminParams" : "Access to admin settings", //_TO_TRANSLATE
    "adminParamsUsers" : "Settings for users administration", //_TO_TRANSLATE
    "chooseAllowedGroups" : "Choose allowed groups", //_TO_TRANSLATE
    "reallyWantToDeleteThisGroup"   : "Do you really want to remove this group ?", //_TO_TRANSLATE
    "contact_entity"   : "Entity", //_TO_TRANSLATE
    "contact_contact": "Contact", //_TO_TRANSLATE
    "contact_contactGroup": "Contacts group", //_TO_TRANSLATE
    "contact_user": "User", //_TO_TRANSLATE
    "contact_third"   : "Contact third", //_TO_TRANSLATE
    "confidentialityAndSecurity"   : "Confidentiality and security", //_TO_TRANSLATE
    "modified"   : "Modified", //_TO_TRANSLATE
    "created"   : "Created", //_TO_TRANSLATE
    "createdAlt": "Created", //_TO_TRANSLATE
    "signedAlt"   : "Signed", //_TO_TRANSLATE
    "deleteResource"   : "Delete document", //_TO_TRANSLATE
    "viewResource"   : "View document", //_TO_TRANSLATE
    "noDocument": "No document", //_TO_TRANSLATE
    "addAttachment"   : "Add an attachment", //_TO_TRANSLATE
    "dateAgo"   : "Since", //_TO_TRANSLATE
    "dateTo"   : "The", //_TO_TRANSLATE
    "attachmentCreation": "Attachment creation", //_TO_TRANSLATE
    "newAttachment": "New attachment", //_TO_TRANSLATE
    "delAttachment": "Delete this attachment", //_TO_TRANSLATE
    "mustCompleteAllAttachments": "Please, complete all attachments", //_TO_TRANSLATE
    "editDocument": "Edit document", //_TO_TRANSLATE
    "personalDataMsg": "Users personal data are", //_TO_TRANSLATE
    "notVisible": "Not visible", //_TO_TRANSLATE
    "enableGroupMsg": "This group could access to all functionnalities.", //_TO_TRANSLATE
    "sendActivationNotification": "Send activation notification", //_TO_TRANSLATE
    "activationNotificationSend": "Activation notification send", //_TO_TRANSLATE
    "tabProcessPosition": "Move to tab", //_TO_TRANSLATE
    "saveModifiedData": "Do you want to save modified data ?", //_TO_TRANSLATE
    "dataUpdated": "Data updated", //_TO_TRANSLATE
    "canUpdateData": "Can update resource data", //_TO_TRANSLATE
    "manageCreateContacts": "Create contacts", //_TO_TRANSLATE
    "manageCreateContactsDesc": "Create contacts from indexing/processing/detail page", //_TO_TRANSLATE
    "manageUpdateContacts": "Update contacts", //_TO_TRANSLATE
    "manageUpdateContactsDesc": "Update contacts from indexing/processing/detail page", //_TO_TRANSLATE
    "newVersion": "New version", //_TO_TRANSLATE
    "information": "Information", //_TO_TRANSLATE
    "mustEditDocument": "You must <b>edit</b> document in order to <b>validate</b> modifications.", //_TO_TRANSLATE
    "autocompletionSearchable": "Recherchable dans l'autocomplétion", //_TO_TRANSLATE
    "autocompletionDisplayable": "Affichable dans l'autocompletion", //_TO_TRANSLATE
    "contactsParameters": "Paramétrage des contacts", //_TO_TRANSLATE
    "siretGenerated": "SIRET number generated",//_TO_TRANSLATE
    "siretGeneratedAndSynchronizationDone": "SIRET number generated and annuary synchronization done",//_TO_TRANSLATE
    "siretGeneratedButAnnuaryUnreachable": "SIRET number generated but annuary is unreachable",//_TO_TRANSLATE
    "entityDeletedButAnnuaryUnreachable": "Entity deleted but annuary is unreachable",//_TO_TRANSLATE
    "langISO": "nl-FR",
    "language": "nl",
    "noSelectedContact": "No associated contact", //_TO_TRANSLATE
    "searchContact": "Search a contact", //_TO_TRANSLATE
    "editorOption": "Editor option", //_TO_TRANSLATE
    "editorOptionAdmin": "Manage editor option", //_TO_TRANSLATE
    "documentEditor": "Document editor", //_TO_TRANSLATE
    "canNotEditImportedDocumentWhenJava":          "You can not edit imported document with Java applet", //translate
    "documentEditor_java": "Open your defaut document editor in a new windows", //_TO_TRANSLATE
    "documentEditor_onlyoffice": "Open OnlyOffice inside Maarch Courrier application", //_TO_TRANSLATE
    "contactsList": "Contacts list", //_TO_TRANSLATE
    "listConfiguration": "Manage list display", //_TO_TRANSLATE
    "noticeGroupeOrder": "Drag and drop group to set order", //_TO_TRANSLATE
    "searchAddressBan": "Search a BAN address", //_TO_TRANSLATE
    "searchAddressDb": "Search in address database", //_TO_TRANSLATE
    "switchManualAddress": "Switch to manual address", //_TO_TRANSLATE
    "targetDepartment": "Target department", //_TO_TRANSLATE
    "contactCreation": "Contact creation", //_TO_TRANSLATE
    "contactModification": "Contact modification", //_TO_TRANSLATE
    "contactEnabled": "Contact enabled", //_TO_TRANSLATE
    "contactDisabled": "Contact disabled", //_TO_TRANSLATE
    "contactUpdated": "Contact updated", //_TO_TRANSLATE
    "addAll": "Add all", //_TO_TRANSLATE
    "additionals": "Additional(s)", //_TO_TRANSLATE
    "denomination": "Denomination", //_TO_TRANSLATE
    "moreInfos" : "More data", //_TO_TRANSLATE
    "confirmCloseEditor" : "Do you really want to close editor ? All modifications will be canceled", //_TO_TRANSLATE
    "onlyNumber" : "Number only", //_TO_TRANSLATE
    "found": "found", //_TO_TRANSLATE
    "here": "here", //_TO_TRANSLATE
    "toCopyAddress": "to copy the address", //_TO_TRANSLATE
    "badFormat": "Bad format", //_TO_TRANSLATE
    "contactFilledTo": "Contact filled to", //_TO_TRANSLATE
    "targetFillingField": "Target field for filling", //_TO_TRANSLATE
    "arGenWithModelMessage" : "The summary sheets with the fields defined in the resource's indexing model.", //_TO_TRANSLATE
    "createContact": "Create a contact", //_TO_TRANSLATE
    "diffusionListUpdated": "Diffusion list updated", //_TO_TRANSLATE
    "saveModifications": "Save modifications", //_TO_TRANSLATE
    "communicationMean": "Communication mean", //_TO_TRANSLATE
    "communicationMeanDesc": "Link a contact with another Maarch instance", //_TO_TRANSLATE
    "see": "see", //_TO_TRANSLATE
    "notSavedBecauseInvalid": "Configuration not updated because some data are invalid", //_TO_TRANSLATE
    "deleteAll": "Delete all", //_TO_TRANSLATE
    "followedMail": "Followed mail(s)", //_TO_TRANSLATE
    "stopFollowingAlert": "Do you want to stop following these mails ?", //_TO_TRANSLATE
    "searchTrackedMail": "Search in tracked mails", //_TO_TRANSLATE
    "checkOnlyofficeServer": "Establish contact with the Onlyoffice server", //_TO_TRANSLATE
    "closeEditor" : "Close the editor", //_TO_TRANSLATE
    "errorOnlyoffice1" : "Cannot launch onlyoffice, a local address is used", //_TO_TRANSLATE
    "errorOnlyoffice2" : "Cannot launch onlyoffice. Check if the only office server is available", //_TO_TRANSLATE
    "onlyOfficeNotEnabled" : "The onlyoffice server is not enabled", //_TO_TRANSLATE
    "uriIsEmpty" : "URI of onlyoffice server is empty in documentEditorsConfig.xml", //_TO_TRANSLATE
    "portIsEmpty" : "Port of onlyoffice server is empty in documentEditorsConfig.xml", //_TO_TRANSLATE
    "externalVisaWorkflow" : "Visa workflow Maarch Parapheur", //_TO_TRANSLATE
    "IdMaarch2Maarch" : "MAARCH2MAARCH identifier", //_TO_TRANSLATE
    "indexingFile" : "Indexation", //_TO_TRANSLATE
    "signUserRequired" : "A signatory is required", //_TO_TRANSLATE
    "signedUserDate" : "Signed", //_TO_TRANSLATE
    "approvedUserDate" : "Approuved", //_TO_TRANSLATE
    "modelDeleted" : "Model deleted", //_TO_TRANSLATE
    "modelSaved" : "Model saved", //_TO_TRANSLATE
    "visaWorkflowUpdated" : "Visa workflow updated", //_TO_TRANSLATE
    "visaWorkflowDeleted" : "Visa workflow deleted", //_TO_TRANSLATE
    "publicModel" : "Public model", //_TO_TRANSLATE
    "privateModel" : "Private model", //_TO_TRANSLATE
    "moveVisaUserErr" : "You cannot move <p>{0}</p> with users who have already approved / signed or sign / approval is in progress.", //_TO_TRANSLATE
    "moveAvisUserErr1" : "You cannot move", //_TO_TRANSLATE
    "moveAvisUserErr2" : "with users who have already given an opinion", //_TO_TRANSLATE
    "avisWorkflowUpdated" : "Opinion workflow updated", //_TO_TRANSLATE
    "avisWorkflowDeleted" : "Opinion workflow deleted", //_TO_TRANSLATE
    "avisSent" : "Opinion given", //_TO_TRANSLATE
    "openFullscreen" : "Enable fullscreen", //_TO_TRANSLATE
    "closeFullscreen" : "Disable fullscreen", //_TO_TRANSLATE
    "hasNoEntity": "Those mails do not have initiator entity", //_TO_TRANSLATE
    "destUserSetToDefault": "Assignee will be the assignee of the initiator entity template list", //_TO_TRANSLATE
    "resetVisaWorkflow": "The workflow actions will be <b>reset</b>", //_TO_TRANSLATE
    "interruptVisaWorkflow": "The following users will not be able to target mail", //_TO_TRANSLATE
    "rejectVisaBack": "Document will be send to the previous user", //_TO_TRANSLATE
    "rejectVisaBackUser": "Document will be send back to", //_TO_TRANSLATE
    "sendToDocTo": "You will send this document to", //_TO_TRANSLATE
    "endWorkflow": "You will end this workflow", //_TO_TRANSLATE
    "hasNoAttachmentsNotes": "The following letters have no annotation or attachments", //_TO_TRANSLATE
    "closeMailAddNoteMandatory": "Some letters have neither annotation nor attachments. Please enter an annotation below, or add an attachment to the letters.", //_TO_TRANSLATE
    "endedCircuit": "Workflow ended", //_TO_TRANSLATE
    "userHasntSigned": "The assignee hasn't signed any document",  //_TO_TRANSLATE
    "noCircuitAvailable": "No workflow defined", //_TO_TRANSLATE
    "cannotAddAvisCircuit": "You cannot set avis circuit", //_TO_TRANSLATE
    "cannotAddVisaCircuit": "You cannot set visa circuit", //_TO_TRANSLATE
    "interrupted": "Interrupted", //_TO_TRANSLATE
    "addOpinionReason": "Please fill the opinion reason", //_TO_TRANSLATE
    "opinionLimitDate": "Opinion limit date", //_TO_TRANSLATE
    "visaWorkflowInterrupted" : "Interrupted workflow",//_TO_TRANSLATE
    "hasInterruptedWorkflow" : "Has interrupted the workflow",//_TO_TRANSLATE
    "addOpinion": "Give your opinion", //_TO_TRANSLATE
    "askOpinion": "Several users ask your opinion", //_TO_TRANSLATE
    "askOpinionUser": "ask your opinion", //_TO_TRANSLATE
    "avisUserAsk": "for opinion", //_TO_TRANSLATE
    "avisUserState": "opinion", //_TO_TRANSLATE
    "userNotInDiffusionList": "You are not in opinion diffusion list", //_TO_TRANSLATE
    "noOpinionLimitDate": "The opinion limit date is not defined", //_TO_TRANSLATE
    "noOpinionNote": "The reason is not defined", //_TO_TRANSLATE
    "opinionLimitDateOutdated": "The opinion limit date is outdated", //_TO_TRANSLATE
    "validateBy": "Validate by", //_TO_TRANSLATE
    "validateAvisParallel": "Your are validating an opinion request", //_TO_TRANSLATE
    "validateAvisParallelSingle": "Your are validating an opinion request of", //_TO_TRANSLATE
    "displayWebserviceAccount": "Display webservice account", //_TO_TRANSLATE
    "circuitNotStarted": "The workflow has not yet started", //_TO_TRANSLATE
    "searchCommunicationMean": "Search communication mean", //_TO_TRANSLATE
    "searchExternalIdM2M": "Search M2M Identifier", //_TO_TRANSLATE
    "goToFolder": "Go to folder", //_TO_TRANSLATE
    "m2mContactInfo": "Expected Format : SIRET/ENTITY_ID of destination entity", //_TO_TRANSLATE
    "ADD": "Add", //_TO_TRANSLATE
    "UP": "Update", //_TO_TRANSLATE
    "DEL": "Delete", //_TO_TRANSLATE
    "LOGIN": "Login", //_TO_TRANSLATE
    "LOGOUT": "Logout", //_TO_TRANSLATE
    "VIEW": "View", //_TO_TRANSLATE
    "ABS": "Vacation",//_TO_TRANSLATE
    "PRE": "Back absence",//_TO_TRANSLATE
    "SIGN"    : "Sign document",//TRANSLATE
    "UNSIGN"    : "Unsign document",//TRANSLATE
    "RESETPSW": "Password reset",//_TO_TRANSLATE
    "ERROR": "Error", //_TO_TRANSLATE
    "fileAlreadySentToSignatureBook": "File already send to signature book", //_TO_TRANSLATE
    "systemActions": "System action(s)", //_TO_TRANSLATE
    "viewAllHistory": "View all history", //_TO_TRANSLATE
    "viewActionsHistory": "View only actions in history", //_TO_TRANSLATE
    "unlink": "Unlink this resource", //_TO_TRANSLATE
    "linkResource": "Link to a new resource",  //_TO_TRANSLATE
    "linkSelectedResources": "Link selected resources", //_TO_TRANSLATE
    "resourceUnlinked": "Resource unlinked", //_TO_TRANSLATE
    "resourcesLinked": "Resources linked", //_TO_TRANSLATE
    "criteriaSearch": "Criteria search", //_TO_TRANSLATE
    "criteriaResourceField": "Subject / Chrono number", //_TO_TRANSLATE
    "criteriaResourceFieldDesc": "Search a subject / chrono or a part of these elements", //_TO_TRANSLATE
    "criteriaContactField": "Recipient / Sender", //_TO_TRANSLATE
    "criteriaContactFieldDesc": "Search the company, the name, the firstname or a part of these elements", //_TO_TRANSLATE
    "mailInformations": "Mail informations", //_TO_TRANSLATE
    "visaNote": "Order", //_TO_TRANSLATE
    "chooseBAN": "Choose an address", //_TO_TRANSLATE
    "click": "Click", //_TO_TRANSLATE
    "removeNote" : "Remove note", //_TO_TRANSLATE
    "editNote" : "Edit note", //_TO_TRANSLATE
    "noteRemoved" : "Note deleted", //_TO_TRANSLATE
    "confirmRemoveNote" : "Note deletion", //_TO_TRANSLATE
    "inSignatureBook_doc" : "Put the main document in signature book", //_TO_TRANSLATE
    "inShipping_doc" : "Intégrer le document principal  in send Maileva", //_TO_TRANSLATE
    "preRequisiteMissing": "Prerequisite missing for OnlyOffice : netcat", //_TO_TRANSLATE
    "onlyofficeEditDenied": "This document with extension", //_TO_TRANSLATE
    "onlyofficeEditDenied2": "can't be edited with onlyoffice", //_TO_TRANSLATE
    "NOTE_version": "Annotated version", //_TO_TRANSLATE
    "SIGN_version": "Original version", //_TO_TRANSLATE
    "versions": "Versions", //_TO_TRANSLATE
    "itemRemovedFromVisaTemplate": "The following users have been removed because they cannot approve or sign", //_TO_TRANSLATE
    "itemRemovedFromAvisTemplate": "The following users have been removed because they cannot give opinion", //_TO_TRANSLATE
    "documentSignedMsg": "The document has been signed, you can't edit this document", //_TO_TRANSLATE
    "banAutocompleteInput": "Autocomplete BAN input", //_TO_TRANSLATE
    "noMailContact" : "No (external) contact linked to this mail", //_TO_TRANSLATE
    "noSelectedAddress": "No selected address",  //_TO_TRANSLATE
    "documentUnsigned": "This document has been unsigned", //_TO_TRANSLATE
    "removeSignature" : "Remove signature", //_TO_TRANSLATE
    "cannotCloseMails" : "Some mails cannot be closed because at least one of mandatory fields are empty", //_TO_TRANSLATE
    "cannotCloseThisMail" : "This mail cannot be closed because at least one of mandatory fields are empty", //_TO_TRANSLATE
    "checkEmptyFields" : "Requisite fields to make this action", //_TO_TRANSLATE
    "contactsReassign" : "Contacts reassign",//_TO_TRANSLATE
    "contactLinkedToMails" : "Contact linked to mails", //_TO_TRANSLATE
    "reaffect" : "Reaffect", //_TO_TRANSLATE
    "reaffectContactRedirect" : "Reaffect to a contact", //_TO_TRANSLATE
    "contactReplacement" : "Replacement contact", //_TO_TRANSLATE
    "deleteContactwithtoutReassign" : "Delete contact without reassign", //_TO_TRANSLATE
    "deleteContactInformations" : "Links between contact and resources, attachments or acknowledgement receipts will be deleted.",//_TO_TRANSLATE
    "reaffectContactInformations" : "Links between contact and resources, attachments or acknowledgement receipts will be reassigned to choosen contact.",//_TO_TRANSLATE
    "availableContacts" : "Available contact", //_TO_TRANSLATE
    "sent" :  "Sent", //_TO_TRANSLATE
    "notSent" :  "Not sent", //_TO_TRANSLATE
    "delivery" :  "Delivery", //_TO_TRANSLATE
    "senderShort" :  "From", //_TO_TRANSLATE
    "recipientShort" :  "To", //_TO_TRANSLATE
    "copieShort" :  "Cc", //_TO_TRANSLATE
    "invisibleCopyShort" :  "Cci", //_TO_TRANSLATE
    "m2m_ARCHIVETRANSFER" : "Digitale brief",
    "m2m_ARCHIVETRANSFERREPLY" :  "Antwoord verzonden",
    "shipping" :  "Send maileva", //_TO_TRANSLATE
    "sendElement" :  "Send an element", //_TO_TRANSLATE
    "pdfVersion" : "PDF version", //_TO_TRANSLATE
    "emailModel" : "Email modele", //_TO_TRANSLATE
    "switchInPlainText" : "Switch in plain text", //_TO_TRANSLATE
    "switchInHtml" : "Switch in HTML", //_TO_TRANSLATE
    "confirmSwitchInPlanText": "Text formatting will be removed. Would you like to continue ?", //_TO_TRANSLATE
    "saveDraft" : "Save draft", //_TO_TRANSLATE
    "attachMainDocument" : "Link main document", //_TO_TRANSLATE
    "attachNote" : "Link a note", //_TO_TRANSLATE
    "attachAttachment" : "Link an attachment", //_TO_TRANSLATE
    "draftSaved" : "Draft saved",  //_TO_TRANSLATE
    "draftUpdated" : "Draft updated", //_TO_TRANSLATE
    "sendingEmail" : "Sending email...", //_TO_TRANSLATE
    "restrictions" : "Restrictions", //_TO_TRANSLATE
    "contactDeleted" : "Contact deleted", //_TO_TRANSLATE
    "badEmailsFormat" : "Some email address are not correct", //_TO_TRANSLATE
    "warnEmptySubject" : "You will send an email without subject, continue ?", //_TO_TRANSLATE
    "emailDeleted" : "Email deleted", //_TO_TRANSLATE
    "mailingActionInformations" : "Following mails contain mailing attachments, mailing will be activate :", //TRANSLATE
    "properties" : "Properties",  //_TO_TRANSLATE
    "generateARMode" : "Acknowledgement receipt mode allowed", //_TO_TRANSLATE
    "autoAR" : "Automatic", //_TO_TRANSLATE
    "manualAR" : "Manual", //_TO_TRANSLATE
    "bothAR" : "Automatic et manual", //_TO_TRANSLATE
    "tagCreation" : "Tag creation", //_TO_TRANSLATE
    "tagModification" : "Tag modification", //_TO_TRANSLATE
    "usage" : "Used for", //_TO_TRANSLATE
    "tagUpdated" : "Tag updated", //_TO_TRANSLATE
    "tagMerged" : "Tag merged", //_TO_TRANSLATE
    "mergeWith" : "Merge with", //_TO_TRANSLATE
    "willBeTransferredToNewTag" : "Number of mails that will be transferred to the tag ", //_TO_TRANSLATE
    "detailDoc" : "Detail page", //_TO_TRANSLATE
    "emptySubject" : "Empty subject", //_TO_TRANSLATE
    "noAvailableMenu" : "No available menu", //_TO_TRANSLATE
    "mailNotPresentInBasket" : "Mail not present in basket", //_TO_TRANSLATE
    "emailSent" : "Email sent", //_TO_TRANSLATE
    "emailCannotSent" : "Error during sending email", //_TO_TRANSLATE
    "sentToFastParapheur" : "Send to FAST-Parapheur", //_TO_TRANSLATE
    "sentToIxbusParapheur" : "Send to Ixbus Parapheur", //_TO_TRANSLATE
    "natureIxbusParapheur" : "Document nautre", //_TO_TRANSLATE
    "loginIxbus" : "Ixbus login", //_TO_TRANSLATE
    "passwordIxbus" : "Ixbus password", //_TO_TRANSLATE
    "workflowModelIxbus" : "Ixbus visa workflow", //_TO_TRANSLATE
    "electronicSignature" : "Electronic signature", //_TO_TRANSLATE
    "handwrittenSignature" : "Handwritten signature", //_TO_TRANSLATE
    "sentToIParapheur" : "Send to IParapheur", //_TO_TRANSLATE
    "editAcknowledgementReceipt" : "Edit the acknowledgement receipt", //_TO_TRANSLATE
    "insufficientPrivilege" : "Privilege insuffisant", //TRANSLATE
    "cannotMergeTags" : "This tag has a parent or children : impossible to merge tags",//_TO_TRANSLATE
    "mergeTagWith" : "Merge this tag with",//_TO_TRANSLATE
    "generateAndDownloadPrintedFolder" : "Generate the printed folder", //_TO_TRANSLATE
    "printedFolder" : "Printed folder", //_TO_TRANSLATE
    "mainDocument" : "Main document", //_TO_TRANSLATE
    "attachSummarySheet" : "Attach the summary sheet", //_TO_TRANSLATE
    "generateSeparators" : "Generate separators for each attachments and acknowledgement receipt", //_TO_TRANSLATE
    "theTag" : "The tag",//_TO_TRANSLATE
    "addUsers" : "Add users", //_TO_TRANSLATE
    "willBeDeletedAndMerged" : "will be deleted and merged with the tag", //_TO_TRANSLATE
    "mailing" : "Mailing", //_TO_TRANSLATE
    "goToBasket" : "Go to basket...", //_TO_TRANSLATE
    "manualSendingAR" : "Manual sending...", //_TO_TRANSLATE
    "withoutEmail" : "Without email", //_TO_TRANSLATE
    "enableMailing" : "Enable mailing",  //_TO_TRANSLATE
    "disableMailing" : "Disable mailing", //_TO_TRANSLATE
    "selectContact" : "select a contact", //_TO_TRANSLATE
    "mailingMsg" : "<b>Mailing enbled</b> : <br><br><p>A <b>master</b> attachment will be created without merged field <b>contact</b> (attachmentRecipient).</p><p>If you click on Mailing, the attachmenets will be generated <b>NOW</b>.<br><br>If you click on Validate, They will be generated <b>BEFORE</b> to send to the first <b>signatory</b> of visa circuit.</p><p><b>One</b> attachment will be generated for <b>each contact</b> linked to the mail.</p>", //_TO_TRANSLATE
    "attachmentGenerated" : "Attachment generated", //_TO_TRANSLATE
    "mustEditAttachmentFirst" : "You must edit the attachment first", //_TO_TRANSLATE
    "saveAndClose" : "Save and close", //_TO_TRANSLATE
    "opinionCircuit" : "Opinion circuit", //_TO_TRANSLATE 
    "visaCircuit" : "Visa circuit",  //_TO_TRANSLATE
    "shippingUnavailable" : "<b>Warning </b> Shipping is not activated. Check the configuration file mailevaConfig.xml", //_TO_TRANSLATE
    "diffNoDestmsg" : "No <b>assignee</b> defined, you <b>can't</b> modify the diffusion list.", //_TO_TRANSLATE
    "noDest" : "No <b>assignee</b> defined", //_TO_TRANSLATE
    "noPrivileges" : "Insufficient privileges", //_TO_TRANSLATE
    "mustDeleteUsersWithNoPrivileges" : "You must delete users with insufficient privileges", //_TO_TRANSLATE
    "sendNumericPackage" : "Send a numeric package", //_TO_TRANSLATE
    "numericPackageSent" : "Numeric package sent", //_TO_TRANSLATE
    "numericPackageDeleted" : "Numeric package deleted", //_TO_TRANSLATE
    "sendNumericPackageInfo" : "You can send mail and attachments between <b> two instances </b> of Maarch Courrier. <br /> <br /> You will receive <b> an acknowledgment of receipt </b> and you will be able <b> follow the progress </b> of the mail at the recipient. (see <a href=\"{0}\" target=\"_blank\"> Maarch2GEC </a> for more information).", //_TO_TRANSLATE
    "mailSubject" : "Mail subject", //_TO_TRANSLATE
    "attachItemToNumericPackage" : "Attach an element to sending mail", //_TO_TRANSLATE
    "note" : "Note", //_TO_TRANSLATE
    "addNoteToNumericPackage" : "Add a note Ajouter to sending mail", //_TO_TRANSLATE
    "mainDocNumericPackage" : "Main document of sent mail", //_TO_TRANSLATE
    "attachmentsNumericPackage" : "Attachments of sent mail", //_TO_TRANSLATE
    "mainDocNumericPackageToSend" : "Main document of sending mail", //_TO_TRANSLATE
    "attachmentsNumericPackageToSend" : "Attachments of sending mail", //_TO_TRANSLATE
    "closeSidePanel" : "Close side panel", //_TO_TRANSLATE
    "openSidePanel" : "Open side panel", //_TO_TRANSLATE
    "saveAsPrivateModel" : "Save as private model", //_TO_TRANSLATE
    "editingDocumentMsg" : "The edition of the document is in progress, do you want to continue ?", //_TO_TRANSLATE
    "createFolder" : "Create a folder", //_TO_TRANSLATE
    "downloadNumericPackage" : "Download the numeric package", //_TO_TRANSLATE
    "actionsHistory" : "Actions history", //_TO_TRANSLATE
    "noActionProcessed": "No action processed", //_TO_TRANSLATE
    "reconcileMsg": "The mails will be converted into <b>signed version</b> and linked to selected new mail.", //_TO_TRANSLATE
    "reconcileMsg2": "If an <b>outgoing mail</b> is selected, only the <b>last mail</b> will be converted into signed version. The others will be <b>removed</b> !", //_TO_TRANSLATE
    "selectMailToReconcile": "Please, select a mail to reconcile", //_TO_TRANSLATE
    "modelUsedByResources": "This model is used by resources, you can't delete it.", //_TO_TRANSLATE
    "cantEditFileWithJava": "You can't edit with the java applet an uploaded document.", //_TO_TRANSLATE
    "linkedResources": "Main document (Linked mails)", //_TO_TRANSLATE
    "linkedResourcesAttachments": "Attachments (linked mails)", //_TO_TRANSLATE
    "linkedEntities": "Linked entities", //_TO_TRANSLATE
    "accounts": "Accounts", //_TO_TRANSLATE
    "alfrescoCreation": "Alfresco account creation", //_TO_TRANSLATE
    "alfrescoModification": "Alfresco account modification", //_TO_TRANSLATE
    "showTreeMsg": "Click here to show the Alfresco tree", //_TO_TRANSLATE
    "mustFillAccountMsg": "Please fill the Alfresco account information in order to choose a root folder.", //_TO_TRANSLATE
    "accountUpdated": "Account updated", //_TO_TRANSLATE
    "alfrescoAccount": "Alfresco account", //_TO_TRANSLATE
    "rootFolder": "Root folder", //_TO_TRANSLATE
    "chooseEntityAssociationAccount": "Choose entities who will be able to use this account", //_TO_TRANSLATE
    "alfrescoAPI": "API Alfresco address", //_TO_TRANSLATE
    "testSucceeded": "Test succeeded",  //_TO_TRANSLATE
    "test": "Test", //_TO_TRANSLATE
    "canUpdateIndexingModel": "Can change indexing model", //_TO_TRANSLATE
    "options": "Options", //_TO_TRANSLATE
    "modificationsProcessed": "Modifications processed", //_TO_TRANSLATE
    "contactsDuplicates": "Contacts duplicates",  //_TO_TRANSLATE
    "contactsMerged": "Contacts merged", //_TO_TRANSLATE
    "addCriteria": "Add a criteria", //_TO_TRANSLATE
    "launchSearch": "Launch search", //_TO_TRANSLATE
    "duplicatesContactDisplayed": "{0} duplicates ({1} displayed)", //_TO_TRANSLATE
    "duplicatesContactsAdmin": "Manage contacts duplicates", //_TO_TRANSLATE
    "merge": "Merge", //_TO_TRANSLATE
    "associatedElements": "associated elements", //_TO_TRANSLATE
    "selectedContact": "Target contact selected", //_TO_TRANSLATE
    "selectDuplicatedContact": "Merge with this contact", // _TO_TRANSLATE
    "noDuplicatesFound": "No duplicates found", // _TO_TRANSLATE
    "exludeContact": "Exclude contact", // _TO_TRANSLATE
    "contactExcluded": "Contact excluded", // _TO_TRANSLATE
    "exportContacts": "Export contacts",  // _TO_TRANSLATE
    "companyLastname": "Company / Lastname", // _TO_TRANSLATE
    "advancedMode": "Advanced mode", // _TO_TRANSLATE
    "defaultMode": "Default mode", // _TO_TRANSLATE
    "linkedTags": "Linked tags", // _TO_TRANSLATE
    "selectParentTag": "Select a parent tag", // _TO_TRANSLATE
    "openThesaurus": "Open the thesaurus", // _TO_TRANSLATE
    "invalidClause": "Invalid clause",
    "invalidColumnType": "One of selected columns does not match type",
    "forbiddenDelimiterType": "Delimiters are forbidden for this type",
    "bddModeCustomFieldMsg": "This field use database values (see the custom's configration in custom fields administration)", // _TO_TRANSLATE
    "updateIndexingFieldWarning": "Warning:if you remove a field from the model, this field will be reset for <b>ALL</b> resources linked to this model. Do you want to continue ?__TO_TRANSLATE",
    "addColumn": "Ajouter une colonne__TO_TRANSLATE",  // _TO_TRANSLATE
    "database": "Database__TO_TRANSLATE",
    "targetLabel": "Target label__TO_TRANSLATE",
    "targetTable": "Target table__TO_TRANSLATE",
    "targetIdentifier": "Target identifier__TO_TRANSLATE",
    "canUpdateResourcesInSignatureBook": "Can modify the main document included in the signature book__TO_TRANSLATE",
    "includeFolderPerimeter": "Consulter les courriers hors périmètre dans les dossiers et courriers suivis__TO_TRANSLATE",
    "documentsOutOfPerimeter": "Certains documents sont en dehors de votre périmètre__TO_TRANSLATE",
    "collaboraOnlineNotEnabled": "The Collabora Online server is not enabled in documentEditorsConfig.xml", // _TO_TRANSLATE
    "checkCollaboraOnlineServer": "Connecting to the Collabora Online server", // _TO_TRANSLATE
    "errorCollaboraOnline1": "Cannot launch Collabora Online, you are using a local address", // _TO_TRANSLATE
    "errorCollaboraOnline2": "Cannot launch Collabora Online. Check if the Collabora Online server is available", // _TO_TRANSLATE
    "collaboraOnlineEditDenied": "This document cannot be edited with Collabora Online", // _TO_TRANSLATE
    "collaboraOnlineEditDenied2": "cannot be edited with Collabora Online", // _TO_TRANSLATE
    "documentEditor_collaboraonline": "Open Collabora Online directly inside Maarch Courrier", // _TO_TRANSLATE
    "updateKO": "Mise à jour échouée !__TO_TRANSLATE",
    "saveInDocserversInfo": "L'historique de la mise à jour a été sauvegardé dans la zone de stockage <b>migration</b>.__TO_TRANSLATE",
    "hideTool": "Cacher la barre d'outils", // _TO_TRANSLATE
    "showTool": "Afficher la barre d'outils", // _TO_TRANSLATE
    "contactInput" : "Contact input__TO_TRANSLATE", // _TO_TRANSLATE
    "emptyValue": "Valeur vide__TO_TRANSLATE",
    "hideModal": "Ne plus afficher cette fenêtre__TO_TRANSLATE",
    "indexingModelUsedBy": "Ce modèle est déjà utilisé par : __TO_TRANSLATE",
    "mailsWithStatus": " courrier(s) avec le statut __TO_TRANSLATE",
    "indexingModelReplaceToDelete": "Pour le supprimer vous devez le remplacer par un autre modèle.__TO_TRANSLATE",
    "indexingModelFieldsReset": "Les champs suivants ne sont pas dans le modèle sélectionné et seront réinitialisés pour tous les courriers : __TO_TRANSLATE",
    "indexingModelReassign": "Réaffectation du modèle d'enregistrement__TO_TRANSLATE",
    "youCannotUnsign": "Vous n'avez pas le droit de <b>dé-signer</b> ce document. Vous devez ếtre la personne qui a signé ce document.__TO_TRANSLATE",
    "arContentIsEmpty": "AR content is empty" // _TO_TRANSLATE
};
